<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminSubscriptionHistory extends Model
{
    protected $table="admin_subscription_history";

    public function card(){

        return $this->belongsTo('App\AdminCard','id','card_id');
    
    }

    public function admin(){

        return $this->belongsTo('App\Admin');
    
    }

    public function apartment(){

        return $this->belongsTo('App\Apartment');
    
    }
    
    public function subscription(){

        return $this->belongsTo('App\Subscription','id','subscription_id');
    
    }
}
