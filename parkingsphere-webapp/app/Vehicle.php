<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    protected $table="vehicles";

    // public function user(){

    //     return $this->belongsTo('App\User','id','user_id');
    
    // }

    public function user(){

        return $this->belongsTo('App\User');
    
    }

    public function apartment(){

        return $this->belongsTo('App\Apartment','apartment_id','id');
    
    }

    public function images(){

        return $this->hasMany('App\VehicleImage','vehicle_id','id');
    
    }

    public function parkingPermit(){

        return $this->hasOne('App\ParkingPermit','vehicle_id','id');
    
    }
}
