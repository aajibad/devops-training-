<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminOption extends Model
{
    protected $table="admin_options";

    protected $guarded = [];

    public function apartment(){

      return $this->belongsTo('App\Apartment','apartment_id','id');

    }
}
