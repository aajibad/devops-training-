<?php

namespace App\Listeners;

use App\Events\ApartmentEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\AdminOption;
use App\AdminParkingPermitOption;
use App\AdminApartment;
use Auth;


class CreateNewAdminOption
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ApartmentEvent  $event
     * @return void
     */
    public function handle(ApartmentEvent $event)
    {
        $adminOption=new AdminOption();
        $adminOption->apartment()->associate($event->apartment->id);
        $adminOption->save();

        $adminParkingPermitOption=new AdminParkingPermitOption();
        $adminParkingPermitOption->permit_type='floor';
        $adminParkingPermitOption->apartment()->associate($event->apartment->id);
        $adminParkingPermitOption->save();

        $adminParkingPermitOption=new AdminParkingPermitOption();
        $adminParkingPermitOption->permit_type='building';
        $adminParkingPermitOption->apartment()->associate($event->apartment->id);
        $adminParkingPermitOption->save();
    }

}
