<?php

namespace App\Listeners;

use App\Events\UserEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;
use App\Mail\NewUserCreated;
use App\AdminApartment;

class SendNewUserEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserEvent  $event
     * @return void
     */
    public function handle(UserEvent $event)
    {
        $admin=AdminApartment::where('apartment_id',$event->user->apartment->id)->first();
        if($admin){
            Mail::to($admin->user->email)->send(new NewUserCreated($event->user));
        }
    }
}
