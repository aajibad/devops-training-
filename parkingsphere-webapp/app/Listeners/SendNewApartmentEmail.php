<?php

namespace App\Listeners;

use App\Events\ApartmentEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;
use App\Mail\NewApartmentCreated;
use App\SuperAdmin;

class SendNewApartmentEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ApartmentEvent  $event
     * @return void
     */
    public function handle(ApartmentEvent $event)
    {
        $superadmin=SuperAdmin::first();

        Mail::to($superadmin->email)->send(new NewApartmentCreated($event->apartment));
    }
}
