<?php

namespace App\Listeners;

use App\Events\AdminEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;
use App\Mail\NewAdminCreated;
use App\SuperAdmin;

class SendNewAdminEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AdminEvent  $event
     * @return void
     */
    public function handle(AdminEvent $event)
    {
        $superadmin=SuperAdmin::first();
        Mail::to($superadmin->email)->send(new NewAdminCreated($event->admin));
    }
}
