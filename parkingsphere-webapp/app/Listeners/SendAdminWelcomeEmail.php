<?php

namespace App\Listeners;

use App\Events\AdminEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;
use App\Mail\NewAdminWelcome;

class SendAdminWelcomeEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AdminEvent  $event
     * @return void
     */
    public function handle(AdminEvent $event)
    {
        Mail::to($event->admin->email)->send(new NewAdminWelcome($event->admin));
    }
}
