<?php

namespace App\Listeners;

use App\Events\ParkingPermitEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;
use App\Mail\NewParkingPermitPurchased;
use App\SuperAdmin;

class SendNewParkingPermitEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ParkingPermitEvent  $event
     * @return void
     */
    public function handle(ParkingPermitEvent $event)
    {
        $superadmin=SuperAdmin::first();
        Mail::to($superadmin->email)->send(new NewParkingPermitPurchased($event->parking_permits));
    }
}
