<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdminCard extends Model
{
    use SoftDeletes;

    protected $tables="admin_cards";
    protected $dates=['deleted_at'];

    public function admin(){

      return $this->belongsTo('App\Admin','user_id','id');

    }

    function numberMask($number) {
      return substr($number, 0, 4) . str_repeat('X', strlen($number) - 8) . substr($number, -4);
    }
}
