<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class AdminApartment extends Pivot
{
    protected $table="admin_apartment";

    protected $guarded=[];

    public function user(){

        return $this->belongsTo('App\Admin','admin_id','id');
    
    }

    public function apartment(){

        return $this->belongsTo('App\Apartment','apartment_id','id');
    
    }
}
