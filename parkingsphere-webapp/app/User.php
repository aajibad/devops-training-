<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

use App\Apartment;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','apartment_id','firstname','lastname'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $dispatchesEvents=[
        'created'=>Events\UserEvent::class
    ];

    public function apartment(){

        return $this->belongsTo('App\Apartment');

    }

    public function vehicles(){

        return $this->hasMany('App\Vehicle','user_id','id');

    }

    public function parkingPermits(){

        return $this->hasMany('App\ParkingPermit','user_id','id');

    }

    public function visitorParkingPermits(){

        return $this->hasMany('App\VisitorParkingPermit','generated_by','id');

    }

    public function cards(){

        return $this->hasMany('App\UserCard','user_id','id');

    }

    public function histories(){

        return $this->hasMany('App\UserPaymentHistory','user_id','id');

    }

    public function towedVehicles(){

        return $this->hasMany('App\TowedVehicle','user_id','id');

    }

        /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    
    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
