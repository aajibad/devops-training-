<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Message;

class MessageConversation extends Model
{
    use SoftDeletes;
    
    protected $table="message_conversations";

    public function messages(){

        return $this->hasMany('App\Message','conversation_id','id');

    }

    public function user(){

        return $this->belongsTo('App\User');
    
    }

    public function images(){
    
        return $this->hasMany('App\MessageImage','conversation_id','id');
    
    }

    public function apartment(){

        return $this->belongsTo('App\Apartment');

    }

    public function count_unread(){
        return \App\Message::where([
            'conversation_id'=>$this->id,
            'is_read'=>0
        ])->count();
    }
}
