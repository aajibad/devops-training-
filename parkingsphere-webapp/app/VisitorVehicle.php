<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VisitorVehicle extends Model
{
    public function apartment(){

        return $this->belongsTo('App\Apartment','apartment_id','id');
    
    }

    public function visitorPermit(){

        return $this->hasOne('App\VisitorParkingPermit','vehicle_id','id');
    
    }

    public function images(){

        return $this->hasMany('App\VehicleImage','vehicle_id','id');
    
    }
}
