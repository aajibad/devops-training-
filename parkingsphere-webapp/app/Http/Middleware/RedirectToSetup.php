<?php

namespace App\Http\Middleware;

use Closure;
use View;

use App\Admin;
use Session;
use Auth;

class RedirectToSetup
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::guard()->user()->status=="CREATED"){
            return redirect()->route('setup');
        }

        return $next($request);


    }
}
