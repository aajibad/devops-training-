<?php

namespace App\Http\Middleware;

use Closure;
use View;
use App\User;
use Session;

class AdminDashboardCounter
{
    public $countUserApprovals;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Session::has("admin_apartment")){
            $ApartmentId=Session::get("admin_apartment")->id;
            // Count user approvals
            $this->countUserApprovals=User::where('apartment_id',$ApartmentId)->where('status','pending')->get()->count();
            View::share ('count_user_approvals', $this->countUserApprovals);
        }

        return $next($request);
    }
}
