<?php

namespace App\Http\Controllers\Superadmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\SuperAdminOption;
use App\Apartment;
use Session;

class SuperAdminOptionController extends Controller
{

  public function __construct(){

    $this->middleware('auth:superadmin');

  }

  public function index()
  {
    $option=SuperAdminOption::all();
    return view('superadmin.settings.settings',['option'=>$option]);
  }

  public function create(Request $request)
  {
    // $option_rate=SuperAdminOption::getOption($request->subscription_type.'_subscription_rate');
    // $option_discount=SuperAdminOption::getOption($request->subscription_type.'_subscription_discount');
     
    $option=new SuperAdminOption();
    $option->name=$request->subscription_type.'_subscription_rate';
    $option->value=$request->value;
    $option->save();

    $option=new SuperAdminOption();
    $option->name=$request->subscription_type.'_subscription_discount';
    $option->value=$request->discount;
    $option->save();

    return redirect()->back();
  }

  public function update()
  {
      return;
  }

  public function delete()
  {
      // $findCard=Card:find($request->id);
      // $findCard->delete();
      return;
  }

  public function testApi(Request $request){
    $name=$request->name;
    $email=$request->email;
  }

}
