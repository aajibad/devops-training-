<?php

namespace App\Http\Controllers\Superadmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\AdminCard;
use App\Subscription;
use Session;

use PayPal\Api\Plan;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;

class PaymentController extends Controller
{

  protected $apiContext;

  public function __construct(){

    $clientID="AVZuaKJOhb-oKqG1Fdc29FULSUhJitHyRfe0Rws1yW_nqKSI_mcVGzGpCRQOCTPmGfOY8U6YQNsFPQWe";
    $secretID="EBH4c-9I0-FEo85NBjHBHpnKIksBUGnG-KLFxvfABCLeVhPo3O6NIx_FdSp8vidhS6Mro18jfldW0MPH";

    $this->apiContext = new ApiContext(
      new OAuthTokenCredential($clientID,$secretID)
    );

    $this->middleware('auth:superadmin');

  }

  public function index()
  {
    $subsciptions=Subscription::all();
    // $billingPlans=$this->listBillingPlans();
      
    return view('superadmin.payments.payments',[
      'subscriptions'=>$subsciptions,
      // 'billing_plans'=>$billingPlans->plans
    ]);
  }

  protected function listBillingPlans()
  {
    $params=array('status'=>'ACTIVE');

    $planList = Plan::all($params, $this->apiContext);
    
    return $planList;
  }

  public function update(Request $request,$id)
  {
    return;
  }

  public function delete()
  {
      // $findCard=Card:find($request->id);
      // $findCard->delete();
      return;
  }

}
