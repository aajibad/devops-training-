<?php

namespace App\Http\Controllers\Superadmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Admin;
use App\User;
use App\Apartment;
use Auth;
use Session;
use App\Subscription;

class UserController extends Controller
{

  public function __construct(){

    $this->middleware('auth:superadmin');

  }

  public function index()
  {
        $admins=Admin::all();
        $users=User::all();
        return view('superadmin.users.users',['admins'=>$admins,'users'=>$users]);
  }

  public function blockAllApartments(Request $request)
  {
        $findAdmin=Admin::find($request->id);

        foreach($findAdmin->apartments as $apartment){
          $findApartment=Apartment::find($apartment->id);
          $findApartment->status='blocked';
          $findApartment->save();
        }

        return redirect()->back();
  }

  public function showApartments($id){
    $findAdmin=Admin::find($id);

    return view('superadmin.users.show_apartment',['user'=>$findAdmin]);
  }

  public function showVehicleInfo($id){
    $findUser=User::find($id);

    return view('superadmin.users.show_vehicle',['user'=>$findUser]);
  }

  public function showSubscriptionHistory($id){
    $findSubscriptions=Subscription::where(['payer_id'=>$id])->get();

    return view('superadmin.users.show_subscription_history',['subscriptions'=>$findSubscriptions]);
  }

  public function logout(Request $request) {
    Auth::guard('superadmin')->logout();
    return redirect('/superadmin/login');
  }
  
}
