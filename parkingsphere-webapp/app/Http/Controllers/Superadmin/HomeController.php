<?php

namespace App\Http\Controllers\Superadmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Subscription;
use App\Apartment;
use App\Admin;
use App\ParkingPermit;
use App\SuperAdminOption;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){

        $this->middleware('auth:superadmin');

    }

    public function index()
    {
        $admins=Admin::all();
        $apartments=Apartment::all();
        $parking_permits=ParkingPermit::all();
        $subscriptions=Subscription::all();

        $options=new SuperAdminOption();

        return view('superadmin.home',[
            'admins'=>$admins,
            'apartments'=>$apartments,
            'parking_permits'=>$parking_permits,
            'subscriptions'=>$subscriptions,
            'option'=>$options
        ]);
    }

}
