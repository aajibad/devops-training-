<?php

namespace App\Http\Controllers\Superadmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Apartment;

class ApartmentController extends Controller
{
    public function __construct(){

        return $this->middleware('auth:superadmin');
    
    }

    public function index(){

        $apartments=Apartment::all();
        return view('superadmin.apartments.apartments',['apartments'=>$apartments]);

    }

    public function changeStatus(Request $request){

        $apartment=Apartment::find($request->id);
        $apartment->status=$request->status;
        $apartment->save();

        return redirect()->back();

    }


}
