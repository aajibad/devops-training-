<?php

namespace App\Http\Controllers\Auth\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\User;
use Auth;

use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class LoginController extends Controller
{
  public function __construct()
  {
      $this->middleware('guest:user-api');
  }

  // public function showLoginForm(){
  //   return view('superadmin.auth.login');
  // }

  // public function login(Request $request){

  //   // Validates the form
  //   $credentials=$this->validate($request,[
  //     'email' => 'required|email',
  //     'password' => 'required'
  //   ]);

  //   // dd(Auth::guard());

  //   // Attempt to logged the user in
  //   if(Auth::guard('user-api')->attempt($credentials)){

  //       // If successfull , then response
  //       return response()->json([
  //         'message'=>'authenticated',
  //         'secret_key'=>Auth::guard('user-api')->tokenById(Auth::guard('user-api')->user()->id),
  //         'status'=>Auth::guard('user-api')->user()->status
  //       ]);
  //   }

  //   // If unsuccessfull redirect back to the login with the form data
  //   return response()->json([
  //       'message'=>'unauthenticated'
  //   ]);

  // }

  public function login(Request $request){

    // grab credentials from the request
    $credentials = $request->only('email', 'password');

    // dd($credentials);
    try {
        // attempt to verify the credentials and create a token for the user
        if (! $token = JWTAuth::attempt($credentials)) {
            return response()->json(['error' => 'invalid_credentials'], 401);
        }
    } catch (JWTException $e) {
        // something went wrong whilst attempting to encode the token
        return response()->json(['error' => 'could_not_create_token'], 500);
    }

    // all good so return the token
    return response()->json([
      'message'=>'authenticated',
      'id'=>JWTAuth::user()->id,
      'username'=>JWTAuth::user()->name,
      'apartment'=>JWTAuth::user()->apartment,
      'secret_key'=>$token,
      'status'=>JWTAuth::user()->status
    ]);

  }

  public function loginWith(Request $request){

    // grab credentials from the request
    $email = $request->only('email');

    $user=User::where('email',$email)->first();

    if($user){
        try {
            // find the user and create a token for the user
            if (! $token=auth()->login($user)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }

        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
    
        // all good so return the token
        return response()->json([
          'message'=>'authenticated',
          'id'=>JWTAuth::user()->id,
          'username'=>JWTAuth::user()->name,
          'apartment'=>JWTAuth::user()->apartment,
          'secret_key'=>$token,
          'status'=>JWTAuth::user()->status
        ]);
    }    

    return response()->json(['message' => 'not_exist']);
  }

  // protected function guard()
  // {
  //     return Auth::guard('user-api');
  // }

}
