<?php

namespace App\Http\Controllers\Auth\User;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use JWTAuth;

class RegisterController extends Controller
{

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirect = '/api/user/register';

    public function register(Request $request)
    {
        // $this->validator($request->all())->validate();
        // $user = $this->create($request);

        $user=new User();
        $user->avatar=$request->avatar;
        $user->name=$request->name;
        $user->email=$request->email;
        $user->phone=$request->phone;
        $user->room_no=$request->room_no;
        $user->password=bcrypt($request->password);
        $user->apartment()->associate($request->apartment_id);
        
        if($user->save()){
            
            $token = JWTAuth::attempt([
                "email"=>$request->email,
                "password"=>$request->password
            ]);

            return response()->json([
                'message'=>'success',
                'id'=>JWTAuth::user()->id,
                'username'=>JWTAuth::user()->name,
                'apartment'=>JWTAuth::user()->apartment,
                'secret_key'=>$token,
                'status'=>JWTAuth::user()->status
            ]);

        }

        return response()->json([
            'message'=>'unsuccessfull'
        ]);

    }

    //Validates user's Input
    protected function validator(array $data)
    {
        $validator=Validator::make($data, [
             'name' => 'required|max:255',
             'email' => 'required|email|max:255',
             'password' => 'required|min:6|confirmed',
        ]);

        return $validator;
    }

    //Get the guard to authenticate Admin
    protected function guard()
    {
        return Auth::guard('user-api');
    }
}
