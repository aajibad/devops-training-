<?php

namespace App\Http\Controllers\Auth\User;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Auth\Events\PasswordReset;

use App\User;
use Password;
use Auth;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = 'user/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:user-api');
    }

    public function showResetForm(Request $request,$token=null){
        
        return view('user.auth.reset')->with(
            ['token'=>$token,'email'=>$request->email]
        );
        
    }

    public function reset(Request $request){

        $this->validate($request, $this->rules(), $this->validationErrorMessages());

        $response = $this->broker()->reset(
            $this->credentials($request), function ($user, $password) {
                $this->resetPassword($user, $password);
            }
        );

        return $response == Password::PASSWORD_RESET
                    ? response()->json([
                        'message'=>'success'
                    ])
                    : response()->json([
                        'message'=>'failed' 
                    ]);
    }

    protected function resetPassword($user, $password)
    {
        $user->password = Hash::make($password);

        $user->api_token=bin2hex(openssl_random_pseudo_bytes(30));

        $user->setRememberToken(Str::random(60));

        $user->save();

        event(new PasswordReset($user));
    }

    protected function guard(){
        
        return Auth::guard('user-api');
            
    } 

    protected function broker(){

        return Password::broker('users');
    
    }
}
