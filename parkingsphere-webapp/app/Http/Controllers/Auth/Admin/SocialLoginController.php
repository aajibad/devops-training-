<?php

namespace App\Http\Controllers\Auth\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Admin;
use App\AdminApartment;
use App\Apartment;
use Session;
use Socialite;
use Auth;

class SocialLoginController extends Controller
{
    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider($service)
    {
        return Socialite::driver($service)->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback($service)
    {
        $user=Socialite::with($service)->user ();
        
        $admin=Admin::where('email',$user->email)->get()->first();

        if($admin){
            Auth::guard('admin')->login($admin);

            $findAdminApartment=AdminApartment::where('admin_id',Auth::guard('admin')->user()->id)->first();
            $apartment=Apartment::find($findAdminApartment->apartment_id);

            Session::forget('admin_apartment');
            Session::forget('admin_permission');

            // Add new session apartment Id and admin Id
            Session::put('admin_apartment', $apartment);
            foreach(Auth::guard('admin')->user()->apartments as $admin){

                Session::put('admin_permission',$admin->pivot->permission);
            
            }

            return redirect()->route('admin.home');
        }
        else{
            // dd($user);
            return redirect()->route('admin.register.form',[
                'name'=>$user->name,
                'email'=>$user->email,
                'avatar'=>$user->avatar_original
            ]);
        }
        // $user->token;
    }
}
