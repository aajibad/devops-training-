<?php

namespace App\Http\Controllers\Auth\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use JWTAuth;
use Auth;
use App\AdminApartment;
use App\Admin;
use App\Apartment;
use Session;

class LoginController extends Controller
{

    public function __construct()
    {
        $this->middleware('guest:admin');
    }

    public function showLoginForm(){
      return view('admin.auth.login');
    }

    public function login(Request $request){

      // Validates the form
      $this->validate($request,[
        'email' => 'required|email',
        'password' => 'required|min:6'
      ]);

      // Attempt to logged the user in
      if(Auth::guard('admin')->attempt(['email'=>$request->email,'password'=>$request->password],$request->remember)){

          // Find first Apartment for current logged in user
          $findAdminApartment=AdminApartment::where('admin_id',Auth::guard('admin')->user()->id)->first();
          $apartment=Apartment::find($findAdminApartment->apartment_id);
          // if(Session::has('admin_apartment_id') || Session::exists('admin_apartment_id')){
          //   // Forgot Old sessions
            
          // }

          Session::forget('admin_apartment');
          Session::forget('admin_permission');

          // Add new session apartment Id and admin Id
          Session::put('admin_apartment', $apartment);
          foreach(Auth::guard('admin')->user()->apartments as $admin){

            Session::put('admin_permission',$admin->pivot->permission);
          
          }

          // If successfull , then redirect to their intended location
          return redirect()->intended(route('admin.home'));
      }

      // If unsuccessfull redirect back to the login with the form data
      return redirect()->back()->withInput($request->only('email','remember'));

    }

    public function TokenizeLogin(Request $request){
      // grab credentials from the request
        $credentials = $request->only('email', 'password');

        $admin=Admin::where('email',$request->email)->first();
        $adminApartment=AdminApartment::where('apartment_id',$request->apartment)->first();
        
        if(!is_null($adminApartment) && $adminApartment->admin_id==$admin->id){
          try {
              // attempt to verify the credentials and create a token for the user
              $claims=["apartment"=>$adminApartment->apartment->id];
              
              \Config::set('auth.defaults.guard', 'admin-api');

              if (! $token = JWTAuth::customClaims(['apartment' => $adminApartment->apartment->id])->attempt($credentials)) {
                  return response()->json(['error' => 'invalid_credentials'], 401);
              }
          } catch (JWTException $e) {
              // something went wrong whilst attempting to encode the token
              return response()->json(['error' => 'could_not_create_token'], 500);
          }

          // all good so return the token
          return response()->json([
            'message'=>'authenticated',
            'admin'=>$admin,
            'apartment'=>$adminApartment->apartment,
            'secret_key'=>$token
          ]);
        }
        
        return response()->json([
          "message"=>"failed",
          "reference"=>"No apartment found for requested admin"
        ]);
    }

}
