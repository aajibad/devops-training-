<?php

namespace App\Http\Controllers\Auth\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

use App\Admin;
use App\Apartment;
use App\AdminApartment;
use App\Subscription;
use Auth;
use Session;

class RegisterController extends Controller
{

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectPath = '/admin/home';

    //shows registration form to seller
    public function showRegistrationForm(Request $request)
    {
        return view('admin.auth.register',[
            'name'=>$request->name,
            'email'=>$request->email,
            'avatar'=>$request->avatar
        ]);
    }


    public function register(Request $request)
    {
      // exit($request);
        //Validates data
        $this->validator($request->all())->validate();

        //Create admin
        $admin = $this->create($request->all());
        $admin->avatar=$request->avatar;
        $admin->save();

        // $formatLower=strtolower($request->apartment_name);
        // $formatAartmentName=str_replace(' ','_',$formatLower);
        // $fileExtension=$request->apartment_logo->extension();

        // $findAdmin=Admin::find($admin->id);
        // exit(var_dump($admin));
        // $apartment=new Apartment();
        // $apartment->name=$request->apartment_name;
        // $apartment->logo=$request->apartment_logo->storeAs('images/apartment_banners','logo_'.$admin->id.'_'.$formatAartmentName.'.'.$fileExtension);
        // $apartment->phone=$request->apartment_phone;
        // $apartment->address=$request->apartment_address;
        // $apartment->subscription_type=$request->subscription;
        // $apartment->save();

        // $findApartment=Apartment::find($apartment->id);
        // $findApartment->admins()->attach($findAdmin,['permission'=>'read_write']);
        // $findApartment->save();

        // $subscription=new Subscription();
        // $subscription->amount='';
        // $subscription->subscription_type=$request->subscription;
        // $subscription->payer()->associate($findAdmin);
        // $subscription->apartment()->associate($apartment);
        // $subscription->save();

         //Authenticates admin
        $this->guard('admin')->login($admin);

        // $findAdminApartment=AdminApartment::where('admin_id',Auth::guard('admin')->user()->id)->first();
        // $foundApartment=Apartment::find($findAdminApartment->apartment_id);

        // Session::forget('admin_apartment');
        // Session::forget('admin_permission');

        // Add new session apartment Id and admin Id
        // Session::put('admin_apartment', $foundApartment);
        // foreach(Auth::guard('admin')->user()->apartments as $admin){

        //   Session::put('admin_permission',$admin->pivot->permission);
        
        // }
        //Redirects admins
        return redirect($this->redirectPath);
    }

    //Validates user's Input
    protected function validator(array $data)
    {
        return Validator::make($data, [
             'name' => 'required|max:255',
             'email' => 'required|email|max:255',
             'password' => 'required|min:6|confirmed',
        ]);
    }

    //Create a new admin instance after a validation.
    protected function create(array $data)
    {
        return Admin::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    //Get the guard to authenticate Admin
    protected function guard()
    {
        return Auth::guard('admin');
    }
}
