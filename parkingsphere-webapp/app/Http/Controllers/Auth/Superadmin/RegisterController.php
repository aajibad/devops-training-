<?php

namespace App\Http\Controllers\Auth\Superadmin;

use App\SuperAdmin;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Auth;

class RegisterController extends Controller
{

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectPath = '/superadmin/home';

    //shows registration form to seller
    public function showRegistrationForm()
    {
        return view('superadmin.auth.register');
    }


    public function register(Request $request)
    {
        //Validates data
        $this->validator($request->all())->validate();

        //Create admin
        $superadmin = $this->create($request->all());

         //Authenticates admin
        $this->guard()->login($superadmin);

        //Redirects admins
        return redirect($this->redirectPath);
    }

    //Validates user's Input
    protected function validator(array $data)
    {
        return Validator::make($data, [
             'name' => 'required|max:255',
             'email' => 'required|email|max:255',
             'password' => 'required|min:6|confirmed',
        ]);
    }

    //Create a new admin instance after a validation.
    protected function create(array $data)
    {
        return SuperAdmin::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    //Get the guard to authenticate Admin
    protected function guard()
    {
        return Auth::guard('superadmin');
    }
}
