<?php

namespace App\Http\Controllers\Auth\Superadmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;

class LoginController extends Controller
{
  public function __construct()
  {
      $this->middleware('guest:superadmin');
  }

  public function showLoginForm(){
    return view('superadmin.auth.login');
  }

  public function login(Request $request){

    // Validates the form
    $this->validate($request,[
      'email' => 'required|email',
      'password' => 'required|min:6'
    ]);

    // Attempt to logged the user in
    if(Auth::guard('superadmin')->attempt(['email'=>$request->email,'password'=>$request->password],$request->remember)){
        // If successfull , then redirect to their intended location
        return redirect()->intended(route('superadmin.home'));
    }

    // If unsuccessfull redirect back to the login with the form data
    return redirect()->back()->withInput($request->only('email','remember'));

  }
}
