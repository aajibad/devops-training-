<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Message;
use App\MessageConversation;
use App\Apartment;
use App\User;
use App\Admin;

class MessageController extends Controller
{
    public function __construct(){

        $this->middleware('auth:admin');

    }

    public function index(){

        return view('admin.complaints.show');
        
    }

    public function create(Request $request){

        $findConversation=MessageConversation::find($request->conversation_id);
        $findAdmin=Admin::find($request->user_id);

        $message=new Message();
        $message->body=$request->body;
        $message->user_type='admin';
        $message->admin()->associate($findAdmin);
        $message->conversation()->associate($findConversation);
        $message->save();
    
    }

    public function show(){

    }

    public function send(){

    }
}
