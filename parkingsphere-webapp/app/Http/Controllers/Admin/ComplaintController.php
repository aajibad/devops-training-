<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\MessageConversation;
use App\Message;
use App\Admin;
use App\Events\NewMessageEvent;
use Auth;
use Session;

class ComplaintController extends Controller
{

    public function __construct(){

        $this->middleware('auth:admin');

    }

    public function index(Request $request){
        
        $apartmentId=Session::get('admin_apartment')->id;
        $conversations=MessageConversation::where('apartment_id',$apartmentId)->with('user','messages','apartment')->get();
        $conversationsPaginate=MessageConversation::where('apartment_id',$apartmentId)->paginate(10);
        
        if($request->json){
            return response()->json([
                "conversations"=>$conversations,
                'apartment_id'=>$apartmentId
            ]);
        }

        return view('admin.complaints.complaints',[
            'conversations'=>$conversationsPaginate
        ]);
        
    }

    public function create(Request $request){

        $findConversation=MessageConversation::find($request->conversation_id);
        $findAdmin=Admin::find($request->user_id);

        $message=new Message();
        $message->body=$request->body;
        $message->user_type='admin';
        $message->admin()->associate($findAdmin);
        $message->conversation()->associate($findConversation);

        if($message->save()){
            $conversation=$findConversation->with('user')->get();
            broadcast(new NewMessageEvent($message))->toOthers();

            return response()->json([
                'return'=>'success',
                'message'=>$message,
                'user'=>$message->conversation->user
            ]);
        }
    
        return response()->json([
            'message'=>'failed'
        ]);
    }

    public function show(Request $request){

        $conversation=MessageConversation::find($request->id);
        $messages=Message::where(['conversation_id'=>$request->id])->with('user','admin')->get();
        
        if($request->json){
            return response()->json([
                'conversation'=>$conversation,
                'messages'=>$messages,
                'user_avatar'=>secure_asset(is_null($conversation->user->avatar)?'images/admin/user.jpg':$conversation->user->avatar),
                'admin_avatar'=>secure_asset(is_null(Auth::guard('admin')->user()->avatar)?'images/admin/user.jpg':Auth::guard('admin')->user()->avatar)
            ]);
        }

        return view('admin.complaints.show',[
            'messages'=>$messages,
            'conversation'=>$conversation
        ]);
    }

    public function send(){

    }
}
