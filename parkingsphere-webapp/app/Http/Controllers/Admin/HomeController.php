<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use App\Apartment;
use App\Subscription;
use App\AdminParkingPermitOption;
use App\ParkingPermit;
use App\VisitorParkingPermit;
use App\TowedVehicle;
use App\Vehicle;
use Session;
use Auth;

class HomeController extends Controller
{

  public function __construct(){

    $this->middleware('auth:admin');

  }

  public function index(Request $request)
  {
    
    $sessionApartment=Session::get('admin_apartment');
    $towedVehicles=TowedVehicle::where('apartment_id',$sessionApartment->id)->get();
    $parkingPermits=$this->getParkingPermitsByApartmentId($sessionApartment->id);
    $visitorParkingPermits=$this->getVisitorParkingPermitsByApartmentId($sessionApartment->id);
    $subscription=Subscription::where('apartment_id',$sessionApartment->id)->get()->first();
    // exit(var_dump($findApartment));

    // $parking_permit_statistic=DB::select();    
    // $visitor_parking_permit_statistic=DB::select();
    // $last_3_month_statistic=DB::select();

    $todaysRevenue=ParkingPermit::where('apartment_id',$sessionApartment->id)
      ->where('created_at',date('Y-m-d'))->get()->sum('payment_amount');
      
    $totalPermitsFloor=DB::select('SELECT SUM(apo.total_permit) as total_floor
                  FROM admin_parking_permit_options as apo 
                  WHERE apo.apartment_id=:apartment_id 
                  AND apo.permit_type=:permit_type',
                  [
                    'apartment_id'=>$sessionApartment->id,
                    'permit_type'=>'floor'
                  ]);

    $totalPermitsBuilding=DB::select('SELECT SUM(apo.total_permit) as total_building 
                  FROM admin_parking_permit_options as apo 
                  WHERE apo.apartment_id=:apartment_id 
                  AND apo.permit_type=:permit_type',[
                    'apartment_id'=>$sessionApartment->id,
                    'permit_type'=>'building'
                  ]);
    
    $availableFloorPermits=AdminParkingPermitOption::where([
      'apartment_id'=>$sessionApartment->id,
      'permit_type'=>'floor'
      ])->orderBy('created_at','DESC')->first();

    $availableBuildingPermits=AdminParkingPermitOption::where([
      'apartment_id'=>$sessionApartment->id,
      'permit_type'=>'building'
      ])->orderBy('created_at','DESC')->first();
    
    $totalTowedVehicles=TowedVehicle::where('apartment_id',$sessionApartment->id)->get();

    // Last 3 months
    $count_parking_permit_results=[];
    $count_visitor_parking_permit_results=[];
    $count_towed_vehicle_results=[];

    $query_date=[];
    $requestDate=is_null($request->data)?'last_3_months':$request->data;
    
    if($requestDate=="today"){
      $query_date["from"]=date("Y-m-d H:m:s",strtotime("-1 day"));
      $query_date["to"]=date("Y-m-d H:m:s");
    }elseif ($requestDate=="week") {
      $query_date["from"]=date("Y-m-d H:m:s",strtotime("-7 day"));
      $query_date["to"]=date("Y-m-d H:m:s");
    }elseif($requestDate=="last_3_months"){
      $query_date["from"]=date("Y-m-d H:m:s",strtotime("-1 month"));
      $query_date["to"]=date("Y-m-d H:m:s");
    }

    $data=$this->getStatistics($query_date["from"],$query_date["to"]);

    $processed_statistics=$data;
    // dd($processed_statistics);
    if($request->json){
      return [
        "forLine"=>json_encode($processed_statistics),
        "forPie"=>[
          [
            "name"=>"Total Permits",
            "value"=>$totalPermitsFloor[0]->total_floor+$totalPermitsBuilding[0]->total_building
          ],
          [
            "name"=>"Available Permits",
            "value"=>$availableFloorPermits->available_permit+$availableBuildingPermits->available_permit
          ],
          [
            "name"=>"Sold Permits",
            "value"=>($totalPermitsFloor[0]->total_floor+$totalPermitsBuilding[0]->total_building)-($availableFloorPermits->available_permit+$availableBuildingPermits->available_permit)
          ],
          [
            "name"=>"Towed Vehicles",
            "value"=>count($totalTowedVehicles)
          ]
        ]
      ];
    }
    // $getStatistic=$this->getAnalyticsByDate(is_null($request->data)?'last_3_months':$request->data);
    
    return view('admin.home',[
      'subscription'=>$subscription,
      'towedVehicles'=>$towedVehicles,
      'parkingPermits'=>$parkingPermits,
      'visitorParkingPermits'=>$visitorParkingPermits,
      'floor_permits'=>$totalPermitsFloor[0]->total_floor,
      'building_permits'=>$totalPermitsBuilding[0]->total_building,
      'available_floor_permits'=>$availableFloorPermits,
      'available_building_permits'=>$availableBuildingPermits,
      'total_towed_vehicles'=>$totalTowedVehicles,
      'statistics'=>$processed_statistics,
      'todays_revenue'=>$todaysRevenue,
      'statistic_date_type'=>$requestDate
    ]);
  }

  protected function getDatesFromRange($start, $end, $format='Y-m-d') {
    return array_map(function($timestamp) use($format) {
        return date($format, $timestamp);
    },

    range(strtotime($start) + ($start < $end ? 4000 : 8000), strtotime($end) + ($start < $end ? 8000 : 4000), 86400));
  }
  
  // public function getStatistics($from,$to){

  //   $apartment_id=Session::get("admin_apartment")->id;
  //   $result;
    
  //   $result["parking_permits"]=DB::select("SELECT pp.created_at as date,COUNT(pp.id) as permits,SUM(pp.payment_amount) as revenue 
  //           FROM parking_permits as pp
  //           WHERE pp.created_at 
  //           BETWEEN '".$from."' 
  //           AND '".$to."' AND pp.apartment_id=".$apartment_id." GROUP BY pp.created_at");
    
  //   $result["visitor_parking_permits"]=DB::select("SELECT vp.created_at as date,COUNT(vp.id) as v_permits 
  //           FROM visitor_parking_permits as vp
  //           WHERE vp.created_at 
  //           BETWEEN '".$from."' 
  //           AND '".$to."' AND vp.apartment_id=".$apartment_id." GROUP BY vp.created_at");

  //   $result["towed_vehicles"]=DB::select("SELECT tv.created_at as date,COUNT(tv.id) as vehicles 
  //           FROM towed_vehicles as tv
  //           WHERE tv.created_at 
  //           BETWEEN '".$from."' 
  //           AND '".$to."' AND tv.apartment_id=".$apartment_id." GROUP BY tv.created_at");
    
  //   return $result;
  // }

  public function getStatistics($from,$to){

    $apartment_id=Session::get("admin_apartment")->id;
    
    $statistics=[];
    $result=[];
    
    $dates=$this->getDatesFromRange($from,$to);

    foreach ($dates as $date) {
      $parking_permit_revenue=DB::select("SELECT SUM(pp.payment_amount) as revenue
      FROM parking_permits as pp
      WHERE pp.created_at='".$date."' AND pp.apartment_id=".$apartment_id)[0]->revenue;

      $parking_permits=DB::select("SELECT COUNT(pp.id) as permits
      FROM parking_permits as pp
      WHERE pp.created_at='".$date."' AND pp.apartment_id=".$apartment_id)[0]->permits;

      $visitor_parking_permits=DB::select("SELECT COUNT(vp.id) as v_permits
      FROM visitor_parking_permits as vp
      WHERE vp.created_at='".$date."' AND vp.apartment_id=".$apartment_id)[0]->v_permits;

      $towed_vehicles=DB::select("SELECT COUNT(tv.id)  as vehicles
      FROM towed_vehicles as tv
      WHERE tv.created_at='".$date."' AND tv.apartment_id=".$apartment_id)[0]->vehicles;

      $result["date"]=$date;

      $result["parking_permits_revenue"]=$parking_permit_revenue?$parking_permit_revenue:0;
      
      $result["parking_permits"]=$parking_permits?$parking_permits:0;
      
      $result["visitor_parking_permits"]=$visitor_parking_permits?$visitor_parking_permits:0;
  
      $result["towed_vehicles"]=$towed_vehicles?$towed_vehicles:0;
      
      array_push($statistics,$result);
    }

    return $statistics;
  }

  public function showVehicle($id){
    $findVehicle=Vehicle::find($id);
    return view('admin.vehicles.view',['vehicle'=>$findVehicle]);
  }

  public function switchApartment($apartment){

    $findApartment=Apartment::find($apartment);

    Session::forget('admin_apartment');
    Session::forget('admin_permission');

    // Add new session apartment Id and admin Id
    Session::put('admin_apartment', $findApartment);
    foreach(Auth::guard('admin')->user()->apartments as $admin){

      Session::put('admin_permission',$admin->pivot->permission);
    
    }

    return redirect()->back();

  }

  public function getAnalyticsByDate($date){
  
    $recent_date;
    $result=[];
    
    if($date=="last_3_months"){
      $recent_date=$this->dateRangeGenerator('month',3);
    }
    elseif ($date=="week") {
      $recent_date=$this->dateRangeGenerator('day',7);
    }
    elseif ($date=="today") {
      $recent_date=$this->dateRangeGenerator('day',1);
    }

    $query_parking_permits=$this->analyticsReport('parking_permits',$recent_date);
    $query_visitor_parking_permits=$this->analyticsReport('visitor_parking_permits',$recent_date);
    $query_towed_vehicles=$this->analyticsReport('towed_vehicles',$recent_date);

    foreach($query_parking_permits as $key=>$permit_data){
      $result[]=[
        'date'=>$permit_data['date'],
        'parking_permit'=>$permit_data['count'],
        'visitor_parking_permit'=>$query_visitor_parking_permits[$key]['count'],
        'towed_vehicle'=>$query_towed_vehicles[$key]['count']
      ];
    }
    
    // $result=[
    //   'parking_permit'=>$query_parking_permits,
    //   'visitor_parking_permit'=>$query_visitor_parking_permits,
    //   'towed_vehicle'=>$query_towed_vehicles
    // ];

    return $result;
  }

  protected function getVisitorParkingPermitsByApartmentId($id){

    $visitorParkingPermits=VisitorParkingPermit::where(['apartment_id'=>$id])->get();
    return $visitorParkingPermits;

  }

  protected function getParkingPermitsByApartmentId($id){

    $parkingPermits=ParkingPermit::where(['apartment_id'=>$id])->get();
    return $parkingPermits;

  }

  private function dateRangeGenerator($type,$before){
        
    $dates=[];

    for($i=$before; 1<=$i; $i--){
        $dates[]=[
            'from'=>date('Y-m-d',strtotime('-'.$i.' '.$type)),
            'to'=>$i-1==0?date('Y-m-d'):date('Y-m-d',strtotime('-'.($i-1).' '.$type))
        ];
    }

    return $dates;

  }

  private function analyticsReport($table,$dates){

        $results;

        foreach($dates as $date){

            $query=DB::select("SELECT COUNT(".$table.".id) as count
            FROM ".$table." 
            WHERE (".$table.".updated_at BETWEEN :from 
            AND :to) AND ".$table.".apartment_id=:id",
            [
                'from'=>$date['from'],
                'to'=>$date['to'],
                'id'=>Session::get('admin_apartment')->id
            ]);
    
            $results[]=[
                'date'=>date("Y-m-d",strtotime($date['from'])),
                'count'=>$query[0]->count
            ];
        }

        return $results;

  }

}
