<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class WalletController extends Controller
{
    public function __construct(){

        $this->middleware('auth:admin');
    
    }

    public function index()
    {
        return view('admin.wallets.wallets');
    }
}
