<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Session;
use App\UserNotification;
use App\Events\UserNotificationEvent;

class NotificationController extends Controller
{
    public function __constructor(){

        $this->middleware('auth:admin');
    
    }

    public function index(){

        $users=User::where('apartment_id',Session::get('admin_apartment')->id)->get();

        return view('admin.notifications.notifications',[
            'users'=>$users
        ]);
    
    }

    public function create(Request $request){

        $title=$request->title;
        $message=$request->message;
        $apartment=Session::get('admin_apartment');

        // dd($request->notification_type);

        if($request->notification_type=="all"){
            $notification=new UserNotification();
            $notification->title=$title;
            $notification->message=$message;
            $notification->notification_type="all";
            $notification->apartment_id=$apartment->id;
            $notification->save();

            event(new UserNotificationEvent($notification));
        }
        elseif($request->notification_type=="individual"){
            foreach($request->selected_user as $id){
                $notification=new UserNotification();
                $notification->title=$title;
                $notification->message=$message;
                $notification->notification_type="individual";
                $notification->user_id=$id;
                $notification->apartment_id=$apartment->id;
                $notification->save();

                event(new UserNotificationEvent($notification));
            }
        }

        return redirect()->back();

    }
}
