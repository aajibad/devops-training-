<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use App\AdminCard;
use Session;

use App\Apartment;
use App\SuperAdminOption;
use App\AdminSubscriptionHistory;
use App\Subscription;
use Auth;
use Srmklive\PayPal\Services\ExpressCheckout;

use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\PaymentExecution;

use PayPal\Api\FlowConfig;
use PayPal\Api\InputFields;
use PayPal\Api\WebProfile;
use PayPal\Api\Presentation;

use PayPal\Api\ChargeModel;
use PayPal\Api\Currency;
use PayPal\Api\MerchantPreferences;
use PayPal\Api\PaymentDefinition;
use PayPal\Api\Plan;
use PayPal\Api\Patch;
use PayPal\Api\PatchRequest;
use PayPal\Common\PayPalModel;
use PayPal\Api\Agreement;
use PayPal\Api\ShippingAddress;

class PaymentController extends Controller
{

  protected $apiContext;
  protected $payer;

  public function __construct(){
    
    // ParkingSphere Account as Default PayPal credentials CLIENT_ID / SECRET_ID 
    // $clientID=env("PAYPAL_CLIENT_ID", "AS1vEl8a_KLs5QvITmjr2TfZczNfPwP5rhPGvxz-uzNiJRwtiFcw1Q6nypm6RYlCPn1RrFvxY6k2ll6z");
    // $secretID=env("PAYPAL_CLIENT_SECRET", "EC9b9MwLiT8q2w4umiwZcN6nSByQl8V5RxoJuLZ8g8hUlsCEE5hMv2lqJ-3XTxKSF0WKODoX63XdnYaZ");

    $clientID=env("PAYPAL_CLIENT_ID", "AS1vEl8a_KLs5QvITmjr2TfZczNfPwP5rhPGvxz-uzNiJRwtiFcw1Q6nypm6RYlCPn1RrFvxY6k2ll6z");
    $secretID=env("PAYPAL_CLIENT_SECRET", "EC9b9MwLiT8q2w4umiwZcN6nSByQl8V5RxoJuLZ8g8hUlsCEE5hMv2lqJ-3XTxKSF0WKODoX63XdnYaZ");

    $this->apiContext = new ApiContext(
      new OAuthTokenCredential($clientID,$secretID)
    );

    $this->payer = new Payer();
    $this->payer->setPaymentMethod("paypal");
  }

  public function index(Request $request)
  {
    $apartment=Apartment::find(Session::get('admin_apartment')->id);

    $subscription=Subscription::find($apartment->subscription->id);
    $superAdminOption=new SuperAdminOption();
    
    $findSubscriptionHistory=AdminSubscriptionHistory::where(['apartment_id'=>Session::get('admin_apartment')->id])->get();
    
    $response='';

    if($subscription->auto_subscription && !$subscription->billing_plan_state){
      $request->session()->flash('return','warning');
      $request->session()->flash('message','<b>Action need to be required</b>, Please activate your subscription plan <br>
      <a href="'.route("admin.subscription.plan.activate",["subscription_id"=>$subscription->id]).'" class="btn btn-default" style="margin-top:8px;">Activate Now</a>');
    }

    return view('admin.payments.payments',[
      'subscription_history'=>$findSubscriptionHistory,
      'apartment'=>$apartment
    ]);
  }

  public function upgradeSubscription(Request $request){

    $subscription=Subscription::find($request->subscription_id);

    if($subscription->billing_plan_state){
      try{
  
        $plan = Plan::get($subscription->subscription_plan_id,$this->apiContext);
        $plan->delete($this->apiContext);
      
      }catch(\Exception $e){
      
        return $e->getData();
      
      }
    }

    $subscription->subscription_type=$request->type;
    // $subscription_period=$subscription->getSubscriptionPeriod();
    // $subscription->expiry_date=date('Y-m-d H:m:s',strtotime("+".$subscription_period['interval']." ".strtolower($subscription_period['frequency'])));
    $subscription->expiry_date=null;
    $subscription->billing_plan=0;
    $subscription->billing_plan_state=0;
    $subscription->auto_subscription=0;
    $subscription->payment_definition_id=null;
    $subscription->subscription_plan_id=null;
    $subscription->subscription_agreement_id=null;
    $subscription->payer_email=null;
    $subscription->payer_id=null;

    if($subscription->save()){
      return redirect()->route('admin.payments')->with([
        'return'=>'success',
        'message'=>'<b>Success</b> your plan has been updated.'
      ]);
    }

    return redirect()->route('admin.payments')->with([
      'return'=>'failed',
      'message'=>'<b>Sorry</b> unable to update your plan.'
    ]);

  }

  public function reviewSubscription(Request $request)
  {

    $subscription_id=$request->subscription_id;

    $subscription=Subscription::find($subscription_id);
    
    $response='';

    if($request->payment_type=='confirm'){
      $response=[
        'subscription'=>$subscription,
        'payment_type'=>$request->payment_type,
        'auto_subscription'=>$request->auto_subscription,
        'queries'=>$request,
        'action'=>'Proceed to Confirm Payment',
        'route'=>'admin.subscription.agreement.execute',
        'message'=>'<b>Thankyou!</b> your verification successfull. proceed to confirm.'
      ];
    }
    elseif($request->payment_type=='review'){
      $response=[
        'subscription'=>$subscription,
        'payment_type'=>$request->payment_type,
        'auto_subscription'=>$request->auto_subscription,
        'route'=>'admin.subscription.prepare',
        'action'=>'Pay with PayPal'
      ];
    }

    return view('admin.payments.summary',$response);
  }

  // protected function setSubscription($id){

  //   $subscription= Subscription::find($id);

  //   $data = [];

  //   $data['items'] = [
  //       [
  //           'name'  => str_replace('_',' ',$subscription->subscription_type)." Subscription",
  //           'price' => $subscription->getAmount(),
  //           'qty'   => 1,
  //       ],
  //   ];
    
  //   $data['subscription_desc'] = $subscription->subscription_type." Subscription";
  //   $data['invoice_id'] = strtoupper(uniqid('PAYPAL_')).'_'.$subscription->id;
  //   $data['invoice_description'] = $subscription->subscription_type." Subscription ".$data['invoice_id'];
  //   $data['return_url'] = url(route('admin.subscription.confirm',[
  //     'subscription_id'=>$subscription->id,
  //     'payment_type'=>'confirm'
  //   ]));
  //   $data['cancel_url'] = url('/admin/payment/cancel');
    
  //   $total = 0; 
  //   foreach ($data['items'] as $item) {
  //       $total += $item['price'] * $item['qty'];
  //   }
    
  //   $data['total'] = $total;

  //   return $data;
  
  // }

  // public function readyToCheckoutSubscription(Request $request){

  //   $provider = new ExpressCheckout;  
  //   $data=$this->setSubscription($request->subscription_id);

  //   if(isset($request->auto_subscription) && $request->auto_subscription!=1){
  //     $response=$provider->setExpressCheckout($data);
  //   }
  //   else{
  //     $response=$provider->setExpressCheckout($data,true);
  //   }

  //   // if(!empty($request->auto_subscrription) && !$request->auto_subscrription){
  //   //   $response=$provider->setExpressCheckout($data);
  //   // }
  //   // else{
  //   //   $response=$provider->setExpressCheckout($data,true);
  //   // }

  //   if($response['ACK']=='Success'){
  //     // return redirect($response['paypal_link']);
  //     return $response;
  //   }

  //   return redirect()->back()->with([
  //     'return'=>$response['ACK'],
  //     'message'=>'Unable to make the payment.'
  //   ]);

  // }

  public function readyToCheckoutSubscription(Request $request){

    $subscription=Subscription::find($request->subscription_id);

    $item = new Item();
    $item->setName(str_replace('_',' ',$subscription->subscription_type)." Subscription")
        ->setCurrency('USD')
        ->setQuantity(1)
        ->setPrice($subscription->getAmount());

    $itemList = new ItemList();
    $itemList->setItems(array($item));
    
    $amount = new Amount();
    $amount->setCurrency("USD")
        ->setTotal($subscription->getAmount());
   
    // Create web profile
    $webProfileId=$this->createWebProfile()->getId();

    $transaction = new Transaction();
    $transaction->setAmount($amount)
        ->setItemList($itemList)
        ->setDescription(str_replace('_',' ',$subscription->subscription_type)." subscription payment.")
        ->setInvoiceNumber(uniqid('PS_SUBSCR_').'_'.strtoupper($subscription->id));

    $redirectUrls = new RedirectUrls();
    $redirectUrls->setReturnUrl(url(route('admin.subscription.confirm',[
                  'subscription_id'=>$subscription->id,
                  'payment_type'=>'confirm'
                ])))
                ->setCancelUrl(url('/admin/payment'));

    $payment = new Payment();
    $payment->setIntent("sale")
        ->setPayer($this->payer)
        ->setRedirectUrls($redirectUrls)
        ->setTransactions(array($transaction))
        ->setExperienceProfileId($webProfileId);

    try {
      $payment->create($this->apiContext);
    } catch (Exception $ex) {
      return $ex;
    }

    return response()->json([
      'payment'=>json_decode($payment,true),
      'subscription'=>$subscription
    ]);

  }


  // Create web profile experience
  protected function createWebProfile(){
    $flowConfig = new FlowConfig();
    $flowConfig->setLandingPageType("Billing");
    $flowConfig->setUserAction("commit");

    $presentation = new Presentation();
    $presentation->setBrandName("ParkingSphere.com")
    ->setLocaleCode("US")
    ->setNoteToSellerLabel("Thankyou!");

    $inputFields = new InputFields();
    $inputFields->setAllowNote(true)
      ->setNoShipping(1);

    $webProfile = new WebProfile();
    $webProfile->setName(strtoupper(uniqid('PS_WEBPRO')))
    ->setFlowConfig($flowConfig)
    ->setPresentation($presentation)
    ->setInputFields($inputFields)
    ->setTemporary(true);

    $createProfileResponse = $webProfile->create($this->apiContext);

    return $createProfileResponse;
  }

  public function proceedToCheckoutSubscription(Request $request){

    $paymentId = $request->payment_id;
    $payerId = $request->payer_id;
    $subscriptionId = $request->subscription_id;

    $subscription=Subscription::find($subscriptionId);

    $payment = Payment::get($paymentId, $this->apiContext);

    $execution = new PaymentExecution();
    $execution->setPayerId($payerId);

    $transaction = new Transaction();
    $amount = new Amount();

    $amount->setCurrency('USD');
    $amount->setTotal($subscription->getAmount());
    $transaction->setAmount($amount);

    $execution->addTransaction($transaction);

    try{
      $response=$payment->execute($execution, $this->apiContext);
    }catch(Exception $e){
      return;
    }

    $response_status="";

    if($response->state=='approved'){
      $subscription->status='active';
  
      $expiring_duration="";
      if($subscription->subscription_type=='1_year'){
        $expiring_duration="+1 year";
      }
      elseif($subscription->subscription_type=='3_months'){
        $expiring_duration="+3 months";
      }
      else{
        $expiring_duration="+1 month";
      }
  
      if($subscription->expiry_date>=date('Y-m-d H:m:s')){
        $expiry_date = strtotime($subscription->expiry_date);
        $final_date = date("Y-m-d H:m:s", strtotime($expiring_duration, $expiry_date));
        $subscription->expiry_date=$final_date;
      }
      else{
        $final_date = date("Y-m-d H:m:s", strtotime($expiring_duration));
        $subscription->expiry_date=$final_date;
      }
  
      $subscription->save();

      $response_status="success";
    }
    else{
      $response_status="failed";
    }
    
    $history=$this->savePaymentHistory($subscription,$response);
    $history->save();

    return response()->json([
        'return'=>$response_status,
        'redirect'=>url('admin/payment'),
        'payment'=>json_decode($response,true)
    ]);
    
  }

  // public function proceedToCheckoutSubscription(Request $request){
    
  //   $data=$this->setSubscription($request->subscription_id);

  //   $provider=new ExpressCheckout;
  //   $response = $provider->doExpressCheckoutPayment($data, $request->token, $request->PayerID);
    
  //   if($response['ACK']=='Success'){

  //     $subscription=Subscription::find($request->subscription_id);
  //     $subscription->status='active';

  //     $expiring_duration="";
  //     if($subscription->subscription_type=='1_year'){
  //       $expiring_duration="+1 year";
  //     }
  //     elseif($subscription->subscription_type=='3_months'){
  //       $expiring_duration="+3 months";
  //     }
  //     else{
  //       $expiring_duration="+1 month";
  //     }

  //     // if($subscription->remainingDays('days')>0){
  //     //   // $remaining_time=$subscription->remainingDays('timestamp');
  //     //   // $expiring_time=strtotime("+".$expiring_duration);
  //     //   // $calc_total_time=$remaining_time+$expiring_time;

  //     //   $now=strtotime('now');
  //     //   $expiry=strtotime("+".$expiring_duration);
  //     //   $calc_total_time=$expiry+$now;

  //     //   $subscription->expiry_date=date("Y-m-d H:m:s",$calc_total_time);
  //     // }
  //     // else{
  //     //   $subscription->expiry_date=date("Y-m-d H:m:s",strtotime("+".$expiring_duration));
  //     // }

  //     if($subscription->expiry_date>=date('Y-m-d H:m:s')){
  //       $expiry_date = strtotime($subscription->expiry_date);
  //       $final_date = date("Y-m-d H:m:s", strtotime($expiring_duration, $expiry_date));
  //       $subscription->expiry_date=$final_date;
  //     }
  //     else{
  //       $final_date = date("Y-m-d H:m:s", strtotime($expiring_duration));
  //       $subscription->expiry_date=$final_date;
  //     }

  //     $subscription->save();

  //     $history=$this->savePaymentHistory($subscription,$response);
  //     $history->save();

  //     return redirect()->route('admin.payments')->with([
  //       'return'=>'success',
  //       'message'=>'<b>Success.</b> Payment completed.'
  //     ]);
  //   }
  //   elseif($response['ACK']=='Failure'){
  //     return redirect()->route('admin.payments')->with([
  //       'return'=>'failed',
  //       'message'=>'<b>Sorry !</b> Unable to complete the payment.'
  //     ]);
  //   }

  // }

  public function savePaymentHistory($subscription,$response){
    
    $payment_history=new AdminSubscriptionHistory();
    $payment_history->amount=$subscription->getAmount();
    $payment_history->discount=0;
    $payment_history->total=$payment_history->amount+$payment_history->discount;
    $payment_history->payment_status=$response->state;
    $payment_history->transaction_id=$response->id;
    $payment_history->subscription_type=$subscription->subscription_type;
    $payment_history->apartment_id=$subscription->apartment->id;
    $payment_history->admin_id=$subscription->payer->id;
    $payment_history->subscription_id=$subscription->id;

    return $payment_history;
  }

  public function createBillingPlan(Request $request){
    $subscription=Subscription::find($request->subscription_id);
    
    // Prevent auto subscription on subscription active state
    if(!$subscription->billing_plan){

      $subscription_name=strtoupper(str_replace('_',' ',$subscription->subscription_type));

      $subscription_period=$subscription->getSubscriptionPeriod();

      // Create billing plan
      $plan = new Plan();
      $plan->setName($subscription_name." SUBSCRIPTION")
      ->setDescription('PARKINGSPHERE SUBSCRIPTION PAYMENT')
      ->setType('fixed');

      $currency=new Currency(array('value' => $subscription->getAmount(), 'currency' => 'USD'));

      $paymentDefinition = new PaymentDefinition();
      $paymentDefinition->setName('Regular Subscription Payment')
      ->setType('REGULAR')
      ->setFrequency($subscription_period['frequency'])
      ->setFrequencyInterval($subscription_period['interval'])
      ->setCycles("12")
      ->setAmount($currency);

      $merchantPreferences = new MerchantPreferences();
      $merchantPreferences->setReturnUrl(url(route('admin.subscription.confirm',[
            'subscription_id'=>$subscription->id,
            'payment_type'=>'confirm'
          ])))
      ->setCancelUrl(url('admin/payment'))
      ->setAutoBillAmount("yes")
      ->setInitialFailAmountAction("CONTINUE")
      ->setMaxFailAttempts("0");

      $plan->setPaymentDefinitions(array($paymentDefinition));
      $plan->setMerchantPreferences($merchantPreferences);

      try {
        $response=$plan->create($this->apiContext);
      } catch (Exception $ex) {
        return $ex;
      }    

      $response_data=[
        'return'=>'failed',
        'message'=>'<b>Sorry</b>, something went wrong try again.</a>'
      ];

      if($response->state="CREATED"){
        $subscription->billing_plan=true;
        $subscription->payment_definition_id=$response->payment_definitions[0]->id;
        $subscription->subscription_plan_id=$response->id;
        $subscription->save();

        // Activate the billing plan
        $plan=$this->activateBillingPlan($subscription->id);

        // Create billing agreement
        $agreement=$this->createBillingAgreement($subscription->id);

        // $response_data=[
        //   'return'=>'success',
        //   'message'=>'<b>Success</b>, your auto subscription has been created successfully.'
        // ];

        return redirect($agreement->getApprovalLink());

      }

      return redirect()->back()->with($response_data);
    }
    else{
      $this->activateBillingPlan($request->subscription_id);
      $subscription->auto_subscription=true;
      $subscription->save();

      if($subscription->billing_plan_state=='active'){
        return redirect()->back()->with([
          'return'=>'success',
          'message'=>'<b>Success</b>, auto subscription plan activated successfull'
        ]);
      }
      else{
        return redirect()->back()->with([
          'return'=>'failed',
          'message'=>'<b>Sorry</b>, unable to activate the plan.'
        ]);
      }
    }
    
    return redirect()->back()->with([
      'return'=>'failed',
      'message'=>'<b>Sorry unable to process your request</b>, now you are in active subscription state. please try again after expired.'
    ]);
  }

  public function activateBillingPlan($subscription_id){

    $subscription=Subscription::find($subscription_id);

    $patch = new Patch();

    $value = new PayPalModel('{
        "state":"ACTIVE"
      }');

    $patch->setOp('replace')
        ->setPath('/')
        ->setValue($value);
    $patchRequest = new PatchRequest();
    $patchRequest->addPatch($patch);

    $plan = Plan::get($subscription->subscription_plan_id, $this->apiContext);

    try{
      
      if($plan->state!='ACTIVE'){
        $plan->update($patchRequest,$this->apiContext);
        $subscription->billing_plan_state='active';
        $subscription->save();
      }

    }catch(Exception $e){
      return $e;
    }

  }

  protected function createBillingAgreement($subscription_id){
    
    $subscription=Subscription::find($subscription_id);
    $agreement = new Agreement();

    $start_date='';

    date_default_timezone_set('America/New_York');

    // Set expiry date only for subscription in active state otherwise its set to today date
    if($subscription->getStatus()=='active'){
      $start_date=gmdate("Y-m-d\TH:i:s\Z",strtotime($subscription->expiry_date));;
    }elseif($subscription->getStatus()=='expired'){
      $start_date=gmdate("Y-m-d\TH:i:s\Z",strtotime('+1 day'));
    }else{
      $start_date=gmdate("Y-m-d\TH:i:s\Z",strtotime('+1 day'));
    }

    $agreement->setName(strtoupper(str_replace('_',' ',$subscription->subscription_type).' SUBSCRIPTION AGREEMENT'))
        ->setDescription('ParkingSphere Subscription Payment')
        ->setStartDate($start_date);

    $plan = new Plan();
    $plan->setId($subscription->subscription_plan_id);
    $agreement->setPlan($plan);

    $payer = new Payer();
    $payer->setPaymentMethod('paypal');
    $agreement->setPayer($payer);

    try{
      $agreement->create($this->apiContext);  
    }
    catch(Exception $e){
      return $e;
    }

    return $agreement;
  }

  public function executeBillingAgreement(Request $request){
    
    $token=$request->token;
    $subscription=Subscription::find($request->subscription_id);

    $agreement=new Agreement();
    $agreement->execute($token, $this->apiContext);

    $agreementInfo=Agreement::get($agreement->getId(),$this->apiContext);

    $response=[
      'return'=>'failed',
      'message'=>'<b>Failed.</b> unable to set up your auto subscription.'
    ];

    if($agreementInfo->state="Active"){

      $subscription->auto_subscription=true;
      $subscription->subscription_agreement_id=$agreementInfo->id;
      $subscription->payer_email=$agreementInfo->payer->payer_info->email;
      $subscription->payer_id=$agreementInfo->payer->payer_info->id;
      $subscription->save();

      $response=[
        'return'=>'success',
        'message'=>'<b>Success.</b> your auto subscription has been set successfuly.'
      ];
    }

    return redirect()->route('admin.payments')->with($response);

  }

  public function getBillingPlan($subscription_id){
    $subscription=Subscription::find($subscription_id);
    $plan = Plan::get($subscription->subscription_plan_id, $this->apiContext);
    dd($plan);
  }

  public function cancelBillingPlan(Request $request){
    $subscription=Subscription::find($request->subscription_id);
    try{
      $patch = new Patch();

      $value = new PayPalModel('{
          "state":"INACTIVE"
        }');

      $patch->setOp('replace')
          ->setPath('/')
          ->setValue($value);
      $patchRequest = new PatchRequest();
      $patchRequest->addPatch($patch);

      $plan = Plan::get($subscription->subscription_plan_id, $this->apiContext);
      $plan->update($patchRequest,$this->apiContext);

      $subscription->billing_plan_state='inactive';
      $subscription->auto_subscription=false;
      $subscription->save();

    }catch(Exception $e){
      return $e;
    }

    return redirect()->back()->with([
      'return'=>'success',
      'message'=>'<b>Success</b>, your auto subscription plan has been deactivated successfully.'
    ]);
  }

  // protected function updateBillingPlan($subscription_id){
    
  //   $subscription=Subscription::find($subscription_id);
  //   $subscription_period=$subscription->getSubscriptionPeriod();
  //   $subscription_name=strtoupper(str_replace('_',' ',$subscription->subscription_type));


  //   $plan = Plan::get($subscription->subscription_plan_id, $this->apiContext);
    
  //   try {
  //     $plan->update($patchRequest,$this->apiContext);
  //   }catch(\Exception $e){
  //     return $e->getData();
  //   }
  // }

  public function test(){
    dd(\PayPal\Api\WebhookEventType::availableEventTypes($this->apiContext));
  }

}
