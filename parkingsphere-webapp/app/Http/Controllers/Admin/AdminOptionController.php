<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\AdminOption;
use App\SuperAdminOption;
use App\Apartment;
use App\Subscription;
use App\AdminParkingPermitOption;
use Auth;
use Session;

class AdminOptionController extends Controller
{

  public function __construct(){

    $this->middleware('auth:admin');

  }

  public function index()
  {
    $findApartment=Apartment::find(Session::get('admin_apartment')->id);

    $findFloorPermitOption=AdminParkingPermitOption::where(
      [
        'apartment_id'=>$findApartment->id,
        'permit_type'=>'floor'
      ])
      ->orderBy('created_at','DESC')
      ->first();

    $findBuildingPermitOption=AdminParkingPermitOption::where(
      [
        'apartment_id'=>$findApartment->id,
        'permit_type'=>'building'
      ])
      ->orderBy('created_at','DESC')
      ->first();

    $superAdminOption=Subscription::where('apartment_id',$findApartment->id)->get()->first();
    $findOption=AdminOption::where('apartment_id',$findApartment->id)
                            ->orderBy('created_at','DESC')
                            ->first();

    return view('admin.settings.settings',[
      'option'=>$findOption,
      'super_admin_option'=>$superAdminOption,
      'apartment'=>$findApartment,
      'floor_permit'=>$findFloorPermitOption,
      'building_permit'=>$findBuildingPermitOption
    ]);

  }

  public function create(Request $request)
  {

    $findApartment=Apartment::find(Session::get('admin_apartment')->id);

    $findAdminOption=AdminOption::where('apartment_id',Session::get('admin_apartment')->id);
    
    // if(!$findAdminOption->get()->isEmpty()){

    //   $adminOption=AdminOption::find($findAdminOption->first()->id);

    //   $adminOption->no_of_floor_permit=$request->no_of_floor_permit;
    //   $adminOption->no_of_building_permit=$request->no_of_building_permit;
    //   $adminOption->amount_permit_monthly=$request->amount_permit_monthly;
    //   $adminOption->amount_permit_yearly=$request->amount_permit_yearly;
    //   $adminOption->max_visitor_permit=$request->max_visitor_permit;
    //   $adminOption->max_visitor_permit_duration=$request->max_visitor_permit_duration;
    //   $adminOption->apartment_parking_policy=$request->apartment_parking_policy;
    
    //   $adminOption->save();
    // }
    // else{
    //   $adminOption=new AdminOption;

    //   $adminOption->no_of_floor_permit=$request->no_of_floor_permit;
    //   $adminOption->no_of_building_permit=$request->no_of_building_permit;
    //   $adminOption->amount_permit_monthly=$request->amount_permit_monthly;
    //   $adminOption->amount_permit_yearly=$request->amount_permit_yearly;
    //   $adminOption->max_visitor_permit=$request->max_visitor_permit;
    //   $adminOption->max_visitor_permit_duration=$request->max_visitor_permit_duration;
    //   $adminOption->apartment_parking_policy=$request->apartment_parking_policy;

    //   $adminOption->save();

    //   $adminOption->apartment()->associate($findApartment);
    //   $adminOption->save();
    // }
    
    $adminOption=new AdminOption;
    $adminOption->amount_permit_monthly=$request->amount_permit_monthly;
    $adminOption->discount_permit_monthly=$request->discount_permit_monthly;
    $adminOption->amount_permit_yearly=$request->amount_permit_yearly;
    $adminOption->discount_permit_yearly=$request->discount_permit_yearly;
    $adminOption->max_visitor_permit=$request->max_visitor_permit;
    $adminOption->max_visitor_permit_duration=$request->max_visitor_permit_duration;
    $adminOption->apartment_parking_policy=$request->apartment_parking_policy;
    $adminOption->apartment()->associate($findApartment->id);
    // dd($adminOption);
    $adminOption->save();

    return redirect()->action('Admin\AdminOptionController@index');
  }

  public function addParkingPermit(Request $request){
    $findOption=AdminParkingPermitOption::where(
      [
        'apartment_id'=>Session::get('admin_apartment')->id,
        'permit_type'=>$request->permit_type
      ])
      ->orderBy('created_at','DESC')
      ->first();

      // dd($findOption);

    if($findOption==null){
      $adminParkingPermitOption=new AdminParkingPermitOption();
      $adminParkingPermitOption->total_permit=$request->total_permit;
      $adminParkingPermitOption->available_permit=$request->total_permit;
      $adminParkingPermitOption->permit_type=$request->permit_type;
      $adminParkingPermitOption->apartment()->associate(Session::get('admin_apartment')->id);
      $adminParkingPermitOption->admin()->associate(Auth::guard('admin')->user()->id);
      $adminParkingPermitOption->save();
    }
    else{
      $adminParkingPermitOption=new AdminParkingPermitOption();
      $adminParkingPermitOption->total_permit=$request->total_permit;
      $adminParkingPermitOption->available_permit=(int)$findOption->available_permit+(int)$request->total_permit;
      $adminParkingPermitOption->permit_type=$request->permit_type;
      $adminParkingPermitOption->apartment()->associate(Session::get('admin_apartment')->id);
      $adminParkingPermitOption->admin()->associate(Auth::guard('admin')->user()->id);
      $adminParkingPermitOption->save();
    }

    return redirect()->back();
  }

  public function subscription($type)
  {
      $findApartment=subscription::find(Session::get('admin_apartment')->id);
      $findApartment->subscription_type=$type;
      $findApartment->save();

      return redirect()->back();
  }

  public function showSetParkingPermitHistory($permit_type){
    $findOption=AdminParkingPermitOption::where(
      [
        'apartment_id'=>Session::get('admin_apartment')->id,
        'permit_type'=>$permit_type
      ])
      ->orderBy('created_at','DESC')
      ->get();

    return view('admin.settings.show_set_parking_permit_history',[
      'histories'=>$findOption
    ]);
  }

  public function update()
  {
      return;
  }

  public function delete()
  {
      // $findCard=Card:find($request->id);
      // $findCard->delete();
      return;
  }

  public function testApi(Request $request){
    $name=$request->name;
    $email=$request->email;
  }

}
