<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\VehicleImage;

class VehicleImageController extends Controller
{
    public function getImages($id){

        $vehicleImages=VehicleImage::find($id);

        return json_encode($vehicleImages);

    }
}
