<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\TowingCompany;
use App\TowedVehicle;
use Session;

class TowedVehicleController extends Controller
{
    public function __construct(){
        
        $this->middleware('auth:admin');

    }

    public function index(){
        $towed_vehicles=TowedVehicle::where(['apartment_id'=>Session::get('admin_apartment')->id])->get();
        $towing_companies=TowingCompany::where(['apartment_id'=>Session::get('admin_apartment')->id])->get();

        return view('admin.towed_vehicles.towed_vehicles',[
            'towed_vehicles'=>$towed_vehicles,
            'towing_companies'=>$towing_companies
        ]);

    }

    public function create(Request $request){
        
        $apartmentId=Session::get('admin_apartment')->id;

        // dd($request->contract->extension());
        if($request->contract->extension()=="pdf" || $request->contract->extension()=="docx" || $request->contract->extension()=="doc"){
            $towing_company=new TowingCompany();
            $towing_company->name=$request->name;
            $towing_company->address=$request->address;
            $towing_company->phone=$request->phone;

            $contractExt=$request->contract->extension();
            $contractFileName=str_replace(' ','-',$towing_company->name)."-CONTRACT-AP-".$apartmentId.".".$contractExt;

            $towing_company->contract=$request->contract->storeAs('contracts',$contractFileName,'app_public');

            $towing_company->apartment()->associate($apartmentId);
            $towing_company->save();

            return redirect()->back()->with([
                "return"=>"success",
                "message"=>"<b>Success</b>, towing company saved successfully."
            ]);   
        }

        return redirect()->back()->with([
            "return"=>"failed",
            "message"=>"<b>Failed</b>, file extension missmatched. <br>Please upload your file as following extensions : <b>.pdf .doc .docx</b>"
        ]);
    }

    public function update(Request $request){
        
        $towing_company=TowingCompany::find($request->id);
        $towing_company->name=$request->name;
        $towing_company->address=$request->address;
        $towing_company->phone=$request->phone;
        $towing_company->save();

        return redirect()->back();
    }

    public function delete($id){
        
        $towing_company=TowingCompany::find($id);
        $towing_company->delete();
        
        return redirect()->back()->with([
            'return'=>'success',
            'message'=>'<b>Success</b> towing company deleted successfully.'
        ]);
    }


}
