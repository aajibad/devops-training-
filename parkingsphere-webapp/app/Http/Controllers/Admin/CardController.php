<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\AdminCard;
use Session;
use Auth;

class CardController extends Controller
{
    public function __construct(){

        $this->middleware('auth:admin');
    
    }

    public function index()
    {
        $cards=AdminCard::where(['user_id'=>Auth::guard('admin')->user()->id])->get();
        return view('admin.cards.cards',['cards'=>$cards]);
    }
  
    public function create(Request $request)
    {
      $card=new AdminCard;
      $card->card_holder=$request->card_holder;
      $card->card_type=$request->card_type;
      $card->card_number=$request->card_number;
      $card->expiry=date('Y-m-t',mktime(0,0,0,1,$request->expiry_month,$request->expiry_year));
      $card->cvv=$request->card_cvv;
      $card->save();
  
      $card->admin()->associate(Auth::guard('admin')->user()->id);
      $card->save();
  
      return redirect()->route('admin.cards');
    }
  
    public function update(Request $request)
    {
      $card=AdminCard::find($request->id);
      $card->auto_payment=$request->auto_payment=='on'?1:0;
      $card->save();
  
      return redirect()->route('admin.cards');
    }
  
    public function delete(Request $request)
    {
        $findCard=AdminCard::find($request->id);
        $findCard->delete();
        
        return redirect()->route('admin.cards');
    }
}
