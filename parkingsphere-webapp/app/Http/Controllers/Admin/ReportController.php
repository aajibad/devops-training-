<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Collection;

use App\User;
use App\ParkingPermit;
use App\VisitorParkingPermit;
use App\Vehicle;
use App\TowedVehicle;
use App\AdminParkingPermitOption;

use Session;

class ReportController extends Controller
{
    public function __construct(){

        $this->middleware('auth:admin');

    }

    public function index(){

        $users=User::all();

        return view('admin.reports.create',[
            'users'=>$users
        ]);
    }

    protected $dates;

    public function generateReport(Request $request){
        
        $apartment_id=Session::get('admin_apartment')->id;
        $report_type=$request->report_type;
        $date_range=$request->date_range;
        $selected_user=$request->selected_user;
        $all_users=$request->all_users;
        $range_selector=$request->range_selector;
        
        $this->dates=[
            'month'=>date('Y-m-d',strtotime('-1 month')),
            'week'=>date('Y-m-d',strtotime('-7 day')),
            'today'=>date('Y-m-d',strtotime('-1 day'))
        ];
        
        $from;
        $to;
        $select_from;
        $query;
        $table;
        $user_type;

        switch ($date_range) {
            case 'month':
                $select_from=$this->dates[$date_range];
                break;
            case 'week':
                $select_from=$this->dates[$date_range];
                break;
            case 'today':
                $select_from=$this->dates[$date_range];
                break;   
        }

        if($range_selector){
            $from=$request->date_from;
            $to=$request->date_to;
        }
        else {
            $from=$select_from;
            $to=date('Y-m-d');   
        }

        if($report_type!='towed_vehicle'){
            if($all_users){
                $user_type='all';
            }
            elseif(sizeof($selected_user)>1){
                $user_type='multi';
            }
            elseif(sizeof($selected_user)==1){
                $user_type='single';
            }
        }

        $data;

        if($report_type=='parking_permit'){
            $table='parking_permits';

            // if($all_users){
            //     $query=$this->reportParkingPermit($request,$from,$to);
            // }
            // else{
            //     $query=$this->reportParkingPermit($request,$from,$to,false);
            // }

            $query=$this->reportParkingPermit($request,$from,$to,$all_users);

            $data["revenue"]=$this->parkingPermit($from,$to,$apartment_id,$user_type,$selected_user,"general","revenue");
            $data["permit"]=$this->parkingPermit($from,$to,$apartment_id,$user_type,$selected_user,"general","permit");
            $data["floor_permit"]=$this->parkingPermit($from,$to,$apartment_id,$user_type,$selected_user,"building","permit");
            $data["building_permit"]=$this->parkingPermit($from,$to,$apartment_id,$user_type,$selected_user,"floor","permit");
            $data["total_building_permit"]=AdminParkingPermitOption::getAvailablePermit("building");
            $data["total_floor_permit"]=AdminParkingPermitOption::getAvailablePermit("floor");
            
            // $term=$this->parkingPermit($from,$to,$apartment_id,'term');
            // $spot=$this->parkingPermit($from,$to,$apartment_id,'spot');

            // $parking_permit_revenue=$this->parkingPermitRevenue($recent_date);
            // $count_parking_permit_floor=$this->parkingPermitCount($recent_date,'floor',false);
            // $count_parking_permit_building=$this->parkingPermitCount($recent_date,'building',false);

        }
        elseif ($report_type=='visitor_parking_permit') {
            $table='visitor_parking_permits';

            $query=$this->reportVisitorParkingPermit($request,$from,$to,$all_users);
            $data["permit"]=$this->visitorParkingPermit($from,$to,$apartment_id,$user_type,$selected_user);
            $data["floor_permit"]=$this->visitorParkingPermit($from,$to,$apartment_id,$user_type,$selected_user,"building");
            $data["building_permit"]=$this->visitorParkingPermit($from,$to,$apartment_id,$user_type,$selected_user,"floor");

            // $no_of_visitor_parking_permit=$this->visitorParkingPermitCount($recent_date,'floor');
            // $count_visitor_parking_permit_floor=$this->visitorParkingPermitCount($recent_date,'floor',false);
            // $count_visitor_parking_permit_building=$this->visitorParkingPermitCount($recent_date,'building',false);

        }
        elseif ($report_type=='towed_vehicle') {
            $table='towed_vehicles';
            
            $query=$this->reportTowedVehicle($request,$from,$to);
            $data["vehicle"]=$this->towedVehicles($from,$to,$apartment_id);
            $data["user_vehicle"]=$this->towedVehicles($from,$to,$apartment_id,"user");
            $data["visitor_vehicle"]=$this->towedVehicles($from,$to,$apartment_id,"visitor");

            // $count_total_vehicles=Vehicle::all()->count();
            // $no_of_towed_vehicles=$this->towedVehicleCount($recent_date);
            // $no_of_towed_vehicles_with_date=$this->towedVehicleCount($recent_date,false);

        // foreach($no_of_visitor_parking_permit as $data){
        //     dd($data['date']);
        // }
        // dd($query);
        // dd($count_parking_permit_floor);
        // dd($parking_permit_revenue);

        // dd($no_of_visitor_parking_permit); 
        // dd($no_of_visitor_parking_permit);

        }

        return view('admin.reports.show',[
            'date_from'=>$from,
            'date_to'=>$to,
            'date_range'=>$date_range,
            'query'=>$query,
            'table'=>$table,
            'data'=>$data,
            'user_type'=>$report_type!='towed_vehicle'?$user_type:''
        ]);

    }

    private function reportParkingPermit($request,$from,$to,$all_user=true){
        
        $query;

        if($all_user){
            $query=ParkingPermit::whereBetween('created_at', array($from, $to))->get();
        }
        else{
            $query=ParkingPermit::whereBetween('created_at', array($from, $to))
                            ->whereIn('user_id',$request->selected_user)
                            ->get();
        }

        return $query;

    }

    private function reportVisitorParkingPermit($request,$from,$to,$all_user=true){

        $query;

        if($all_user){
            $query=VisitorParkingPermit::whereBetween('created_at', array($from, $to))->get();
        }
        else{
            $query=VisitorParkingPermit::whereBetween('created_at', array($from, $to))
                            ->whereIn('generated_by',$request->selected_user)
                            ->get();
        }

        return $query;

    }

    private function reportTowedVehicle($request,$from,$to){

        $query;
        
        $query=TowedVehicle::whereBetween('created_at', array($from,$to))->get();

        return $query;

    }

    /** Generate date range for given specific type before date
     *  @param string $type 'month'/'days'/'week'
     *  @param int $before 
     *  @return array ['from'=>'','to'=>'']
     */
    private function dateRangeGenerator($type,$before){
        
        $dates=[];

        for($i=$before; 1<=$i; $i--){
            $dates[]=[
                'from'=>date('Y-m-d',strtotime('-'.$i.' '.$type)),
                'to'=>$i-1==0?date('Y-m-d'):date('Y-m-d',strtotime('-'.($i-1).' '.$type))
            ];
        }

        return $dates;

    }

    // private function parkingPermitRevenue($dates){

    //     $results;

    //     foreach($dates as $date){

    //         $query=DB::select("SELECT SUM(parking_permits.payment_amount) as revenue
    //         FROM parking_permits 
    //         WHERE parking_permits.updated_at BETWEEN :from 
    //         AND :to",
    //         [
    //             'from'=>$date['from'],
    //             'to'=>$date['to']
    //         ]);
    
    //         $results[]=[
    //             'date'=>date("Y-m-d",strtotime($date['from'])),
    //             'revenue'=>$query[0]->revenue
    //         ];
    //     }

    //     return $results;

    // }

    // private function parkingPermitCount($dates,$parking_spot,$is_range=true){

    //     $results;
    //     $count=0;
    //     $final_result;

    //     foreach($dates as $date){

    //         $query=DB::select("SELECT COUNT(parking_permits.id) as count
    //         FROM parking_permits 
    //         WHERE parking_permits.updated_at BETWEEN :from 
    //         AND :to AND parking_permits.parking_spot=:parking_spot",
    //         [
    //             'from'=>$date['from'],
    //             'to'=>$date['to'],
    //             'parking_spot'=>$parking_spot
    //         ]);
    
    //         $results[]=[
    //             'date'=>date("Y-m-d",strtotime($date['from'])),
    //             'count'=>$query[0]->count
    //         ];

    //         $count+=$query[0]->count;
    //     }

    //     if($is_range){
    //         $final_result=$results;
    //     }
    //     else{
    //         $final_result=$count;
    //     }

    //     return $final_result;

    // }

    // private function visitorParkingPermitCount($dates,$parking_spot,$result=true){

    //     $results;
    //     $count=0;
    //     $final_result;

    //     foreach($dates as $date){

    //         $query=DB::select("SELECT COUNT(visitor_parking_permits.id) as count
    //         FROM visitor_parking_permits 
    //         WHERE visitor_parking_permits.created_at BETWEEN :from 
    //         AND :to AND visitor_parking_permits.parking_spot=:parking_spot",
    //         [
    //             'from'=>$date['from'],
    //             'to'=>$date['to'],
    //             'parking_spot'=>$parking_spot
    //         ]);
    
    //         $results[]=[
    //             'date'=>date("Y-m-d",strtotime($date['from'])),
    //             'count'=>$query[0]->count
    //         ];

    //         $count+=$query[0]->count;
    //     }

    //     if($result){
    //         $final_result=$results;
    //     }
    //     else{
    //         $final_result=$count;
    //     }

    //     return $final_result;

    // }

    // private function towedVehicleCount($dates,$is_countable=true){

    //     $results;
    //     $count=0;
    //     $final_result;

    //     foreach($dates as $date){

    //         $query=DB::select("SELECT COUNT(towed_vehicles.id) as count
    //         FROM towed_vehicles 
    //         WHERE towed_vehicles.created_at BETWEEN :from 
    //         AND :to",
    //         [
    //             'from'=>$date['from'],
    //             'to'=>$date['to']
    //         ]);
    
    //         $results[]=[
    //             'date'=>date("Y-m-d",strtotime($date['from'])),
    //             'count'=>$query[0]->count
    //         ];

    //         $count+=$query[0]->count;
    //     }

    //     if($is_countable){
    //         $final_result=$count;
    //     }
    //     else{
    //         $final_result=$results;
    //     }

    //     return $final_result;

    // }

    protected function parkingPermit($from,$to,$apartment_id,$user_type,$user,$revenue_type="general",$mode="revenue"){

        $additional;
        $fetch;
        $query;

        if($revenue_type=="monthly" || $revenue_type=="yearly"){
            $additional="AND pp.payment_term='".$revenue_type."'";
        }
        elseif ($revenue_type=="building" || $revenue_type=="floor") {
            $additional="AND pp.parking_spot='".$revenue_type."'";
        }else{
            $additional="";
        }

        if($mode=="revenue"){
            $fetch="SUM(pp.payment_amount) as revenue";
        }elseif($mode=="permit"){
            $fetch="COUNT(pp.id) as permit";
        }

        if($user_type=="multi" || $user_type=="single"){
            $query=DB::select("SELECT ".$fetch.", pp.created_at as date FROM parking_permits as pp 
            WHERE pp.created_at BETWEEN '".$from."' AND '".$to."' AND pp.apartment_id=".$apartment_id." ".$additional." 
            AND pp.user_id IN(".implode(",",$user).") GROUP BY pp.created_at");

        }
        elseif($user_type=="all"){
            $query=DB::select("SELECT ".$fetch.", pp.created_at as date FROM parking_permits as pp 
            WHERE pp.created_at BETWEEN '".$from."' AND '".$to."' AND pp.apartment_id=".$apartment_id." ".$additional."
            GROUP BY pp.created_at");
        }

        return $query;
    }

    protected function visitorParkingPermit($from,$to,$apartment_id,$user_type,$user,$spot_type=""){

        $additional;
        $fetch;
        $query;

        if ($spot_type=="building" || $spot_type=="floor") {
            $additional="AND vp.parking_spot='".$spot_type."'";
        }else{
            $additional="";
        }

        if(true){
            $fetch="COUNT(vp.id) as permit";
        }

        if($user_type=="multi" || $user_type=="single"){
            $query=DB::select("SELECT ".$fetch.", vp.created_at as date FROM visitor_parking_permits as vp 
            WHERE vp.created_at BETWEEN '".$from."' AND '".$to."' AND vp.apartment_id=".$apartment_id." ".$additional." 
            AND vp.generated_by IN(".implode(",",$user).") GROUP BY vp.created_at");
        }
        elseif($user_type=="all"){
            $query=DB::select("SELECT ".$fetch.", vp.created_at as date FROM visitor_parking_permits as vp 
            WHERE vp.created_at BETWEEN '".$from."' AND '".$to."' AND vp.apartment_id=".$apartment_id." ".$additional."
            GROUP BY vp.created_at");
        }

        return $query;
    }

    protected function towedVehicles($from,$to,$apartment_id,$vehicle_owner=""){

        $additional;
        $fetch;
        $query;

        if ($vehicle_owner=="user" || $vehicle_owner=="visitor") {
            $additional="AND tv.user_type='".$vehicle_owner."'";
        }else{
            $additional="";
        }

        if(true){
            $fetch="COUNT(tv.id) as vehicle";
        }
        
        $query=DB::select("SELECT ".$fetch.", tv.created_at as date FROM towed_vehicles as tv 
        WHERE tv.created_at BETWEEN '".$from."' AND '".$to."' AND tv.apartment_id=".$apartment_id." ".$additional."
        GROUP BY tv.created_at");

        return $query;

    }
}
