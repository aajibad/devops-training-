<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Apartment;
use App\AdminApartment;
use App\Subscription;
use App\Admin;
use Session;
use Auth;

class SetupController extends Controller
{

    public function index()
    {
        if(Auth::guard('admin')->user()->status!="CREATED"){
            return redirect()->route('admin.home');
        }
        return view('admin.setup');
    }
  
    public function create(Request $request)
    {

        if(!$request->accept_terms){
            return redirect()->back()->with([
                'return'=>'failed',
                'message'=>'<b>Failed</b>,Could not submit your information without accepted terms and conditions.'
            ]);
        }

        $admin=Auth::guard('admin')->user();

        $formatLower=strtolower($request->apartment_name);
        $formatAartmentName=str_replace(' ','_',$formatLower);
        $fileExtension=$request->apartment_logo->extension();

        $findAdmin=Admin::find($admin->id);
        
        $apartment=new Apartment();
        $apartment->name=$request->apartment_name;
        $apartment->logo=$request->apartment_logo->storeAs('images/apartment_banners','logo_'.$admin->id.'_'.$formatAartmentName.'.'.$fileExtension);
        $apartment->phone=$request->apartment_phone;
        $apartment->address=$request->apartment_address;
        $apartment->save();

        $apartment->admins()->attach($findAdmin,['permission'=>'read_write']);
        $apartment->save();

        $subscription=new Subscription();
        $subscription->subscription_type=$request->subscription;
        $subscription->payer()->associate($findAdmin);
        $subscription->apartment()->associate($apartment);
        $subscription->save();

        $findAdminApartment=AdminApartment::where('admin_id',Auth::guard('admin')->user()->id)->first();
    
        Session::forget('admin_apartment');
        Session::forget('admin_permission');
 
        //  Add new session apartment Id and admin Id
        Session::put('admin_apartment', $apartment);

        foreach(Auth::guard('admin')->user()->apartments as $admin){
 
           Session::put('admin_permission',$admin->pivot->permission);

        }

        $findAdmin->status='ACTIVE';
        $findAdmin->save();
      
        return redirect()->route('admin.home');
    }
  
}
