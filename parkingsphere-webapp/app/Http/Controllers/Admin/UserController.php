<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Admin;
use App\User;
use App\Apartment;
use App\AdminApartment;
use App\Subscription;
use App\ParkingPermit;
use App\VisitorParkingPermit;
use App\Vehicle;
use App\VisitorVehicle;
use JWTAuth;
use Auth;
use Session;
use DB;

class UserController extends Controller
{

//   public function __construct(){

//     $this->middleware('auth:admin');

//   }

  public function index()
  {
      $apartment_id=Session::get('admin_apartment')->id;
      // $user=Auth::user();
      $apartment=Apartment::find($apartment_id);

      return view('admin.users.users',[
          'apartment'=>$apartment
        ]);
  }

  public function show()
  {
      $findUser=Admin::find(Auth::user()->id);

      return view('admin.users.show',['user'=>$findUser]);
  }

  public function createApartment(Request $request)
  { 
    $admin=Admin::find($request->admin);

    $formatLower=strtolower($request->apartment_name);
    $formatAartmentName=str_replace(' ','_',$formatLower);
    $fileExtension=$request->apartment_logo->extension();
    
    $apartment=new Apartment();
    $apartment->name=$request->apartment_name;
    $apartment->logo=$request->apartment_logo->storeAs('images/apartment_banners','logo_'.$admin->id.'_'.$formatAartmentName.'.'.$fileExtension);
    $apartment->phone=$request->apartment_phone;
    $apartment->address=$request->apartment_address;
    $apartment->subscription_type=$request->subscription;
    $apartment->save();

    $findApartment=Apartment::find($apartment->id);
    $findApartment->admins()->attach($admin,['permission'=>'read_write']);
    $findApartment->save();

    $subscription=new Subscription();
    $subscription->amount='';
    $subscription->payer()->associate($admin->id);
    $subscription->apartment()->associate($apartment);
    $subscription->save();
    
    // $subscription=new Subscription();
    // $subscription->apartment()->associate($findApartment->id);
    // $subscription->save();

    return redirect()->back();
  }

  public function createAdmin(Request $request)
  { 
    // Find Apartmeent instance
    $findApartment=Apartment::find(Session::get('admin_apartment')->id);
    //Validates data
    $this->validator($request->all())->validate();
    
    //Create admin
    $admin = $this->create($request->all());

    $findAdmin=Admin::find($admin->id);
    $findAdmin->apartments()->attach($findApartment,['permission'=>$request->permission]);
    $findAdmin->save();

    return redirect()->route('admin.users');
  }

  public function createUser(Request $request)
  { 
    // Find Apartmeent instance
    $findApartment=Apartment::find(Session::get('admin_apartment')->id);
    //Validates data
    $this->validator($request->all())->validate();
    
    //Create user
    $user=new User();
    $user->name=$request['name'];
    $user->email=$request['email'];
    $user->password=bcrypt($request['password']);
    $user->apartment()->associate($findApartment->id);
    $user->save();

    return redirect()->back();
  }

  public function update(Request $request)
  {
      
    $admin=Admin::find(Auth::guard('admin')->user()->id);
    if(!is_null($request->avatar)){
        $fileExtension=$request->avatar->extension();
        $admin->avatar=$request->avatar->storeAs('images/admin/avatar','avatar_'.$admin->id.'.'.$fileExtension);
    }
    $admin->firstname=$request->firstname;
    $admin->lastname=$request->lastname;
    $admin->address=$request->address;
    $admin->phone=$request->phone;
    $admin->save();

    return redirect()->back();
  }

  public function delete()
  {
      // $findCard=Card:find($request->id);
      // $findCard->delete();
      return;
  }

//   public function changePermission(Request $request){
//     //   dd($request);
//     $admin=AdminApartment::where("admin_id",$request->id)->update(['permission' => $request->permission]);
//         return redirect()->back();
//   }

  public function listApprovals(){
    $findAllApprovals=User::where('apartment_id',Session::get('admin_apartment')->id)
    ->where('status','pending')->get();

    return view('admin.users.user_approvals',[
        'approvals'=>$findAllApprovals
    ]);
  }

  public function showUserHistory($id){
    
    $apartment_id=Session::get('admin_apartment')->id;

    $permits=ParkingPermit::where([
        'apartment_id'=>$apartment_id,
        'user_id'=>$id
        ])->get();

    $v_permits=VisitorParkingPermit::where([
        'apartment_id'=>$apartment_id,
        'generated_by'=>$id
        ])->get();

    return view('admin.users.history',[
        'permits'=>$permits,
        'v_permits'=>$v_permits
    ]);

  }

  public function approveUser(Request $request){
    
    $findUser=User::find($request->id);
    
    if($findUser->status=='pending' || $findUser->status=='blocked'){
        $findUser->status='active';
        $findUser->save();
    }
    elseif ($findUser->status=='active') {
        $findUser->status='blocked';
        $findUser->save();
    }

    return redirect()->back();
  }

  //Validates user's Input
  protected function validator(array $data)
  {
      return Validator::make($data, [
           'name' => 'required|max:255',
           'email' => 'required|email|max:255',
           'password' => 'required|min:6|confirmed',
      ]);
  }

  //Create a new admin instance after a validation.
  protected function create(array $data)
  {
      return Admin::create([
          'name' => $data['name'],
          'email' => $data['email'],
          'password' => bcrypt($data['password']),
      ]);
  }

  public function logout(Request $request) {
    Auth::guard('admin')->logout();
    return redirect('/admin/login');
  }

  public function authenticateTokenUser(Request $request){
    try {

		if (! $user = JWTAuth::parseToken()->authenticate()) {
			return response()->json(['user_not_found'], 404);
		}

	} catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

		return response()->json(['token_expired'], $e->getStatusCode());

	} catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

		return response()->json(['token_invalid'], $e->getStatusCode());

	} catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

		return response()->json(['token_absent'], $e->getStatusCode());

	}

	// the token is valid and we have found the user via the sub claim
	return response()->json([
        'message'=>'authenticated',
        'user'=>$user
    ]);
  }

    public function invalidateTokenUser(Request $request){
      
        JWTAuth::parseToken()->invalidate();

        return response()->json([
            'message'=>'success'
        ]);
    }

    public function searchVehicle(Request $request){
        $rules =[
            'license_no' => 'required',
            'apartment_id'=> 'required'
        ];

        $validator=Validator::make($request->all(),$rules);

        if($validator->fails()){
            return response()->json($validator->errors());
        }

        $vehicle=Vehicle::where('license_no','LIKE',$request->license_no.'%')
        ->where('apartment_id',$request->apartment_id)
        ->with('apartment','user','images','parkingPermit')->get();
        
        // $vehicle=DB::table('vehicles')->where('license_no','LIKE',$request->license_no.'%')
        // ->where('apartment_id',$request->apartment_id)->get();

        $visitor=VisitorVehicle::where('license_no','LIKE',$request->license_no.'%')
        ->where('apartment_id',$request->apartment_id)
        ->with('apartment','images','visitorPermit')->get();
        
        if(!$vehicle && !$visitor){
            return response()->json(['return'=>'failed','message'=>'No vehicles found']);
        }

        return response()->json([
            'return'=>'success',
            'data'=>[
                ['vehicle_type'=>'user','vehicle'=>$vehicle],
                ['vehicle_type'=>'visitor','vehicle'=>$visitor],
            ]
        ]);

                
    }
}
