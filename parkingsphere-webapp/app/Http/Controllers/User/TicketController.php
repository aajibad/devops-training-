<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;

use Validator;
use Auth;
use Braintree;

use App\Apartment;
use App\ParkingPermit;
use App\VisitorParkingPermit;
use App\Vehicle;
use App\AdminOption;
use JWTAuth;

class TicketController extends Controller
{
    private $gateway;

    public function __construct(){
        // $this->gateway = new Braintree_Gateway([
        //     'environment' => env("BRAINTREE_ENV"),
        //     'merchantId' => env("BRAINTREE_MERCHANT_ID"),
        //     'publicKey' => env("BRAINTREE_PUBLIC_KEY"),
        //     'privateKey' => env("BRAINTREE_PRIVATE_KEY")
        // ]);
    }

    public function create(Request $request) {
        
        $validator=Validator::make($request->all(),$this->rules());
        
        if($validator->fails()){
            return response()->json($validator->errors());
        }

        $apartment=Apartment::find($request->apartment_id);

        $admin_option=AdminOption::where('apartment_id',$apartment->id)->get()->first();

        // if(AdminOption::where('apartment_id',$apartment->id)->isEmpty()){
        //     return response()->json(['message'=>'permit currently unavailable.']);
        // }   
        
        if($apartment->status=="active" && (!is_null($admin_option->amount_permit_monthly) && !is_null($admin_option->amount_permit_yearly))){
            $vehicle=new Vehicle();
            $vehicle->model=$request->model;
            $vehicle->color=$request->color;
            $vehicle->license_no=$request->license_no;
            $vehicle->apartment()->associate($request->apartment_id);
            $vehicle->save();
    
            $parking_permit=new ParkingPermit();
            $parking_permit->parking_spot=$request->parking_spot;
            $parking_permit->parking_spot_meta=$request->parking_spot_meta;
            $parking_permit->payment_term=$request->payment_term;
            $parking_permit->status='pending';
    
            if($parking_permit->payment_term=='monthly'){
                $parking_permit->expired_on=date('Y-m-d',strtotime('+1 month'));
                $parking_permit->payment_amount=$admin_option->amount_permit_monthly;
            }
            elseif($parking_permit->payment_term=='yearly'){
                $parking_permit->expired_on=date('Y-m-d',strtotime('+12 month'));
                $parking_permit->payment_amount=$admin_option->amount_permit_yearly;
            }

            $parking_permit->apartment()->associate($apartment->id);
            $parking_permit->vehicle()->associate($vehicle->id);
            $parking_permit->user()->associate(Auth::user()->id);
            
            if(!$parking_permit->save()){
                return response()->json([
                    'message'=>'unsuccessfull'
                ]);
            }

            return response()->json([
                'message'=>'success',
                'data'=>$parking_permit
            ]);
        }

        return response()->json([
            'message'=>'apartment currently unavailable.'
        ]);



    }

    public function generate(){

    }

    public function renew(){

    }

    function listPermits(){
        
        $permit=ParkingPermit::where("user_id",JWTAuth::user()->id)->with(["apartment","vehicle"])->get();
        if(!$permit){
            return response()->json([
                "message"=>"no permits found."
            ]);
        }

        return response()->json($permit);
    }

    protected function rules(){
        return [
            'model' => 'required|max:255',
            'color' => 'required',
            'license_no' => 'required|min:5|max:25',
            'apartment_id' => 'required',
            'parking_spot' => 'required',
            'parking_spot_meta' => 'required',
            'payment_term' => 'required',
        ];
    }

    protected function findOrCreateCustomer(){

        $user=JWTAuth::user();

        if(is_null($user->braintree_customer_id)){
            $result = $gateway->customer()->create([
                'firstName' => $user->firstname,
                'lastName' => $user->lastname,
                'email' => $user->email,
                'customFields' => [
                    'apartment' => $user->apartment->name,
                ]
            ]);            

            return $result;
        }
        else{
            $customer = $gateway->customer()->find($user->braintree_customer_id);
            return $customer;
        }
    }

    protected function generateClientToken(){
        $user=JWTAuth::user();

        return $this->gateway->clientToken([
            'customerId'=>$user->braintree_customer_id
        ])->generate();
    }

    protected function updateCustomerPaymentMethod($paymentNonce){
        $user=JWTAuth::user();

        $result = $gateway->customer()->update(
            $user->braintree_customer_id,[
                'paymentMethodNonce' => $nonce
            ]);

        return $result;
    }

    protected function createTransaction($paymentNonce,$permit,$user){

        $result = $gateway->transaction()->sale([
            'amount' => $permit->amount,
            'paymentMethodNonce' => $paymentNonce,
            'customerId' => $user->braintree_customer_id,
            'options' => [
                'storeInVaultOnSuccess' => true,
                'submitForSettlement' => True
            ]
        ]);

        if ($result->success) {
            return response()->json([
                'return'=>'success'
            ]);
        } else {
            return response()->json([
                'return'=>'failed',
                'message'=>"Unable to process your payment"
            ]);
        }
    }
}
