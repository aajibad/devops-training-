<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

use App\Apartment;
use App\VisitorVehicle;
use App\VisitorParkingPermit;
use App\Notifications\SendVisitorParkingTicketLinkNotification;

use Validator;
use Auth;
use Nexmo;
use QrCode;
use JWTAuth;

class VisitorTicketController extends Controller
{

    public function showForm($token){
        
        return view('user.visitor_ticket_details',[
            'token'=>$token
        ]);
    
    }
    
    public function generate(Request $request){

        $validator=Validator::make($request->all(),$this->rules());
        
        if($validator->fails()){
            return response()->json($validator->errors());
        }

        
        $user=JWTAuth::user();
        $apartment=Apartment::find($user->apartment->id);
        
        $visitor_permit=new VisitorParkingPermit();
        $visitor_permit->parking_spot=$request->parking_spot;
        $visitor_permit->parking_spot_meta=$request->parking_spot_meta;
        $visitor_permit->status='pending';

        $visitor_permit->start_from=$request->start_from;
        $visitor_permit->expired_on=$request->expired_on;

        $visitor_permit->apartment()->associate($apartment->id);
        $visitor_permit->user()->associate($user->id);
        $visitor_permit->phone=$request->phone;

        $visitor_permit->ticket_token=bin2hex(openssl_random_pseudo_bytes(4));
        
        $message=[
            'to'   => $request->phone,
            'from' => '16105552344',
            'text' => strtoupper($user->name).", Generated parking permit. Please visit link to register vehicle.\nLINK: ".url('/vehicle/details',['token'=>$visitor_permit->ticket_token])
        ];

        try {
            Nexmo::message()->send($message);
        } catch (\Exception $e) {
            return response()->json([
                        'message'=>'Link could not be sent! Please check your number and try again.',
                        'error-code'=>$e->getCode()
                    ]);
        }

        if($apartment->status=="active" && $user->status=="active"){
            if(!$visitor_permit->save()){
                return response()->json([
                    'message'=>'unsuccessfull'
                ]);
            }

            return response()->json([
                'message'=>'success',
                'data'=>$visitor_permit,
                'url'=>''
            ]);
        }

        return response()->json([
            'message'=>'Failed, there may be an error occured for the following reasons.
                        apartment currently unavailable or you are still not
                        activated. please contact your housing manager.'
        ]);

    }

    public function registerVehicle(Request $request){
        
        // $validator=Validator::make($request->all(),[
        //                 'ticket_token'=>'required',
        //                 'vehicle_model'=>'required',
        //                 'vehicle_color'=>'required',
        //                 'vehicle_license_no'=>'required'
        //             ]);

        // if($validator->fails()){
        //     return redirect()->back();
        // }
        
        $request->validate([
            'ticket_token'=>'required',
            'vehicle_model'=>'required',
            'vehicle_color'=>'required',
            'vehicle_license_no'=>'required'
        ]);

        $findTicket=VisitorParkingPermit::where('ticket_token',$request->ticket_token)->get()->first();
        
        if(!is_null($findTicket)){
            $vehicle=new VisitorVehicle();
            $vehicle->model=$request->vehicle_model;
            $vehicle->color=$request->vehicle_color;
            $vehicle->license_no=$request->vehicle_license_no;
            $vehicle->apartment()->associate($findTicket->apartment->id);
            
            foreach($request->vehicle_image as $key=>$value){
                $value->storeAs('/images/visitor_vehicles',$request->ticket_token.'-'.$key,'app_public');
            }  

            if($vehicle->save())
                $findTicket->vehicle()->associate($vehicle->id);
                $findTicket->save();
                return redirect()->back()->with([
                            'return'=>'success',
                            'message'=>'your vehicle registered successfully.',
                            'ticket_token'=>$request->ticket_token
                        ]);            
        }
        else{
            return redirect()->back()->with([
                'return'=>'error',
                'message'=>'your ticket token missmatched.',
            ]);
        }        


    }

    public function showQrCode($ticket_token){
        $findTicket=VisitorParkingPermit::where('ticket_token',$ticket_token)->get()->first();

        if(!is_null($findTicket)){
            return QrCode::margin(2)
            ->size(250)
            ->generate('TICKET_TOKEN : '.strtoupper($ticket_token).
                       ',APARTMENT : '.strtoupper($findTicket->apartment->name).
                       ',SOURCE : www.parkingsphere.com');
        }
        else{
            return response('Sorry!, ticket token missmatched.');
        }
    }

    function listVisitorPermits(){
        
        $permit=VisitorParkingPermit::where("generated_by",JWTAuth::user()->id)->with(["apartment","vehicle"])->get();
        
        if(!$permit){
            return response()->json([
                "message"=>"no permits found."
            ]);
        }
        // return new JsonResponse($permit);

        return response()->json($permit);
    }

    protected function rules(){
        return [
            'phone'=>'required|min:10|max:12',
            'parking_spot' => 'required',
            'parking_spot_meta' => 'required',
            'start_from' => 'required',
            'expired_on' => 'required',
        ];
    }
}
