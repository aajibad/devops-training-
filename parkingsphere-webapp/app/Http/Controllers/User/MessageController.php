<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Message;
use App\MessageConversation;
use App\Apartment;
use App\User;
use App\Events\NewComplaintEvent;
use App\Events\NewMessageEvent;
use App\MessageImage;

use Validator;
use JWTAuth;
use DB;

class MessageController extends Controller
{
    public function __construct(){

        $this->middleware('auth:user-api');

    }

    public function create(Request $request){
        $formValidation=Validator::make($request->all(),[
            'form'=>'required'
        ]);
        
        if($formValidation->fails()){
            return response()->json($formValidation->errors());
        }
        
        $response;

        if($request->form=='new_conversation'){
            
            $user=JWTAuth::user();

            $rules = [
                'title' => 'required',
                'body' => 'required',
            ];

            $images=json_decode($request->images);

            $validator=Validator::make($request->all(),$rules);
        
            if($validator->fails()){
                return response()->json($validator->errors());
            }

            $findApartment=Apartment::find($user->apartment->id);
            $findUser=User::find($user->id);

            // Create conversation
            $conversation=new MessageConversation();
            $conversation->title=$request->title;
            $conversation->apartment()->associate($findApartment);
            $conversation->user()->associate($findUser);
            $conversation->save();

            // Create message
            $message=new Message();
            $message->body=$request->body;
            $message->user_type='user';
            $message->user()->associate($findUser);
            $message->conversation()->associate($conversation);
            
            if($message->save()){
                
                event(new NewComplaintEvent($message));

                $response=[
                    'message'=>'success',
                    'id'=>$conversation->id
                ];
            }      
            else{
                $response=['message'=>'failed'];
            }      
        }
        elseif($request->form=='new_message'){

            $rules =[
                'conversation_id' => 'required',
                'user_id' => 'required',
                'body' => 'required',
            ];

            $validator=Validator::make($request->all(),$rules);
        
            if($validator->fails()){
                return response()->json($validator->errors());
            }

            $findConversation=MessageConversation::find($request->conversation_id);
            $findUser=User::find($request->user_id);

            $message=new Message();
            $message->body=$request->body;
            $message->user_type='user';
            $message->user()->associate($findUser);
            $message->conversation()->associate($findConversation);
            $message->save();


            if($message->save()){
                event(new NewMessageEvent($message));
                $response=['message'=>'success'];
            }      
            else{
                $response=['message'=>'failed'];
            }      
        }

        return response()->json($response);
    }

    public function conversations(){
        
        $conversations=MessageConversation::where('user_id',JWTAuth::user()->id)
        ->with(['messages'=>function($query){
            $query->orderBy('created_at','desc');
        }])
        ->withCount(['messages'=>function($query){
            $query->where([
                'user_type'=>'admin',
                'is_read'=>0,
            ]);
        }])->orderBy('created_at','desc')->get();

        return response()->json($conversations);

    }

    public function showConversationByUserId(Request $request){

        $conversation=MessageConversation::where('id',$request->id)->withCount('images')->with(['messages','user'])->first();

        return response()->json($conversation);
    }

    public function showMessageByConversationId($id){

        $messages=Message::where('conversation_id',$id)->get();

        return response()->json($messages);
    }

    public function deleteConversation(Request $request){
        $conversation=MessageConversation::find($request->id);
        
        if($conversation->delete()){
            return response()->json([
                'message'=>'success'
            ]);
        }

        return response()->json(['message'=>'failed']);
    }

    public function uploadComplaintImages(Request $request){
        
        $rules =[
            'id' => 'required',
            'image' => 'required',
        ];

        $validator=Validator::make($request->all(),$rules);
    
        if($validator->fails()){
            return response()->json($validator->errors());
        }

        $image=new MessageImage();
        $image->path=$request->file('image')->store('/images/complaints','app_public');
        $image->conversation()->associate($request->id);
        
        if($image->save()){
            return response()->json([
                'message'=>'success'
            ]);
        }

        return response()->json([
            'message'=>'failed'
        ]);
    
    }

    public function showComplaintImages(Request $request){
        
        $rules =[
            'id' => 'required',
        ];

        $validator=Validator::make($request->all(),$rules);
    
        if($validator->fails()){
            return response()->json($validator->errors());
        }

        $images=MessageImage::where('conversation_id',$request->id)->get();

        if(count($images)>0){
            return response()->json(['message'=>'success','images'=>$images]);
        }

        return response()->json(['message'=>'No images found.']);    

    }

    public function markAsRead(Request $request){
        $messages=Message::where([
            'conversation_id'=>$request->id,
            'user_type'=>$request->user
        ])->update([
            'is_read'=>true
        ]);
        
        return response()->json([
            'message'=>'success'
        ]);
    }

    // Get all user notifications
    public function announcements(){
        
        $user=JWTAuth::user();

        $countUnreadNotification=DB::table('user_notifications')
        ->whereRaw('notification_type="all" AND apartment_id=?',$user->apartment->id)
        ->orWhereRaw('notification_type="individual" AND user_notifications.user_id=?',$user->id)
        ->whereRaw('is_read=?',0)
        ->get();

        $notifications=DB::table('user_notifications')
        ->whereRaw('notification_type="all" AND apartment_id=?',$user->apartment->id)
        ->orWhereRaw('notification_type="individual" AND user_notifications.user_id=?',$user->id)
        ->orderBy('created_at','desc')
        ->get();

        return response()->json([
            "unread"=>count($countUnreadNotification),
            "notifications"=>$notifications
        ]);
    }
}
