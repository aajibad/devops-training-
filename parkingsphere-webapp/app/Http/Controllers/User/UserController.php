<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

use App\User;
use Validator;
use JWTAuth;

class UserController extends Controller
{
    public function updateStatus(Request $request){
        $validate=Validator::make($request->all(), [
            'id' => 'required',
            'status'=>'required|boolean'
        ]);

        if($validate->fails()){
            return response()->json($validate->errors());
        }

        $user=User::find($request->id);
        $user->status=$request->status?"active":"blocked";
        
        if($user->save()){
            return response()->json([
                'message'=>'success'
            ]);
        }

        return response()->json([
            'message'=>'unsuccessfull'
        ]);
    }

    public function update(Request $request){
        
        $validate=Validator::make($request->all(),[
            'id' => 'required',
            'name' => 'required',
            'room_no' => 'required',
            'phone' => 'required',
        ]);

        if($validate->fails()){
            return response()->json($validate->errors());
        }

        $user=User::find($request->id);

        $message='';

        if($user->get()->isNotEmpty()){
            $filename=$user->id.'_'.date("Y-m-d",mktime(0,0,0));
            $user->avatar=$request->has('avatar')
                            ?$request->avatar->storeAs('images/user_avatar',$filename,'app_public')
                            :null;
            $user->name=$request->name;
            $user->room_no=$request->room_no;
            $user->phone=$request->phone;    

            $user->save()?$message="success":$message="unsuccessfull";
        }
        else{
            $message="user not found.";
        }

        return response()->json([
            'message'=>$message
        ]);
    }

    public function permitsByUserId(Request $request){

        $user=JWTAuth::user()->with(["apartment","parkingPermits","visitorParkingPermits","vehicles"])->first();
    
        
        if(!is_null($user)){
            return new \Illuminate\Http\JsonResponse($user);
        }  
        else{
            return response()->json([
                'message'=>'user not found.'
            ]);
        }
    }
}
