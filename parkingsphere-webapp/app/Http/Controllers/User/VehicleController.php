<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

use App\TowedVehicle;
use App\VehicleImage;
use JWTAuth;

class VehicleController extends Controller
{
 
    public function search(Request $request){

        $apartment=JWTAuth::user()->apartment->id;

        $result=TowedVehicle::where("vehicle_license_no", "LIKE", "%$request->license_no%")
                ->where("apartment_id",$apartment)
                ->with(['apartment','user','towingCompany'])
                ->with(['userVehicle'=>function($query){
                    $query->with('images')->get();
                }])
                ->with(['visitorVehicle'=>function($query){
                    $query->with('images')->get();
                }])
                ->get();
        
        if(count($result)>0){
            return response()->json([
                'message'=>true,
                'result'=>$result
            ]);
        }

        return response()->json([
            'message'=>false
        ]);
    }

    public function showVehicleImages(Request $request){

        $images=VehicleImage::where('vehicle_id',$request->id)->get();
        
        if(count($images)>0){
            return response()->json([
                'return'=>'success',
                'images'=>$images
            ]);
        }
        
        return response()->json([
            'return'=>'failed',
            'message'=>'no images found.'
        ]);
    }
}
