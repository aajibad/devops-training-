<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class VisitorParkingPermit extends Model
{
    use Notifiable;

    protected $table="visitor_parking_permits";

    public function routeNotificationForNexmo($notification)
    {
        return $this->phone;
    }

    public function vehicle(){

        return $this->belongsTo('App\Vehicle','vehicle_id','id');
    
    }

    public function user(){

        return $this->belongsTo('App\User','generated_by','id');
    
    }

    public function apartment(){

        return $this->belongsTo('App\Apartment','apartment_id','id');
    
    }


}
