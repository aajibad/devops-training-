<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\ParkingPermit;

class NewParkingPermitPurchased extends Mailable
{
    use Queueable, SerializesModels;

    public $parking_permit;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(ParkingPermit $parking_permit)
    {   
        $this->parking_permit=$parking_permit;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.parking_permit.created');
    }
}
