<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */

    protected $listen = [
        'App\Events\UserEvent' => [
            'App\Listeners\SendWelcomeEmail',
            'App\Listeners\SendNewUserEmail'
        ],
        'App\Events\AdminEvent' => [
            'App\Listeners\SendAdminWelcomeEmail',
            'App\Listeners\SendNewAdminEmail'
        ],
        'App\Events\ApartmentEvent' => [
            'App\Listeners\CreateNewAdminOption',
            'App\Listeners\SendNewApartmentEmail',
        ],
        'App\Events\ParkingPermitEvent' => [
            'App\Listeners\SendNewParkingPermitEmail',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
