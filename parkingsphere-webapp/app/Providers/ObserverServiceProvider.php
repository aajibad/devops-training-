<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Apartment;
use App\User;
use App\Observers\UserObserver;
use App\Observers\ApartmentObserver;

class ObserverServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        User::observe(UserObserver::class);
        Apartment::observe(ApartmentObserver::class);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
