<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ParkingPermit extends Model
{
    protected $table="parking_permits";

    protected $dispatchesEvents=[
        'created'=>'App\Events\ParkingPermitEvent',
    ];
    
    public function vehicle(){

        return $this->belongsTo('App\Vehicle');
    
    }

    public function user(){

        return $this->belongsTo('App\User');
    
    }

    public function apartment(){

        return $this->belongsTo('App\Apartment','apartment_id','id');
    
    }
}
