<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TowingCompany extends Model
{
    protected $table="towing_companies";
    
    // public function towedVehicles(){

    //     return $this->hasMany('App\TowedVehicle','towing_company_id','id');
    
    // }

    public function apartment(){

        return $this->belongsTo('App\Apartment');
    
    }
}
