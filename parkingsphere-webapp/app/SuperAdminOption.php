<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SuperAdminOption extends Model
{
    protected $table="super_admin_options";

    public static function getOption($name){   
        
        if(!$option=self::where(['name'=>$name])->orderBy('created_at', 'desc')->first()){
            return $option;
        }

        return 0; 
    }

    public static function getOptionValue($name){   
        
        if(!$option=self::where(['name'=>$name])->orderBy('created_at', 'desc')->first()){
            return $option->value;
        }

        return 0; 
    }

    // monthly | 3 months | year
    public static function getRate($name){   
        
        $option=self::where(['name'=>$name.'_subscription_rate'])->orderBy('created_at', 'desc')->first();

        if(!is_null($option)){
            return $option->value;
        }

        return 0; 
    }

    // monthly | 3 months | year
    public static function getDiscount($name){   
        
        $option=self::where(['name'=>$name.'_subscription_discount'])->orderBy('created_at', 'desc')->first();
        
        if(!is_null($option)){
            return $option->value;
        }

        return 0; 
    }
}
