<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\AdminResetPasswordNotification;
use Tymon\JWTAuth\Contracts\JWTSubject;

class Admin extends Authenticatable implements JWTSubject
{
    use Notifiable;

    protected $table='admins';
    protected $guard='admin';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $dispatchesEvents=[
        'created'=>Events\AdminEvent::class
    ];

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new AdminResetPasswordNotification($token));
    }

    public function cards(){
        
        return $this->hasMany('App\AdminCard','id','user_id');
        
    }

    public function apartments(){
        
        return $this->belongsToMany('App\Apartment')->withPivot('permission');
        
    }

    public function histories(){

        return $this->hasMany('App\AdminPaymentHistory','admin_id','id');

    }

    public function messages(){

        return $this->hasMany('App\Message','sender_id','id');

    }

    public function payments(){

        return $this->hasMany('App\AdminPaymentHistory');

    }

    public function subscription(){

        return $this->hasOne('App\Subscription');
    
    }

        /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    
    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
