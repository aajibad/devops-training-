<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserCard extends Model
{
    protected $tables="user_cards";

    public function user(){

      return belongsTo('App\User','user_id','id');

    }

    function numberMask($number) {
      return substr($number, 0, 4) . str_repeat('X', strlen($number) - 8) . substr($number, -4);
    }
}
