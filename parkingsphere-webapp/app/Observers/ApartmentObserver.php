<?php

namespace App\Observers;

use App\Apartment;

class ApartmentObserver
{
    public function created(Apartment $apartment)
    {
        $apartment_id=$apartment->id;
        $apartment_name=$apartment->name;
        $apartment->unique_id=strtoupper(substr($apartment_name,0,2).'AP'.str_pad($apartment_id,4,'0',STR_PAD_LEFT));
    }
}