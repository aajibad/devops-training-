<?php

namespace App\Observers;

use App\User;

class UserObserver
{
    /**
     * Listen to the User created event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function creating(User $user)
    {
        // $user->api_token=bin2hex(openssl_random_pseudo_bytes(30));
        
        $firstname=$user->firstname;
        $lastname=$user->lastname;
        $apartment_unique_id=$user->apartment->unique_id;
        $user->unique_id=strtoupper($apartment_unique_id.substr($lastname,0,1).substr($firstname,0,3));
    }

    /**
     * Listen to the User deleting event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function deleting(User $user)
    {
        //
    }
}