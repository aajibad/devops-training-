<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserNotification extends Model
{
    protected $table="user_notifications";

    public function apartment(){

        return $this->belongsTo('App\Apartment');
    
    }
}
