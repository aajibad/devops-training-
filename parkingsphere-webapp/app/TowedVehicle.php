<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TowedVehicle extends Model
{
    protected $table="towed_vehicles";

    public function apartment(){

        return $this->belongsTo('App\Apartment');
    
    }

    public function user(){

        return $this->belongsTo('App\User');
    
    }

    public function userVehicle(){

        return $this->belongsTo('App\Vehicle','vehicle_id','id');
    
    }

    public function visitorVehicle(){

        return $this->belongsTo('App\VisitorVehicle','vehicle_id','id');
    
    }

    public function towingCompany(){

        return $this->belongsTo('App\TowingCompany');
    
    }

}
