<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Session;

class AdminParkingPermitOption extends Model
{
    public function apartment(){

        return $this->belongsTo('App\Apartment');
    
    }

    public function admin(){

        return $this->belongsTo('App\Admin');
    
    }

    public static function getAvailablePermit($permit_type){
        $query=Self::where([
            'permit_type'=>$permit_type,
            'apartment_id'=>Session::get('admin_apartment')->id
            ])->orderBy('id','DESC')->get()->first();

        return $query->available_permit;
    }

    public static function getTotalAvailablePermit(){
        $building=Self::getPermit('building');
        $floor=Self::getPermit('floor');

        return $building+$floor;
    }
}
