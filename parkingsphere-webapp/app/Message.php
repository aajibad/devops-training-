<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $table="messages";

    protected $fillable=[
        'is_read'
    ];

    public function images(){

        return $this->hasMany('App\MessageImage','message_id','id');

    }

    public function user(){

        return $this->belongsTo('App\User');

    }

    public function admin(){

        return $this->belongsTo('App\Admin','user_id');

    }

    public function conversation(){

        return $this->belongsTo('App\MessageConversation');

    }
}
