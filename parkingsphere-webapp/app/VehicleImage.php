<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VehicleImage extends Model
{
    protected $table="vehicle_images";

    public function vehicle(){

        return $this->belongsTo('App\Vehicle','id','vehicle_id');
    
    }
}
