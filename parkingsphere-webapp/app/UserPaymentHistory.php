<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPaymentHistory extends Model
{
    protected $table="user_payments_history";

    public function card(){

        return $this->belongsTo('App\Card','id','card_id');
    
    }

    public function user(){

        return $this->belongsTo('App\User','id','user_id');
    
    }
}
