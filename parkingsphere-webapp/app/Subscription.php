<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\SuperAdminOption;

class Subscription extends Model
{
    protected $tables="subscriptions";

    public function apartment(){

        return $this->belongsTo('App\Apartment','apartment_id','id');
    
    }

    public function history(){

        return $this->hasMany('App\AdminSubscriptionHistory','subscription_id','id');
    
    }

    public function payer(){

        return $this->belongsTo('App\Admin','admin_id','id');
    
    }

    public function remainingDays($hint=''){
        $now = strtotime('now'); // or your date as well
        $your_date = strtotime($this->expiry_date);
        
        $diff=$your_date-$now;
        $remains=round($diff/86400);
        $datediff='';
        $suffix='';

        if($this->expiry_date>date('Y-m-d H:m:s')){
            if($remains>60){
                $datediff ='';
                $suffix='More than 2 Months to expire';
            }
            elseif(0<$remains && $remains<60){
                $datediff =$remains;
                $suffix=" Days Reamining";
            }
            else{
                $datediff='0';
                $suffix=" Days Reamining";
            }
        }
        else{
            $datediff='0';
            $suffix=" Days Reamining";
        }

        if($hint=='days'){
            return round($diff/86400);
        }

        if($hint=='timestamp'){
            return $diff;
        }
        
        return $datediff.$suffix;
    }

    public function getAmount(){
        
        $rate=$this->getRate();
        $discount=$this->getDiscount();

        return $rate-$discount;
    }

    public function getDiscount(){
        
        $rate=$this->getRate();
        $discount_percent=$this->getDiscountRate();
        
        return ($rate/100)*$discount_percent;
        
    }

    public function getDiscountRate(){
        return SuperAdminOption::getDiscount($this->subscription_type);
    }

    public function getRate(){
        return SuperAdminOption::getRate($this->subscription_type);
    }

    public function getStatus(){
        $status='';
        if(is_null($this->expiry_date)){
            $status="required";
        }
        elseif($this->expiry_date>=date('Y-m-d H:m:s')){
            $status="active";
        }
        else{
            $status="expired";
        }
        return $status;
    }

    public function getSubscriptionPeriod(){
        $subscription_period=[
            'frequency'=>'',
            'interval'=>''
          ];
    
        if($this->subscription_type=='1_month'){

        $subscription_period['frequency']="Month";
        $subscription_period['interval']="1";
        
        }elseif($this->subscription_type=='3_months'){
        
        $subscription_period['frequency']="Month";
        $subscription_period['interval']="3";
        
        }else{
        
        $subscription_period['frequency']="Year";
        $subscription_period['interval']="1";
        
        }

        return $subscription_period;
    }

    // function calcDate(){
    //     $remaining_time=$this->remainingDays('timestamp');
    //     $expiring_time=strtotime("+1 days");
    //     $calc_total_time=$remaining_time+$expiring_time;

    //     return date("Y-m-d H:m:s",$calc_total_time)." Remaining Days : ".$this->remainingDays('days')." Remaining Time : ".date('d',$remaining_time)." Expiring Time : ".date('d',$expiring_time);
    // }
}
