<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MessageImage extends Model
{
    public function conversation(){
    
        return $this->belongsTo('App\MessageConversation','conversation_id','id');
    
    }
}
