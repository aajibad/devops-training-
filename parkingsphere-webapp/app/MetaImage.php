<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MetaImage extends Model
{
    protected $table="meta_images";

    public function vehicle(){

        return $this->belongsTo('App\Vehicle','id','reference_id');

    }
}
