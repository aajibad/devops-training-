<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SuperAdminCard extends Model
{
    protected $tables="super_admin_cards";

    public function superadmin(){

      return $this->belongsTo('App\SuperAdmin','user_id','id');

    }

    function numberMask($number) {
      return substr($number, 0, 4) . str_repeat('X', strlen($number) - 8) . substr($number, -4);
    }
}
