<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Apartment extends Model
{
    use Notifiable;

    protected $table="apartments";

    protected $dispatchesEvents = [
        'created'=>Events\ApartmentEvent::class,
    ];

    public function parkingPermits(){

        return $this->hasMany('App\ParkingPermit','apartment_id','id');

    }

    public function visitorParkingPermits(){

        return $this->hasMany('App\VisitorParkingPermit','apartment_id','id');

    }

    public function users(){

        return $this->hasMany('App\User');

    }

    public function admins(){

        return $this->belongsToMany('App\Admin')->withPivot('permission');

    }

    public function adminOption(){

      return $this->hasOne('App\AdminOption','apartment_id','id');

    }

    public function messageConversations(){

      return $this->hasMany('App\MessageConversation','apartment_id','id');

    }

    public function towedVehicles(){

      return $this->hasMany('App\TowedVehicle','apartment_id','id');

    }

    public function vehicles(){

      return $this->hasMany('App\Vehicle','apartment_id','id');

    }

    public function subscription(){

      return $this->hasOne('App\Subscription','apartment_id','id');

    }
}
