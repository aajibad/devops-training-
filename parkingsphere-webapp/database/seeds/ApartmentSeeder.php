<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Admin;

class ApartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $apartments=[
            "Anchorage Apartments",
            "The Annapurna Inn",
            "Balboa Apartments",
            "Bonanza Apartments",
            "Edgewater Apartments",
            "El Dorado East",
            "El Dorado West",
            "Fontainebleu Apartments",
            "House of Lords",
            "Kahlua Apartments",
            "Los Cedros",
            "Makaha Apartments",
            "Monte Vista Apartments",
            "Seaview Apartments",
            "Surf Rider Apartments",
            "Tropicana Gardens",
        ];

        $apartment_address=[
            "811 Camino Pescadero"
            ,"785 Camino Del Sur"
            ,"6711 El Colegio Road"
            ,"6545 Trigo Road"
            ,"6707 Abrego Road"
            ,"6657 El Colegio Road"
            ,"6667 El Colegio Road"
            ,"6625 El Colegio Road"
            ,"6689 El Colegio Road"
            ,"781 Embarcadero Del Norte"
            ,"6626 Picasso Road"
            ,"6631 Picasso Road"
            ,"6674 Picasso Road"
            ,"6565 Sabado Tarde Road"
            ,"796 Embarcadero Del Norte"
            ,"6585 El Colegio Road"
        ];

        $status=['active','blocked','pending'];

        $admins=Admin::all();

        foreach($apartments as $key=>$apartment){
            DB::table('apartments')->insert([
                'name' => $apartment,
                'phone' => rand(19567893,20000000),
                'address' => $apartment_address[$key],
                'logo'=>'images/apartment_banners/sample_apartment_banner.jpeg',
                'status'=>$status[rand(0,2)],
                'unique_id'=>strtoupper(substr($apartment,0,2).'AP'.str_pad(rand(1,15),4,'0',STR_PAD_LEFT))
            ]);
        }
        
    }
}
