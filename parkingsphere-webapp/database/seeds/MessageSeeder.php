<?php

use Illuminate\Database\Seeder;
use App\MessageConversation;

class MessageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user_type=[
            'admin','user'
        ];

        $conversation=MessageConversation::all();

        for($i=1; $i<=15; $i++){
            DB::table('messages')->insert([
                'body'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                'is_read'=>rand(0,1),
                'user_id'=>1,
                'user_type'=>$user_type[rand(0,1)],
                'conversation_id'=>rand(1,5),
                'created_at'=>date('Y-m-d')
            ]);
        }
    }
}
