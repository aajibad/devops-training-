<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Apartment;
use App\Admin;

class AdminApartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        $admins=Admin::all();
        $apartments=Apartment::all();

        $permissions=[
            'read_write',
            'read_only'
        ];

        foreach($admins as $admin){
            DB::table('admin_apartment')->insert([
                'admin_id' => rand(1,count($admins)),
                'apartment_id' => rand(1,sizeof($apartments)),
                'permission' =>$permissions[rand(0,1)]
            ]); 
        }
    }
}
