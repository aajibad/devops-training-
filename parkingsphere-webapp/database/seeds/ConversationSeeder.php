<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use App\User;

class ConversationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $titles=[
            'Vehicle damaged','Complaint about towing company',
            'Visitor parking permit still not acitvated',
            'Parking permit payments not working',
            'Benz CLK500 window broken'
        ];

        $users=User::all();

        foreach($titles as $title){
            DB::table('message_conversations')->insert([
                'title'=>$title,
                'user_id'=>rand(1,30),
                'apartment_id'=>1,
                'created_at'=>date('Y-m-d')
            ]);
        }
    }
}
