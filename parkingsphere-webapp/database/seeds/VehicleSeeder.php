<?php

use Illuminate\Database\Seeder;
use App\User;

class VehicleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $colors=[
            'black','white','grey','yellow','red','green','cyan'
        ];

        $models=[
            'Benz','Bentley','Toyota','Honda','BMW','Astorn Martin','Lamborgini','Ferrari'
        ];  
        $users=User::all();

        for($i=1; $i<30; $i++){
            DB::table('vehicles')->insert([
                'model' => $models[rand(0,7)],
                'color' => $colors[rand(0,6)],
                'license_no' => uniqid(),
                'apartment_id' =>rand(1,15),
                'user_id'=>rand(1,10)
            ]);
        }
    }
}
