<?php

use Illuminate\Database\Seeder;

class AdminSubscriptionHistorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $status=[
            'success','failed','pending'
        ];

        $subscription_types=[
            'monthly','1_year','3_months'
        ];

        for($i=1; $i<=15; $i++){
            DB::table('admin_subscription_history')->insert([
                'amount'=>rand(50,600),
                'payment_status'=>$status[rand(0,2)],
                'card_number'=>rand(4502,8543).rand(4502,8543).rand(4502,8543).rand(4502,8543),
                'subscription_type'=>$subscription_types[rand(0,2)],
                'apartment_id'=>rand(1,3),
                'admin_id'=>rand(1,4),
                'subscription_id'=>rand(1,15),
                'created_at'=>date('Y-m-d H:m:s')
            ]);
        }
    }
}
