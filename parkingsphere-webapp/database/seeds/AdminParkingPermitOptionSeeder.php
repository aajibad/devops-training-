<?php

use Illuminate\Database\Seeder;

class AdminParkingPermitOptionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admin_parking_permit_options')->insert([
            'total_permit' => 0,
            'available_permit' =>0,
            'permit_type' =>'floor',
            'apartment_id' =>1,
            'admin_id' =>1,
        ]);

        DB::table('admin_parking_permit_options')->insert([
            'total_permit' => 0,
            'available_permit' =>0,
            'permit_type' =>'building',
            'apartment_id' =>1,
            'admin_id' =>1,
        ]);
    }
}
