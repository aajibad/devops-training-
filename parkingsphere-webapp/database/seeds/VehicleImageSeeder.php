<?php

use Illuminate\Database\Seeder;
use App\Vehicle;

class VehicleImageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $vehicles=Vehicle::all();

        for($i=1; $i<=15; $i++){
            DB::table('vehicle_images')->insert([
                'path' => 'images/vehicles/'.rand(1,15).'.jpg',
                'vehicle_id' => rand(1,25),
            ]);
        }
    }
}
