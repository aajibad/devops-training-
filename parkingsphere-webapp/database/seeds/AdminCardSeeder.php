<?php

use Illuminate\Database\Seeder;
use App\Admin;

class AdminCardSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admins=Admin::all();
        $card_type=['visa','master_card'];
        $d=mktime(11, 14, 54, rand(1,28), rand(1,12), '20'.rand(10,25));

        // $card_holders=[
        //     Noah,Liam,Mason,Jacob,William,Ethan,James,Alexander,Michael,
        //     Benjamin,Elijah,Daniel,Aiden,Logan,Matthew,Lucas,Jackson,
        //     David,Oliver,Jayden,Joseph,Gabriel,Samuel,Carter,Anthony,John,Dylan,Luke,
        //     Henry,Andrew,Isaac,Christopher,Joshua,Wyatt,Sebastian,Owen,Caleb,Nathan,
        //     Ryan,Jack,Hunter,Levi,Christian,Jaxon,Julian,Landon,Grayson,Jonathan,
        //     Isaiah,Charles
        // ];

        $card_holders=[
            "Noah","Liam","Mason","Jacob","William","Ethan","James",
            "Alexander","Michael","Benjamin","Elijah","Daniel",
            "Aiden","Logan","Matthew"
        ];

        foreach($card_holders as $holder){
            DB::table('admin_cards')->insert([
                'card_holder' => $holder,
                'card_type' => $card_type[rand(0,1)],
                'card_number' => rand(4502,8543).rand(4502,8543).rand(4502,8543).rand(4502,8543),
                'expiry'=>date("Y-m-d", $d),
                'cvv'=>rand(000,999),
                'auto_payment'=>rand(0,1),
                'user_id'=>rand(1,1)
            ]);
        }
    }
}
