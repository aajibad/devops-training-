<?php

use Illuminate\Database\Seeder;

class TowedVehicleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $colors=[
            'black','white','grey','yellow','red','green','cyan'
        ];

        $models=[
            'Benz','Bentley','Toyota','Honda','BMW','Astorn Martin','Lamborgini','Ferrari'
        ];  

        $user_type=[
            'user','visitor'
        ];

        for($i=1; $i<30; $i++){
            DB::table('towed_vehicles')->insert([
                'vehicle_model' => $models[rand(0,7)],
                'vehicle_color' => $colors[rand(0,6)],
                'vehicle_license_no' => uniqid(),
                'vehicle_id' => rand(1,25),
                'user_type' =>$user_type[rand(0,1)],
                'user_id'=>1,
                'apartment_id'=>rand(1,2),
                'towing_company_id'=>rand(1,5),
                'created_at'=>date('Y-m-d',mktime(0,0,0,rand(1,30),rand(1,4),'20'.rand(15,18))),
                'updated_at'=>date('Y-m-d',mktime(0,0,0,rand(1,30),rand(1,4),'20'.rand(15,18)))
            ]);
        }
    }
}
