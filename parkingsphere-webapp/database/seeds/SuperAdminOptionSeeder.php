<?php

use Illuminate\Database\Seeder;

class SuperAdminOptionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $options=[
            ['monthly_subscription_rate',50],
            ['3_months_subscription_rate',150],
            ['1_year_subscription_rate',600]
        ];

        foreach($options as $option){
            DB::table('super_admin_options')->insert([
                'name'=>$option[0],
                'value'=>$option[1],
                'created_at'=>date('Y-m-d H:m:s')
            ]);
        }
    }
}
