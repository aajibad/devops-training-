<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        // DB::table('users')->insert([
        //     'name' => 'smith',
        //     'email' =>'will@smith.com',
        //     'password' => bcrypt('secret'),
        //     'firstname' =>'Will',
        //     'lastname' =>'Smith',
        //     'phone' =>'9452144202',
        //     'room_no' =>'RM2541',
        //     'unique_id' =>'SWILLMITH',
        //     'apartment_id'=>'1'
        // ]);

        // for($i=1; $i<=20; $i++){
        //     DB::table('admins')->insert([
        //         'name' => 'admin'.$i,
        //         'email' =>'admin'.$i.'@admin.com',
        //         'password' => bcrypt('secret'),
        //     ]);
        // }

        DB::table('admins')->insert([
            'name' => 'admin',
            'email' =>'admin@admin.com',
            'password' => bcrypt('secret'),
        ]);

        DB::table('admin_apartment')->insert([
            'admin_id' => '1',
            'apartment_id' =>'1',
            'permission' =>'read_write',
        ]);

        // DB::table('subscriptions')->insert([
        //     'apartment_id' =>'1',
        // ]);

        DB::table('superadmins')->insert([
            'name' => 'superadmin',
            'email' =>'superadmin@admin.com',
            'password' => bcrypt('secret'),
        ]);

        $this->call([
        
            SuperAdminOptionSeeder::class,
            // AdminOptionSeeder::class,                       
            ApartmentSeeder::class,
            UserSeeder::class,
            AdminCardSeeder::class,
            ParkingPermitSeeder::class,
            VisitorParkingPermitSeeder::class,
            ConversationSeeder::class,
            MessageSeeder::class,
            MessageImageSeeder::class,
            ParkingPermitSeeder::class,
            VehicleSeeder::class,
            VehicleImageSeeder::class,
            TowedVehicleSeeder::class,
            TowingCompanySeeder::class,            
            AdminParkingPermitOptionSeeder::class,
            
        ]);
    }
}
