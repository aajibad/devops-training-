<?php

use Illuminate\Database\Seeder;

class TowingCompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $company=[
            "A9 Towing Company",
            "Action Towing",
            "Apple Towing Company",
            "Best Towing",
            "Brother’s Towing",
            "Car Pro Auto Repair",
            "Chip’s Transport & Towing",
            "City Motors Towing",
            "Coastal Pride Towing",
            "Cod Towing",
            "Collision King",
            "Covert Recovery Solutions",
            "Dependable Tow Inc."
        ];
        
        for($i=1; $i<30; $i++){
            DB::table('towing_companies')->insert([
                'name' => $company[rand(0,12)],
                'address' => "No:97,New street",
                'phone' => uniqid(),
                'apartment_id' => rand(1,25),
                'created_at'=>date('Y-m-d',mktime(0,0,0,rand(1,30),rand(1,4),'20'.rand(15,18))),
                'updated_at'=>date('Y-m-d',mktime(0,0,0,rand(1,30),rand(1,4),'20'.rand(15,18)))
            ]);
        }
    }
}
