<?php

use Illuminate\Database\Seeder;
use App\Apartment;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users=[
            "Noah","Liam","Mason","Jacob","William","Ethan","James",
            "Alexander","Michael","Benjamin","Elijah","Daniel",
            "Aiden","Logan","Matthew","Lucas","Jackson","David","Oliver",
            "Jayden","Joseph","Gabriel","Samuel","Carter",
            "Anthony","John","Dylan","Luke","Henry","Andrew","Isaac","Christopher",
            "Joshua","Wyatt","Sebastian","Owen","Caleb","Nathan",
            "Ryan","Jack","Hunter","Levi","Christian","Jaxon","Julian",
            "Landon","Grayson","Jonathan",
            "Isaiah","Charles"
        ];

        $status=['active','blocked'];
        $apartments=Apartment::all();

        foreach($users as $user){
            DB::table('users')->insert([
                'name' => strtolower($user),
                'email' => strtolower($user).'@email.com',
                'firstname'=>'john',
                'lastname'=>$user,
                'password' => bcrypt('secret'),
                'status'=>$status[rand(0,1)],
                'apartment_id'=>'1',
                // 'api_token'=>bin2hex(openssl_random_pseudo_bytes(30))
            ]);
        }
    }
}
