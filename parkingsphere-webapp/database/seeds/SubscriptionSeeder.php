<?php

use Illuminate\Database\Seeder;
use App\Apartment;

class SubscriptionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $apartments=Apartment::all();

        $subscription_types=[
            'monthly','1_year','3_months'
        ];

        $status=[
            'success','expired','required'
        ];

        $d=mktime(11, 14, 54, rand(1,28), rand(1,12), '20'.rand(10,25));
        
        foreach($apartments as $apartment){
            DB::table('subscriptions')->insert([
                'subscription_type'=>$subscription_types[rand(0,2)],
                'expiry_date'=>date('Y-m-d',mktime(0,0,0,rand(1,30),rand(1,12),'20'.rand(10,18))),
                'status'=>$status[rand(0,2)],
                'apartment_id'=>$apartment->id,
                'admin_id'=>rand(1,5),
                'created_at'=>date("Y-m-d", $d),
                'updated_at'=>date("Y-m-d", $d)
            ]);
        }
    }
}
