<?php

use Illuminate\Database\Seeder;
use App\MessageConversation;

class MessageImageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $conversations=MessageConversation::all();

        for($i=1; $i<=15; $i++){
            DB::table('message_images')->insert([
                'path'=>'images/vehicles/'.rand(1,15).'.jpg',
                'conversation_id'=>rand(1,5)
            ]);
        }
    }
}
