<?php

use Illuminate\Database\Seeder;
use App\Apartment;
use App\User;
use App\Vehicle;

class ParkingPermitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $apartments=Apartment::all();
        $users=User::all();
        $vehicles=Vehicle::all();


        $d=mktime(11, 14, 54, rand(1,28), rand(1,12), '20'.rand(10,25));

        $parking_spot=[
            'building',
            'floor'
        ];

        $payment_term=[
            'monthly','yearly'
        ];

        $status=[
            'expired','active','pending'
        ];

        for($i=1; $i<30; $i++){
            DB::table('parking_permits')->insert([
                'parking_spot' => $parking_spot[rand(0,1)],
                'parking_spot_meta' => 'Floor/Building '.rand(1,300),
                'payment_term' => $payment_term[rand(0,1)],
                'payment_amount' => rand(200,600),
                'expired_on' => date("Y-m-d", $d),
                'status' => $status[rand(0,2)],
                'vehicle_id' => rand(1,15),
                'user_id' => rand(1,30),
                'apartment_id' => '1',
                'created_at'=>date("Y-m-d", $d),
                'updated_at'=>date("Y-m-d", $d)
            ]);
        }
    }
}
