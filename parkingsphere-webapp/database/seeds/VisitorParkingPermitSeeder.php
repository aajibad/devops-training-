<?php

use Illuminate\Database\Seeder;
use App\Apartment;
use App\User;
use App\Vehicle;

class VisitorParkingPermitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $apartments=Apartment::all();
        $users=User::all();
        $vehicles=Vehicle::all();


        $d=mktime(11, 14, 54, rand(1,28), rand(1,12), '20'.rand(18,25));

        $parking_spot=[
            'building',
            'floor'
        ];

        $payment_term=[
            'monthly','yearly'
        ];

        $status=[
            'expired','active','pending'
        ];

        for($i=1; $i<30; $i++){
            DB::table('visitor_parking_permits')->insert([
                'parking_spot' => $parking_spot[rand(0,1)],
                'parking_spot_meta' => 'Floor/Budate("Y-m-d", $dilding '.rand(1,300),
                'start_from' => date("Y-m-d"),
                'expired_on' => date("Y-m-d", $d),
                'status' => $status[rand(0,2)],
                'vehicle_id' => rand(1,sizeof($vehicles)),
                'generated_by' => rand(1,sizeof($users)),
                'apartment_id' => rand(1,sizeof($apartments)),
                'created_at'=>date("Y-m-d", $d),
                'updated_at'=>date("Y-m-d", $d)
            ]);
        }
    }
}
