<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminParkingPermitOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_parking_permit_options', function (Blueprint $table) {
            $table->increments('id');
            $table->string('total_permit')->default(0);
            $table->string('available_permit')->default(0);
            $table->string('permit_type');
            $table->string('apartment_id');
            $table->string('admin_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_parking_permit_options');
    }
}
