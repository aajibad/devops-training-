<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserPaymentsHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_payments_history', function (Blueprint $table) {
            $table->increments('id');
            $table->string('pay_for');
            $table->string('paid_amount');
            $table->string('currency');
            $table->string('payment_status');
            $table->string('reference');
            $table->string('card_id');
            $table->string('apartment_id');
            $table->string('user_id');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_payments_history');
    }
}
