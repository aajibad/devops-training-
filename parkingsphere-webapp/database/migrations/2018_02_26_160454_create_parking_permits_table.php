<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParkingPermitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parking_permits', function (Blueprint $table) {
            $table->increments('id');
            $table->string('parking_spot');
            $table->string('parking_spot_meta');
            $table->string('payment_term');
            $table->string('payment_amount');
            $table->date('expired_on');
            $table->string('status');
            $table->string('vehicle_id');
            $table->string('user_id');
            $table->string('apartment_id');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parking_permits');
    }
}
