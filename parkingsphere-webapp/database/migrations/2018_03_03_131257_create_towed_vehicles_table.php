<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTowedVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('towed_vehicles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('vehicle_model');
            $table->string('vehicle_license_no');
            $table->string('vehicle_color');
            $table->string('vehicle_id');
            // user/visitor
            $table->string('user_type');
            $table->string('user_id')->nullable();            
            $table->string('apartment_id');         
            $table->string('towing_company_id');         
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('towed_vehicles');
    }
}
