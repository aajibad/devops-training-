<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminSubscriptionHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_subscription_history', function (Blueprint $table) {
            $table->increments('id');
            $table->string('amount');
            $table->string('discount')->nullable();
            $table->string('total');
            $table->string('transaction_id');
            $table->string('subscription_type');
            $table->string('payment_status');
            $table->string('apartment_id');
            $table->string('admin_id');
            $table->string('subscription_id');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_subscription_history');
    }
}
