<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisitorParkingPermitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visitor_parking_permits', function (Blueprint $table) {
            $table->increments('id');
            $table->string('phone');
            $table->string('parking_spot');
            $table->string('parking_spot_meta');
            $table->string('start_from');
            $table->string('expired_on');
            $table->string('status');
            $table->string('vehicle_id')->nullable();
            $table->string('generated_by');
            $table->string('apartment_id');
            $table->string('ticket_token');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visitor_parking_permits');
    }
}
