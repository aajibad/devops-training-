<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscriptions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('subscription_type')->nullable();
            // $table->string('amount')->nullable();
            $table->timestamp('expiry_date')->nullable();
            // Required / Expired / Success / Failed
            $table->string('status')->default('required');
            $table->boolean('billing_plan')->default(false);
            $table->boolean('billing_plan_state')->default(false);
            $table->boolean('auto_subscription')->default(false);
            $table->string('payment_definition_id')->nullable();
            $table->string('subscription_plan_id')->nullable();
            $table->string('subscription_agreement_id')->nullable();
            $table->string('payer_email')->nullable();
            $table->string('payer_id')->nullable();
            $table->string('apartment_id')->nullable();
            $table->string('admin_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscriptions');
    }
}
