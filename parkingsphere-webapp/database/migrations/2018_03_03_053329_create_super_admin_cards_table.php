<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuperAdminCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('super_admin_cards', function (Blueprint $table) {
            $table->increments('id');
            $table->string('card_holder');
            $table->string('card_type');
            $table->string('card_number');
            $table->date('expiry');
            $table->string('cvv');
            $table->boolean('auto_payment')->default(0);
            $table->string('user_id')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('super_admin_cards');
    }
}
