<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_options', function (Blueprint $table) {
            $table->increments('id');
            // $table->string('no_of_floor_permit')->nullable();
            // $table->string('no_of_building_permit')->nullable();
            $table->string('amount_permit_monthly')->default(0);
            $table->string('discount_permit_monthly')->default(0);
            $table->string('amount_permit_yearly')->default(0);
            $table->string('discount_permit_yearly')->default(0);
            $table->string('max_visitor_permit')->default(0);
            $table->string('max_visitor_permit_duration')->default(0);
            $table->longText('apartment_parking_policy')->nullable();
            $table->string('apartment_id');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_options');
    }
}
