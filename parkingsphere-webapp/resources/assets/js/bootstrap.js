
window._ = require('lodash');

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

try {
    window.$ = window.jQuery = require('jquery');

    require('bootstrap-sass');
} catch (e) {}

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

/**
 * Next we will register the CSRF Token as a common header with Axios so that
 * all outgoing HTTP requests automatically have it attached. This is just
 * a simple convenience so we don't have to attach every token manually.
 */

let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */
// import Echo from 'laravel-echo'

// window.Pusher = require('pusher-js');

// window.Echo = new Echo({
//     authEndpoint : 'https://localhost/apartment/public/admin/broadcasting/auth',
//     broadcaster: 'pusher',
//     key: '19e910c1743b19fea7e1',
//     cluster: 'ap2',
//     encrypted: true
// });


// Using socket.io server
// import io from 'socket.io-client';
// window.Io=io('http://localhost:3000');

import io from 'socket.io-client';
window.Io=io('http://18.205.149.183:5000');

// My CredimportScripts
// app_id = "537216"
// key = "19e910c1743b19fea7e1"
// secret = "9ceeed7b9f9f98d8b0e7"
// cluster = "ap2"

// Client Credentials
// app_id = "502902"
// key = "2c2c25a75cc53c96ac08"
// secret = "b3a3179a973be63dedce"
// cluster = "us2"