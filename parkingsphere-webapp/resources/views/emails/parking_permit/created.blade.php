@component('mail::message')
# New Parking permit has been purchased.

### Parking permit details.

@component('mail::panel')
<p>Parking spot : <b>{{$parking_permit->parking_spot}}</b></p>
<p>Payment Term: <b>{{$parking_permit->payment_term}}</b></p>
<p>Payment Amount : <b>{{$parking_permit->payment_amount}}</b></p>
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
