@component('mail::message')
# New user registration

@component('mail::panel')
##### Username : {{$admin->name}}
<p>
    Email : **{{$admin->email}}**
</p>

@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
