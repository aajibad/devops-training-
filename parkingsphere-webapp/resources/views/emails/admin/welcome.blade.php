@component('mail::message')
# Welcome to ParkingSphere.com

Thank you for the registration on **[parkingsphere.com](https://www.parkingsphere.com)**

<p><b>Hi! {{$admin->name}},</b> Your account was successfully created !</p>

@component('mail::button', ['url' => 'http://35.173.177.85/parkingsphere/public/admin/login'])
Go to dashboard
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
