@component('mail::message')
# New Apartment Registered


### Apartment Name: **{{$apartment->name}}** 
<br>
@component('mail::panel')
    <p>Subscription: <b>{{$apartment->subscription_type}}</b></p>    
    <p>Phone: <b>{{$apartment->phone}}</b></p>
    <p>Address: <b>{{$apartment->address}}</b></p>
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
