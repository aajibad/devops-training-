@component('mail::message')
# Welcome to ParkingSphere.com

Thank you for the registration on <br>**[parkingsphere.com](https://www.parkingsphere.com)**

<p>**Hi! {{$user->name}},** Your account was successfully created !</p>


Thanks,<br>
{{ config('app.name') }}
@endcomponent
