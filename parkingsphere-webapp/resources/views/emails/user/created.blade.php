@component('mail::message')
# New user registered

@component('mail::panel')
---
#### Username : {{$user->name}}
<p>
    Email : **{{$user->email}}**
    <br>
    Apartment : **{{$user->apartment->name}}**
</p>
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
