<div class="row clearfix">
    @foreach($user->apartments as $apartment)
    <div class="col-xs-12 ol-sm-12 col-md-12 col-lg-12">
        <div class="panel-group" id="accordion_1" role="tablist" aria-multiselectable="true">
            <div class="panel panel-col-indigo">
                <div class="panel-heading" role="tab" id="heading_{{$apartment->id}}">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion_1" href="#{{$apartment->id}}" aria-expanded="false" aria-controls="{{$apartment->id}}">
                            {{$apartment->name}}                               
                        </a>                                
                    </h4>
                </div>
                <div id="{{$apartment->id}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_{{$apartment->id}}">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <img src="{{ secure_asset($apartment->logo)}}" style="width:100%;" class="img-resposive">
                            </div>
                            <div class="col-md-12">
                                <h6>{{$apartment->name}}</h6>
                                </hr>
                                <div class="col-md-6">
                                    <label>Phone</label>
                                    <p>{{$apartment->phone}}</p>
                                </div>
                                <div class="col-md-6">
                                    <label>No of users</label>
                                    <p>{{count($apartment->users)}}</p>
                                </div>
                                <div class="col-md-12">
                                    <label>Address</label>
                                    <p>{{$apartment->address}}</p>
                                </div>   
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endforeach
</div>