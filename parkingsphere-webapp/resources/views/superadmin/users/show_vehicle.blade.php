<div class="row clearfix">
    <table id="example" class="table table-responsive" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Model</th>
                <th>Color</th>
                <th>License No</th>
                <th>Apartment</th>
            </tr>
        </thead>
        <tbody>
            @if(count($user->vehicles)!=0)
                @foreach($user->vehicles as $vehicle)
                <tr>
                    <td>{{$vehicle->model}}</td>
                    <td>{{$vehicle->color}}</td>
                    <td>{{$vehicle->license_no}}</td>
                    <td>{{$vehicle->apartment->name}}</td>
                </tr>
                <tr>    
                    <td colspan="4">
                        <div id="user_lightgallery" style="padding:0px;">
                            @foreach($vehicle->images as $image)                        
                            <div class="item thumbnail" data-src="{{secure_asset($image->path)}}">
                                <img class="portrait" src="{{secure_asset($image->path)}}">
                            </div>
                            @endforeach
                        </div>
                    </td>     
                </tr>
                @endforeach
            @else
            <tr>
                <td colspan="4">
                    <p style="text-align:center; margin:10px 0px;">No vehicles found.</p>
                </td>        
            </tr>
            @endif
        <tbody>
    </table>
</div>