<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="header">
        <h2 class="font-20">Housing Managers</h2>
        <small>Show apartments | Subscription history | Block all apartments</small>
      </div>
      <div class="body">
        <table id="example" class="mdl-data-table datatable" cellspacing="0" width="100%">
          <thead>
              <tr>
                <th>Avatar</th>
                <th>Username</th>
                <th>Email</th>
                <th>Apartments</th>
                {{--  <th>No of users</th>  --}}
                <th>Registered</th>
                <th>Action</th>
              </tr>
          </thead>
          <tbody>
            @foreach($admins as $admin)
              <tr>
                  <td>
                    <img src="{{ secure_asset(is_null($admin->avatar)?'images/admin/user.jpg':$admin->avatar) }}" style="width:35%; heigth:35%;" class="img-responsive">
                  </td>
                  <td>{{$admin->name}}</td>
                  <td>{{$admin->email}}</td>
                  <td>{{count($admin->apartments)}}</td>
                  {{--  <td></td>  --}}
                  <td>{{$admin->created_at}}</td>
                  <td>
                    <button class="btn btn-info btn-block btn-sm" data-toggle="modal" onclick="showApartment('{{route('superadmin.user.apartments',['id'=>$admin->id])}}')">View Apartments</button>
                    {{-- <button class="btn btn-primary btn-block btn-sm" onclick="showSubscription('{{route('superadmin.user.subscriptions',['id'=>$admin->id])}}')">Subscription History</button> --}}
                    {{--  Block all apartments  --}}
                    <form method="post" action="{{route('superadmin.apartments.block')}}" style="margin-top:5px;">
                        {{csrf_field()}}
                        <input type="hidden" name="id" value="{{$admin->id}}">
                        <button type="submit" class="btn btn-danger btn-block btn-sm">Block</button>
                    </form>
                  </td>
              </tr>
            @endforeach
          <tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="header">
        <h2 class="font-20">Users (Tenants)</h2>
        <small>Show vehicle information | Block user</small>
      </div>
      <div class="body">
          <table id="example" class="mdl-data-table datatable" cellspacing="0" width="100%">
              <thead>
                  <tr>
                    <th>Avatar</th>
                    <th>Username</th>
                    <th>Email</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Room No</th>
                    <th>Apartment</th>
                    <th>Action</th>
                  </tr>
              </thead>
              <tbody>
                @foreach($users as $user)
                  <tr>
                      <td>
                        <img src="{{ secure_asset(is_null($user->avatar)?'images/admin/user.jpg':$user->avatar) }}" style="width:30%; heigth:30%;" class="img-responsive">
                      </td>
                      <td>{{$user->name}}</td>
                      <td>{{$user->email}}</td>
                      <td>{{$user->firstname}}</td>
                      <td>{{$user->lastname}}</td>
                      <td>{{$user->room_no}}</td>
                      <td>{{$user->apartment->name}}</td>
                      <td>
                        <button class="btn btn-info btn-sm btn-block" onclick="showVehicleInfo('{{route('superadmin.user.vehicle',['id'=>$user->id])}}')">Vehicle Info</button>
                        {{-- <button class="btn btn-danger btn-sm btn-block">Block</button> --}}
                      </td>
                  </tr>
                @endforeach
              <tbody>
            </table>
      </div>
    </div>
  </div>
</div>

<!-- Default Size -->
<div class="modal fade" id="apartmentModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">Registered Apartments</h4>
            </div>
            <div class="modal-body" id="apartment-model-content">
                <p style="margin:5% 0px; text-align:center; font-weight:600;">Please wait...</p> 
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>

<!-- Default Size -->
<div class="modal fade" id="tenantVehicleModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">Registered Vehicle</h4>
            </div>
            <div class="modal-body" id="vehicle-model-content">
                <p style="margin:5% 0px; text-align:center; font-weight:600;">Please wait...</p> 
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>

<!-- Default Size -->
<div class="modal fade" id="subscriptionModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">Subscriptions</h4>
            </div>
            <div class="modal-body" id="subscription-model-content">
                <p style="margin:5% 0px; text-align:center; font-weight:600;">Please wait...</p> 
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>
@section('script')
  <script>
    function showApartment(url){
      $('#apartmentModal').modal();
      $.get(url,function(html){
        $('#apartment-model-content').html(html);
      })
    }

    function showSubscription(url){
      $modal=$('#subscriptionModal');
      $modal.modal();
      $.get(url,function(html){
        $('#subscription-model-content').html(html);
        $modal.find('.datatable').dataTable();
      })
    }

    function showVehicleInfo(url){
      $modal=$('#tenantVehicleModal');
      $modal.modal();
      $.get(url,function(html){
        $('#vehicle-model-content').html(html);
        $("#user_lightgallery").lightGallery({
          thumbnail:true,
          animateThumb: true,
          download: false,
          selector: '.item'
        }); 
      })
    }

    $("#user_lightgallery").lightGallery({
      thumbnail:true,
      animateThumb: true,
      download: false,
      selector: '.item'
    }); 

  </script>
@endsection
