<table id="example" class="mdl-data-table datatable" cellspacing="0" width="100%">
    <thead>
        <tr>
        <th>Apartment</th>  
        <th>Manager Id</th>
        <th>Manager</th>
        <th>Subscription Type</th>
        <th>Last Paid</th>
        <th>Expired At</th>
        <th>Status</th>
        {{--  <th>Action</th>  --}}
        </tr>
    </thead>
    <tbody>
        @foreach($subscriptions as $subscription)
        <tr>
            <td>{{$subscription->apartment}}</td>
            <td>{{$subscription->payer->id}}</td>
            <td>{{$subscription->payer->name}}</td>
            <td>{{$subscription->apartment->subscription}}</td>
            <td>{{$subscription->created_at}}</td>
            <td>{{$subscription->created_at}}</td>
            <td>
                @if($subscription->status=='')
                <h4 class="label label-danger"></h4>
                @elseif($subscription->status=='')
                <h4 class="label label-danger"></h4>
                @else
                <h4 class="label label-danger"></h4>
                @endif
            </td>
        </tr>
        @endforeach
    </tbody>
</table>