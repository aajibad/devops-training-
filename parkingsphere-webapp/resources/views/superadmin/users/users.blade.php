@extends('layouts.core_super')

@section('content_body')
  @include('superadmin.users.create')
  @include('superadmin.users.list')
@endsection