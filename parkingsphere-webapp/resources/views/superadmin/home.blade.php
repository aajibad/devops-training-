@extends('layouts.core_super')

@section('style')
    <style>
        .custom-form-control-rate{
            width: 55%;
            border: 1px solid gainsboro;
            border-radius: 0px;
        }
        .custom-form-control-discount{
            width: 43%;
            margin-left:2px;
            border: 1px solid gainsboro;
            border-radius: 0px;
        }

        .custom-form-control:focus{
            border-color: cadetblue;
        }
        /* #app-clock{
            padding:15px; 
            background:#fff; 
            min-height:100px; 
            height:auto; 
            margin:0px 15px 25px 15px; 
            box-shadow:1px 0px 1px 2px gainsboro;
        } */
    </style>
@endsection

@section('content_body')
  <!-- Hover Expand Effect -->

  <?php 
    date_default_timezone_set('America/New_York');
    $time = date("H");
    /* Set the $timezone variable to become the current timezone */
  ?>

    <div class="row">
        <div id="app-clock">
            <div class="col-md-9">
                @if ($time < "12")
                    <h3 class="col-cyan" style="margin:0px; text-transform:capitalize;">
                        {{"Good morning !"}} {{Auth::guard('superadmin')->user()->name}}
                    </h3>
                @elseif ($time >= "12" && $time < "17")
                    <h3 class="col-orange" style="margin:0px; text-transform:capitalize;">
                        {{"Good afternoon !"}} {{Auth::guard('superadmin')->user()->name}}
                        {{-- <small>Its time now</small>    --}}
                    </h3>
                @elseif ($time >= "17" && $time < "19")
                    <h3 class="col-teal" style="margin:0px; text-transform:capitalize;">
                        {{"Good Evening !"}} {{Auth::guard('superadmin')->user()->name}}
                    </h3> 
                @elseif ($time >= "19")
                    <h3 class="col-blue-grey" style="margin:0px; text-transform:capitalize;">
                        {{"Good Night !"}} {{Auth::guard('superadmin')->user()->name}}
                    </h3>
                @endif
                <h6 style="margin-top:3px;" class="col-blue-grey">Welcome to Superadmin Dashboard</h6>
            </div>
            <div class="col-md-3">
                <b v-text="currentDate" class="col-teal"></b>
                <h3 v-text="currentTime" style="margin-top:0px;" class="col-blue-grey"></h3>
            </div>
        </div>
    </div>

  <div class="row">
      <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
          <div class="info-box-2 bg-teal hover-expand-effect">
              <div class="icon">
                  <i class="material-icons">timelapse</i>
              </div>
              <div class="content">
                  <div class="text">NO OF HOUSING MANAGERS</div>
                  <div class="number">{{count($admins)}}</div>
              </div>
          </div>
      </div>
      {{--  <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
          <div class="info-box-2 bg-green hover-expand-effect">
              <div class="icon">
                  <i class="material-icons">drive_eta</i>
              </div>
              <div class="content">
                  <div class="text">TOWED VEHICLES</div>
                  <div class="number">05</div>
              </div>
          </div>
      </div>  --}}
      <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
          <div class="info-box-2 bg-light-green hover-expand-effect">
              <div class="icon">
                  <i class="material-icons">attach_money</i>
              </div>
              <div class="content">
                  <div class="text">TODAY'S REVENUE</div>
                  <div class="number">$250.00</div>
              </div>
          </div>
      </div>
  </div>
  <!-- #END# Hover Expand Effect -->

  <div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="header">
                <h2>Change subscriptions rate</h2>
            </div>
            <div class="body">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="info-box-2 bg-blue-grey">
                            <div class="icon">
                                <i class="material-icons">attach_money</i>
                            </div>
                            <div class="content">
                                <div class="text">MONTHLY</div>
                                <div class="number">{{number_format($option->getRate('monthly'),2,'.',',')}} / {{$option->getDiscount('monthly')}}%</div>
                            </div>
                    </div>
                    <div>
                        <form action="{{route('superadmin.option.create')}}" method="post">
                            {{csrf_field()}}
                            <input type="hidden" name="subscription_type" value="monthly">  
                            <input type="number" class="custom-form-control-rate input-lg" name="value" placeholder="Rate" value="{{$option->getRate('monthly')}}" onkeypress="saveOption(this,event)">
                            <input type="number" class="custom-form-control-discount input-lg" name="discount" placeholder="Discount" value="{{$option->getDiscount('monthly')}}" onkeypress="saveOption(this,event)">
                        </form>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="info-box-2 bg-blue-grey">
                        <div class="icon bg-default">
                            <i class="material-icons">attach_money</i>
                        </div>
                        <div class="content">
                            <div class="text">3 MONTHS</div>
                            <div class="number">{{number_format($option->getRate('3_months'),2,'.',',')}} / {{$option->getDiscount('3_months')}}%</div>
                        </div>
                    </div>
                    <div>
                        <form action="{{route('superadmin.option.create')}}" method="post">
                            {{csrf_field()}}
                            <input type="hidden" name="subscription_type" value="3_months">  
                            <input type="number" class="custom-form-control-rate input-lg" name="value" placeholder="Rate" value="{{$option->getRate('3_months')}}" onkeypress="saveOption(this,event)">
                            <input type="number" class="custom-form-control-discount input-lg" name="discount" placeholder="Discount" value="{{$option->getDiscount('3_months')}}" onkeypress="saveOption(this,event)">
                        </form>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="info-box-2 bg-blue-grey">
                        <div class="icon">
                            <i class="material-icons">attach_money</i>
                        </div>
                        <div class="content">
                            <div class="text">1 YEAR</div>
                            <div class="number">{{number_format($option->getRate('1_year'),2,'.',',')}} / {{$option->getDiscount('1_year')}}%</div>
                        </div>
                    </div>
                    <div>
                        <form action="{{route('superadmin.option.create')}}" method="post">
                            {{csrf_field()}} 
                            <input type="hidden" name="subscription_type" value="1_year"> 
                            <input type="number" class="custom-form-control-rate input-lg" name="value" placeholder="Rate" value="{{$option->getRate('1_year')}}" onkeypress="saveOption(this,event)">
                            <input type="number" class="custom-form-control-discount input-lg" name="discount" placeholder="Discount" value="{{$option->getDiscount('1_year')}}" onkeypress="saveOption(this,event)">
                        </form>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
  </div>    

   <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="header">
          <h2><span class="label label-danger">Expired</span> Subscriptions</h2>
        </div>
        <div class="body">
            <table id="example" class="mdl-data-table datatable" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Apartment</th>  
                    <th>Manager</th>
                    <th>Subscription Type</th>
                    <th>Subscription Amount</th>
                    <th>Last Paid</th>
                    <th>Expired At</th>
                    <th>Status</th>
                    {{-- <th>Action</th> --}}
                </tr>
            </thead>
            <tbody>
                @foreach($subscriptions as $subscription)
                    
                    {{-- @if($subscription->getStatus()=='expired' || $subscription->getStatus()=='required') --}}
                    @if($subscription->getStatus()=='expired')
                    <tr>
                        <td style="width:50px;">{{$subscription->apartment->name}}</td>
                        <td>{{$subscription->payer->name}}</td>
                        <td>
                            @if($subscription->subscription_type=='1_year')
                                1 YEAR
                            @elseif($subscription->subscription_type=='3_months')
                                3 MONTHS
                            @elseif($subscription->subscription_type=='monthly')
                                MONTHLY
                            @endif
                        </td>
                        <td>{{number_format($subscription->getAmount(),2,'.',',')}}</td>
                        <td>{{$subscription->updated_at}}</td>
                        <td>{{$subscription->expiry_date}}</td>
                        <td>
                            @if($subscription->status=='expired')
                                <h4 class="label label-danger">
                                    {{strtoupper($subscription->status)}}
                                </h4>
                            @elseif($subscription->status=='required')
                                <h4 class="label label-warning">
                                    {{strtoupper($subscription->status)}}
                                </h4>
                            @endif
                        </td>
                        {{-- <td>
                            @if($subscription->apartment->status=='active')
                                <form action="{{route('superadmin.apartment.change.status')}}" method="post">
                                    {{csrf_field()}}
                                    <input type="hidden" name="id" value="{{$subscription->apartment->id}}">
                                    <input type="hidden" name="status" value="blocked">
                                    <button type="submit" class="btn btn-danger btn-block">Block</button>
                                </form>
                            @elseif($subscription->apartment->status=='pending' || $subscription->apartment->status=='blocked')
                                <form action="{{route('superadmin.apartment.change.status')}}" method="post">
                                    {{csrf_field()}}
                                    <input type="hidden" name="id" value="{{$subscription->apartment->id}}">
                                    <input type="hidden" name="status" value="active">
                                    <button type="submit" class="btn btn-success btn-block">Activate</button>
                                </form>
                            @endif
                        </td> --}}
                    </tr>
                    @endif
                @endforeach
            </tbody>
            </table>
        </div>
      </div>
    </div>
  </div>
  </div>
@endsection

@section('script')
  <script>
    function saveOption(dom,e){
          if(e.keyCode==13){
            $(dom).closest('form').submit();
          }
      }

    var date=new Date();

    var app=new Vue({
        el:"#app-clock",
        data:{
            currentTime: null,
            currentDate: null
        },
        methods: {
            updateCurrentTime() {
                this.currentTime = moment().format('LTS');
            },
            updateCurrentDate() {
                this.currentDate = moment().format('LL');
            }
        },
        created() {
            moment.locale(); 
            this.currentTime = moment().format('LTS');
            this.currentDate = moment().format('LL');
            setInterval(() => this.updateCurrentTime(), 1 * 1000);
            setInterval(() => this.updateCurrentDate(), 1 * 1000);
        }
    })
  </script>
@endsection
