@extends('layouts.core_super')

@section('content_body')
  @include('superadmin.payments.create')
  @include('superadmin.payments.list')
@endsection

@section('script')
<script>
  $('.date').bootstrapMaterialDatePicker();
</script>
@endsection