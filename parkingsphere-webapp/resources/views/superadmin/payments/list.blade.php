<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="header">
        <h2>New Subscriptions</h2>
      </div>
      <div class="body">
        <table id="example" class="mdl-data-table datatable responsive" cellspacing="0" width="100%">
          <thead>
              <tr>
                <th>Apartment</th>
                <th>Subscription Type</th>
                <th>Amount</th>
                <th>Payment Status</th>
                <th>Auto Subscription</th>
                {{-- <th>Action</th> --}}
              </tr>
          </thead>
          <tbody>
            @foreach($subscriptions as $subscription)
              @if($subscription->getStatus()=='required')
                <tr>
                    <td>
                      {{$subscription->apartment->name}}
                    </td>
                    <td style="width:100px;">
                        @if($subscription->subscription_type=='monthly')
                          MONTHLY
                        @elseif($subscription->subscription_type=='3_months')
                          3 MONTHS
                        @elseif($subscription->subscription_type=='1_year')
                          1 YEAR
                        @endif
                    </td>
                    <td>${{number_format($subscription->amount,2,'.',',')}}</td>
                    <td>
                      @if($subscription->getStatus()=='required')
                        <span class="label label-warning">{{strtoupper($subscription->getStatus())}}</span>
                      @elseif($subscription->getStatus()=='success')
                        <span class="label label-success">{{strtoupper($subscription->getStatus())}}</span>
                      @elseif($subscription->getStatus()=='expired')
                        <span class="label label-danger">{{strtoupper($subscription->getStatus())}}</span>
                      @endif
                    </td>
                    <td>
                      <div class="switch">
                          <label><input type="checkbox" disabled="" {{$subscription->auto_subscription?'checked':''}}><span class="lever"></span><b>{{$subscription->auto_subscription?'ON':'OFF'}}</b></label>
                      </div>
                    </td>
                    {{-- <td>
                      @if($subscription->apartment->status=='active')
                      <form action="{{route('superadmin.apartment.change.status')}}" method="post">
                          {{csrf_field()}}
                          <input type="hidden" name="id" value="{{$subscription->apartment->id}}">
                          <input type="hidden" name="status" value="blocked">
                          <button type="submit" class="btn btn-danger btn-block">Block</button>
                        </form>
                      @elseif($subscription->apartment->status=='pending' || $subscription->apartment->status=='blocked')
                        <form action="{{route('superadmin.apartment.change.status')}}" method="post">
                          {{csrf_field()}}
                          <input type="hidden" name="id" value="{{$subscription->apartment->id}}">
                          <input type="hidden" name="status" value="active">
                          <button type="submit" class="btn btn-success btn-block">Activate</button>
                        </form>
                      @endif
                    </td> --}}
                </tr>
              @endif
            @endforeach
          <tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="header">
        <h2>Subscriptions (ACTIVE / EXPIRED)</h2>
      </div>
      <div class="body">
        <table id="example" class="mdl-data-table datatable responsive" cellspacing="0" width="100%">
          <thead>
              <tr>
                <th>Apartment</th>
                <th>Subscription Type</th>
                <th>Amount</th>
                <th>Payment Status</th>
                <th>Auto Subscription</th>
                {{-- <th>Action</th> --}}
              </tr>
          </thead>
          <tbody>
            @foreach($subscriptions as $subscription)
              @if($subscription->getStatus()!='required')
                <tr>
                    <td>
                      {{$subscription->apartment->name}}
                    </td>
                    <td style="width:100px;">
                        @if($subscription->subscription_type=='monthly')
                          MONTHLY
                        @elseif($subscription->subscription_type=='3_months')
                          3 MONTHS
                        @elseif($subscription->subscription_type=='1_year')
                          1 YEAR
                        @endif
                    </td>
                    <td>${{number_format($subscription->amount,2,'.',',')}}</td>
                    <td>
                      @if($subscription->getStatus()=='required')
                        <span class="label label-warning">{{strtoupper($subscription->getStatus())}}</span>
                      @elseif($subscription->getStatus()=='success')
                        <span class="label label-success">{{strtoupper($subscription->getStatus())}}</span>
                      @elseif($subscription->getStatus()=='expired')
                        <span class="label label-danger">{{strtoupper($subscription->getStatus())}}</span>
                      @endif
                    </td>
                    <td>
                      <div class="switch">
                          <label><input type="checkbox" disabled="" {{$subscription->auto_subscription?'checked':''}}><span class="lever"></span><b>{{$subscription->auto_subscription?'ON':'OFF'}}</b></label>
                      </div>
                    </td>
                    {{-- <td>
                      @if($subscription->apartment->status=='active')
                      <form action="{{route('superadmin.apartment.change.status')}}" method="post">
                          {{csrf_field()}}
                          <input type="hidden" name="id" value="{{$subscription->apartment->id}}">
                          <input type="hidden" name="status" value="blocked">
                          <button type="submit" class="btn btn-danger btn-block">Block</button>
                        </form>
                      @elseif($subscription->apartment->status=='pending' || $subscription->apartment->status=='blocked')
                        <form action="{{route('superadmin.apartment.change.status')}}" method="post">
                          {{csrf_field()}}
                          <input type="hidden" name="id" value="{{$subscription->apartment->id}}">
                          <input type="hidden" name="status" value="active">
                          <button type="submit" class="btn btn-success btn-block">Activate</button>
                        </form>
                      @endif
                    </td> --}}
                </tr>
              @endif
            @endforeach
          <tbody>
        </table>
      </div>
    </div>
  </div>
</div>
  
{{-- <div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="header">
        <h2>Billing Plans</h2>
      </div>
      <div class="body">
        <table id="example" class="mdl-data-table datatable responsive" cellspacing="0" width="100%">
          <thead>
              <tr>
                <th>Name</th>
                <th>Description</th>
                <th>Type</th>
                <th>State</th>
                <th>Created</th>
                <th>Updated</th>
              </tr>
          </thead>
          <tbody>
            @foreach($billing_plans as $plan)
              <tr>
                <td>{{$plan->name}}</td>
                <td>{{$plan->description}}</td>
                <td>{{$plan->type}}</td>
                <td>
                  @if($plan->state=="CREATED")
                    <span class="label label-info">{{$plan->state}}</span>
                  @elseif($plan->state=="ACTIVE")
                    <span class="label label-primary">{{$plan->state}}</span>
                  @elseif($plan->state=="INACTIVE")
                    <span class="label label-danger">{{$plan->state}}</span>
                  @endif
                </td>
                <td>{{$plan->create_time}}</td>
                <td>{{$plan->update_time}}</td>
              </tr>
            @endforeach
          <tbody>
        </table>
      </div>
    </div>
  </div>
</div> --}}