@extends('layouts.core_super')

@section('content_body')
  @include('superadmin.apartments.create')
  @include('superadmin.apartments.list')
@endsection
