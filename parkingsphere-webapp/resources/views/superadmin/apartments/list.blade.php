<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="header">
        <h2>Registered Apartments</h2>
        <small>Change apartments status to control subscription payments.</small>
      </div>
      <div class="body">
        <table id="example" class="mdl-data-table datatable " cellspacing="0" width="100%">
          <thead>
              <tr>
                <th>Logo</th>
                <th>Name</th>
                <th>Status</th>
                <th>Phone</th>
                <th>Address</th>
                <th>Action</th>
              </tr>
          </thead>
          <tbody>
            @foreach($apartments as $apartment)
              <tr>
              <td style="width:140px;"><img src="{{secure_asset($apartment->logo)}}" class="img-resonsive" style="width:65%;"></td>
                  <td>{{$apartment->name}}</td>
                  <td>
                    @if($apartment->status=="active")
                      <label class="label label-success label-lg" >ACTIVE</label>
                    @elseif($apartment->status=="blocked")
                      <label class="label label-danger label-lg" >BLOCKED</label>
                    @else
                      <label class="label label-info label-lg" >PENDING</label>
                    @endif
                  </td>
                  <td>{{$apartment->phone}}</td>
                  <td>{{$apartment->address}}</td>
                  <td>
                    @if($apartment->status=='active')
                      <form action="{{route('superadmin.apartment.change.status')}}" method="post">
                        {{csrf_field()}}
                        <input type="hidden" name="id" value="{{$apartment->id}}">
                        <input type="hidden" name="status" value="blocked">
                        <button type="submit" class="btn btn-danger btn-block">Block</button>
                      </form>
                    @elseif($apartment->status=='pending' || $apartment->status=='blocked')
                      <form action="{{route('superadmin.apartment.change.status')}}" method="post">
                        {{csrf_field()}}
                        <input type="hidden" name="id" value="{{$apartment->id}}">
                        <input type="hidden" name="status" value="active">
                        <button type="submit" class="btn btn-success btn-block">Activate</button>
                      </form>
                    @endif
                    {{-- <button class="btn btn-block btn-primary" style="margin-top:5px;">Show Manager</button> --}}
                  </td>
              </tr>
            @endforeach
          <tbody>
        </table>
      </div>
    </div>
  </div>
</div>
