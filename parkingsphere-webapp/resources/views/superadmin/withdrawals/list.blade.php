<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="header">
        <h2>Withdrawal Requests</h2>
      </div>
      <table id="example" class="mdl-data-table datatable" cellspacing="0" width="100%">
        <thead>
            <tr>
              <th>User ID</th>
              <th>Username</th>
              <th>Apartment</th>
              <th>Wallet Balance</th>
              <th>Requested Amount</th>
              <th>Requested Date</th>
              <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <tr>
              <td>#453234</td>
              <td>John Smith</td>
              <td>New Apartment</td>
              <td>$3300.00</td>
              <td>$2500.00</td>              
              <td>03.02.2018</td>
              <td>
                <button class="btn btn-primary btn-sm">Release</button>
              </td>
            </tr>
            <tr>
              <td>#453554</td>
              <td>Will Smith</td>
              <td>New Apartment 1</td>
              <td>$3300.00</td>
              <td>$1000.00</td>              
              <td>17.01.2018</td>
              <td>
                <button class="btn btn-primary btn-sm">Release</button>
              </td>
            </tr>
        <tbody>
      </table>
    </div>
  </div>
</div>
