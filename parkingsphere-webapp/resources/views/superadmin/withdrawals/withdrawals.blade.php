@extends('layouts.core_super')

@section('content_body')
  @include('superadmin.withdrawals.create')
  @include('superadmin.withdrawals.list')
@endsection

@section('script')

@endsection