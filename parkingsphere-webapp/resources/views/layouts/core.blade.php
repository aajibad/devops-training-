@extends('layouts.main')

@section('body_class')
  class="theme-indigo"
@endsection

@section('content')

  <!-- Page Loader -->
    {{-- <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div> --}}
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <!-- #END# Search Bar -->

  <!-- Top Bar -->
  <nav class="navbar">
    <div class="container-fluid">
      <div class="navbar-header">
        <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
        <a href="javascript:void(0);" class="bars"></a>
        <a class="navbar-brand" href="{{url('/')}}">Parking<b>Sphere</b></a>
      </div>
      <div class="collapse navbar-collapse" id="navbar-collapse">
        <ul class="nav navbar-nav navbar-right">
          <!-- Call Search -->
          <li>
            <a href="javascript:void(0);" class="js-search" data-close="true">
              <i class="material-icons">search</i>
            </a>
          </li>
          <!-- #END# Call Search -->
          <!-- Notifications -->
          <!-- <li class="dropdown">
            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                              <i class="material-icons">notifications</i>
                              <span class="label-count">7</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">NOTIFICATIONS</li>
              <li class="body">
                <ul class="menu">
                  <li>
                    <a href="javascript:void(0);">
                      <div class="icon-circle bg-light-green">
                                                  <i class="material-icons">person_add</i>
                                              </div>
                      <div class="menu-info">
                                                  <h4>12 new members joined</h4>
                                                  <p>
                                                      <i class="material-icons">access_time</i> 14 mins ago</p>
                                              </div>
                    </a>
                  </li>
                  <li>
                    <a href="javascript:void(0);">
                      <div class="icon-circle bg-cyan">
                                                  <i class="material-icons">add_shopping_cart</i>
                                              </div>
                      <div class="menu-info">
                                                  <h4>4 sales made</h4>
                                                  <p>
                                                      <i class="material-icons">access_time</i> 22 mins ago
                                                  </p>
                                              </div>
                    </a>
                  </li>
                  <li>
                    <a href="javascript:void(0);">
                      <div class="icon-circle bg-red">
                                                  <i class="material-icons">delete_forever</i>
                                              </div>
                      <div class="menu-info">
                                                  <h4><b>Nancy Doe</b> deleted account</h4>
                                                  <p>
                                                      <i class="material-icons">access_time</i> 3 hours ago
                                                  </p>
                                              </div>
                    </a>
                  </li>
                  <li>
                    <a href="javascript:void(0);">
                      <div class="icon-circle bg-orange">
                        <i class="material-icons">mode_edit</i>
                      </div>
                      <div class="menu-info">
                        <h4><b>Nancy</b> changed name</h4>
                        <p>
                          <i class="material-icons">access_time</i> 2 hours ago
                        </p>
                      </div>
                    </a>
                  </li>
                  <li>
                    <a href="javascript:void(0);">
                      <div class="icon-circle bg-blue-grey">
                        <i class="material-icons">comment</i>
                      </div>
                      <div class="menu-info">
                        <h4><b>John</b> commented your post</h4>
                        <p>
                          <i class="material-icons">access_time</i> 4 hours ago
                        </p>
                      </div>
                    </a>
                  </li>
                  <li>
                    <a href="javascript:void(0);">
                      <div class="icon-circle bg-light-green">
                        <i class="material-icons">cached</i>
                      </div>
                      <div class="menu-info">
                        <h4><b>John</b> updated status</h4>
                        <p>
                          <i class="material-icons">access_time</i> 3 hours ago
                        </p>
                      </div>
                    </a>
                  </li>
                  <li>
                    <a href="javascript:void(0);">
                      <div class="icon-circle bg-purple">
                        <i class="material-icons">settings</i>
                      </div>
                      <div class="menu-info">
                        <h4>Settings updated</h4>
                        <p>
                          <i class="material-icons">access_time</i> Yesterday
                        </p>
                      </div>
                    </a>
                  </li>
                </ul>
              </li>
              <li class="footer">
                <a href="javascript:void(0);">View All Notifications</a>
              </li>
            </ul>
          </li> -->
          <!-- #END# Notifications -->
          {{-- <!-- Tasks -->
          <li class="dropdown">
                          <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                              <i class="material-icons">flag</i>
                              <span class="label-count">9</span>
                          </a>
                          <ul class="dropdown-menu">
                              <li class="header">TASKS</li>
                              <li class="body">
                                  <ul class="menu tasks">
                                      <li>
                                          <a href="javascript:void(0);">
                                              <h4>
                                                  Footer display issue
                                                  <small>32%</small>
                                              </h4>
                                              <div class="progress">
                                                  <div class="progress-bar bg-pink" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 32%">
                                                  </div>
                                              </div>
                                          </a>
                                      </li>
                                      <li>
                                          <a href="javascript:void(0);">
                                              <h4>
                                                  Make new buttons
                                                  <small>45%</small>
                                              </h4>
                                              <div class="progress">
                                                  <div class="progress-bar bg-cyan" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 45%">
                                                  </div>
                                              </div>
                                          </a>
                                      </li>
                                      <li>
                                          <a href="javascript:void(0);">
                                              <h4>
                                                  Create new dashboard
                                                  <small>54%</small>
                                              </h4>
                                              <div class="progress">
                                                  <div class="progress-bar bg-teal" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 54%">
                                                  </div>
                                              </div>
                                          </a>
                                      </li>
                                      <li>
                                          <a href="javascript:void(0);">
                                              <h4>
                                                  Solve transition issue
                                                  <small>65%</small>
                                              </h4>
                                              <div class="progress">
                                                  <div class="progress-bar bg-orange" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 65%">
                                                  </div>
                                              </div>
                                          </a>
                                      </li>
                                      <li>
                                          <a href="javascript:void(0);">
                                              <h4>
                                                  Answer GitHub questions
                                                  <small>92%</small>
                                              </h4>
                                              <div class="progress">
                                                  <div class="progress-bar bg-purple" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 92%">
                                                  </div>
                                              </div>
                                          </a>
                                      </li>
                                  </ul>
                              </li>
                              <li class="footer">
                                  <a href="javascript:void(0);">View All Tasks</a>
                              </li>
                          </ul>
                      </li>

          <!-- #END# Tasks -->
          <li class="pull-right"><a href="javascript:void(0);" class="js-right-sidebar" data-close="true"><i class="material-icons">more_vert</i></a></li> --}}
        </ul>
      </div>
    </div>
  </nav>
  <!-- #Top Bar -->

  <aside id="leftsidebar" class="sidebar">
    <!-- User Info -->
    <div class="user-info">
        <div class="image">
            <img src="{{secure_asset(is_null(Auth::user()->avatar)?'images/admin/user.jpg':Auth::user()->avatar)}}" width="48" height="48" alt="User" />
        </div>
        <div class="info-container">
            <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{Auth::user()->name}} (Housing Manager)</div>
            <div class="email">{{Session::get('admin_apartment')->name}}</div>
            <div class="btn-group user-helper-dropdown">
                <i class="material-icons" data-toggle="dropdown">keyboard_arrow_down</i>
                <ul class="dropdown-menu pull-right">
                    <li style="padding:5px 15px; color:#666; font-weight:500; text-align:left;">
                        Switch Apartment
                    </li>    
                    <li role="seperator" class="divider"></li>
                    @foreach(Auth::user()->apartments as $apartment)
                    <li class={{Session::get("admin_apartment")->id==$apartment->id?"active":""}}>
                        <a href="{{route('admin.apartment.switch',['apartment'=>$apartment->id])}}">{{$apartment->name}}</a>
                    </li>
                    @endforeach
                    <li role="seperator" class="divider"></li>
                    <li>
                        <a href="{{route('admin.users.show')}}"><i class="material-icons">person</i>Profile</a>
                    </li>
                    <li>
                        <form method="post" id="admin_logout_form" action="{{route('admin.logout')}}">
                        {{csrf_field()}}
                        </form>
                        <a href="" onclick="event.preventDefault(); $('#admin_logout_form').submit();">
                        <i class="material-icons">input</i>Sign Out
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- #User Info --> 
    <!-- Menu -->
    <div class="menu">
        <ul class="list">
            <li class="header">MAIN NAVIGATION</li>
            <li class="{{ Request::path() == 'admin/home' ? 'active' : '' }}">
                <a href="{{route('admin.home')}}">
                    <i class="material-icons">home</i>
                    <span>Home</span>
                </a>
            </li>
            <li class="{{ Request::path() == 'admin/payment' ? 'active' : '' }}">
                <a href="{{route('admin.payments')}}">
                    <i class="material-icons">local_atm</i>
                    <span>Subscription</span>
                </a>
            </li>
            <li class="{{ Request::path() == 'admin/user/approvals' ? 'active' : '' }}">
                <a href="{{route('admin.users.approvals')}}">
                    <i class="material-icons">perm_identity</i>
                    <span>User Approval Request</span>
                    <span class="badge {{$count_user_approvals>0?'bg-blue':'bg-blue-grey'}} round-badge">{{$count_user_approvals}}</span>
                </a>
            </li>
            {{-- <li class="{{ Request::path() == 'admin/card' ? 'active' : '' }}">
                <a href="{{route('admin.cards')}}">
                    <i class="material-icons">credit_card</i>
                    <span>Cards</span>
                </a>
            </li> --}}
            <li class="{{ Request::path() == 'admin/notification' ? 'active' : '' }}">
                <a href="{{route('admin.notifications')}}">
                    <i class="material-icons">textsms</i>
                    <span>Send Announcements</span>
                </a>
            </li>
            <li class="{{ Request::path() == 'admin/report' ? 'active' : '' }}">
                <a href="{{route('admin.report.form')}}">
                    <i class="material-icons">show_chart</i>
                    <span>Reports</span>
                </a>
            </li>
            <li class="{{ Request::path() == 'admin/towing' ? 'active' : '' }}">
                <a href="{{route('admin.towed_vehicles')}}">
                    <i class="material-icons">directions_car</i>
                    <span>Towed Vehicles</span>
                </a>
            </li>
            <li class="{{ Request::path() == 'admin/complaints' || Request::path() == 'admin/complaints/show' ? 'active' : '' }}">
                <a href="{{route('admin.complaints')}}">
                    <i class="material-icons">forum</i>
                    <span>Complaints</span>
                    {{-- <p class="badge bg-cyan" style="line-height:2; margin-top: 5px;margin-left: 10px;padding: 1px 5px;">10</p> --}}
                </a>
            </li>

            <li class="{{ Request::path() == 'admin/user' || Request::path() == 'admin/user/show' ? 'active' : '' }}">
                <a href="{{route('admin.users')}}">
                    <i class="material-icons">group</i>
                    <span>Users</span>
                </a>
            </li>
            <li class="{{ Request::path() == 'admin/settings' ? 'active' : '' }}">
                <a href="{{route('admin.settings')}}">
                    <i class="material-icons">settings</i>
                    <span>Settings</span>
                </a>
            </li>
        </ul>
    </div>

    <!-- #Menu -->
    <!-- Footer -->
    <div class="legal">
      <div class="copyright">
        Copyright &copy; 2018 <a href="javascript:void(0);">ParkingSphere.com</a>
      </div>
      {{-- <div class="version">
        <b>Version: </b> 1.0.5
      </div> --}}
    </div>
    <!-- #Footer -->
  </aside>

  <section class="content">
    <div class="container-fluid">
        @yield('content_body')
    </div>
  </section>

  <div id="chat-instance">
      <div class="chat-box" id="chat-box" style="display:none;">
          <div class="chat-open">
              <p class="chat-open-header">
                  Complaint Chat
                  <i class="material-icons" id="complaint-chat-box-close" style="float:right; padding-right:4px; padding-top:2px; font-size:20px;" onclick="closeChat()">clear</i>
                  <i class="material-icons" id="complaint-chat-box-open" style="float:right; padding-right:4px;" onclick="showChat()">keyboard_arrow_up</i>
              </p>
              <div class="box" id="complaint-chat">
                  <div style="width:100%; height:auto; min-height:15px;">
                      <div class="chat-box-header">
                          <p style="font-size:12px; margin:0px;">
                              @{{conversation.title}}
                          </p>
                      </div>
                  </div>
                  <div class="bs-example" style="overflow:hidden; height:auto; width:100%;" data-example-id="media-alignment">
                      <div class="media-sender-left" v-for="message in messages" v-if="message.user_type=='user'">
                          <div class="media-left">
                              <a href="javascript:void(0);">
                              <img class="media-object img-circle" :src="user_avatar" width="32" height="32">
                              </a>
                          </div>
                          <div class="media-body">
                              <span class="media-heading" style="text-transform:capitalize; margin:0px; text-align: left;font-size: 10px;">
                                  <span class="badge bg-indigo">@{{conversation.user.name}}</span> <span>@{{message.created_at}}</span>
                              </span>
                              <p style="text-align:left;">
                                  @{{message.body}}
                              </p>
                          </div>
                      </div>
                      <div class="media-sender-right" v-else>
                          <div class="media-body">
                            
                            <span class="media-heading" style="margin:0px; text-align: right;font-size: 10px;">
                                <span class="badge bg-pink">You</span>
                                <span>@{{message.created_at}}</span>
                            </span>
                            <p>
                                @{{message.body}}
                            </p>
                          </div>
                          <div class="media-right">
                              <a href="#">
                                  <img class="media-object img-circle" :src="admin_avatar" width="32" height="32">
                              </a>
                          </div>
                      </div>
                      <span id="chat-end"></span>
                  </div>
                  <div class="box-reply">
                      <form action="" id="chat-reply" method="POST" onsubmit="return false;">
                          {{csrf_field()}}
                          <input type="hidden" name="conversation_id" id="conversation_id" value="">
                          <input type="hidden" name="user_id" id="user_id" value="{{Auth::guard('admin')->user()->id}}">
                          <input type="text" name="body" id="message_body" autofocus required>    
                          <button type="button" v-on:click="sendChatReply()" class="btn btn-primary btn-sm">SEND</button>
                      </form>
                  </div>
              </div>
          </div>
      </div>
  </div>
@endsection
