<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link rel="stylesheet" type="text/css" href="{{ secure_asset('css/vendor.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ secure_asset('css/flat-admin.css') }}">

    <!-- Font Awesome -->
    <link href="{{ secure_asset('font-awesome/css/fontawesome-all.css') }}" rel="stylesheet">

    <!-- Theme -->
    <link rel="stylesheet" type="text/css" href="{{ secure_asset('css/theme/blue-sky.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ secure_asset('css/theme/blue.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ secure_asset('css/theme/red.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ secure_asset('css/theme/yellow.css') }}">
</head>
<body>
  <div class="app app-default">
    <div class="app-container app-login">
      <div class="flex-center">
        <div class="app-header"></div>
        <div class="app-body">
          <div class="loader-container text-center">
              <div class="icon">
                <div class="sk-folding-cube">
                    <div class="sk-cube1 sk-cube"></div>
                    <div class="sk-cube2 sk-cube"></div>
                    <div class="sk-cube4 sk-cube"></div>
                    <div class="sk-cube3 sk-cube"></div>
                  </div>
                </div>
              <div class="title">Logging in...</div>
          </div>
          <div class="app-block">
            @yield('content')
          </div>
        </div>
        <div class="app-footer">
        </div>
      </div>
    </div>
  </div>

  <!-- <script type="text/javascript" src="{{ secure_asset('js/vendor.js') }}"></script>
  <script type="text/javascript" src="{{ secure_asset('js/auth.js') }}"></script> -->
</body>
</html>
