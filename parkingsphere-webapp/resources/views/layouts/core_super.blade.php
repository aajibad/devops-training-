@extends('layouts.main')

@section('body_class')
  class="theme-indigo"
@endsection

@section('content')

  <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <!-- #END# Search Bar -->

  <!-- Top Bar -->
  <nav class="navbar">
    <div class="container-fluid">
      <div class="navbar-header">
        <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
        <a href="javascript:void(0);" class="bars"></a>
        <a class="navbar-brand" href="../../index.html">Parking<b>Sphere</b></a>
      </div>
      <div class="collapse navbar-collapse" id="navbar-collapse">
        <ul class="nav navbar-nav navbar-right">
          <!-- Call Search -->
          {{--  <li>
            <a href="javascript:void(0);" class="js-search" data-close="true">
              <i class="material-icons">search</i>
            </a>
          </li>  --}}
          <!-- #END# Call Search -->
          <!-- Notifications -->
          <li class="dropdown">
            <form method="post" id="admin_logout_form" action="{{route('superadmin.logout')}}">
                {{csrf_field()}}
            </form>
            <a href="javascript:void(0);" class="dropdown-toggle" onclick="event.preventDefault(); $('#admin_logout_form').submit();" data-toggle="dropdown" role="button">
                <i class="material-icons">input</i>
            </a>
          </li>
          <!-- #END# Notifications -->
          {{-- <!-- Tasks -->
          <li class="dropdown">
                          <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                              <i class="material-icons">flag</i>
                              <span class="label-count">9</span>
                          </a>
                          <ul class="dropdown-menu">
                              <li class="header">TASKS</li>
                              <li class="body">
                                  <ul class="menu tasks">
                                      <li>
                                          <a href="javascript:void(0);">
                                              <h4>
                                                  Footer display issue
                                                  <small>32%</small>
                                              </h4>
                                              <div class="progress">
                                                  <div class="progress-bar bg-pink" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 32%">
                                                  </div>
                                              </div>
                                          </a>
                                      </li>
                                      <li>
                                          <a href="javascript:void(0);">
                                              <h4>
                                                  Make new buttons
                                                  <small>45%</small>
                                              </h4>
                                              <div class="progress">
                                                  <div class="progress-bar bg-cyan" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 45%">
                                                  </div>
                                              </div>
                                          </a>
                                      </li>
                                      <li>
                                          <a href="javascript:void(0);">
                                              <h4>
                                                  Create new dashboard
                                                  <small>54%</small>
                                              </h4>
                                              <div class="progress">
                                                  <div class="progress-bar bg-teal" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 54%">
                                                  </div>
                                              </div>
                                          </a>
                                      </li>
                                      <li>
                                          <a href="javascript:void(0);">
                                              <h4>
                                                  Solve transition issue
                                                  <small>65%</small>
                                              </h4>
                                              <div class="progress">
                                                  <div class="progress-bar bg-orange" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 65%">
                                                  </div>
                                              </div>
                                          </a>
                                      </li>
                                      <li>
                                          <a href="javascript:void(0);">
                                              <h4>
                                                  Answer GitHub questions
                                                  <small>92%</small>
                                              </h4>
                                              <div class="progress">
                                                  <div class="progress-bar bg-purple" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 92%">
                                                  </div>
                                              </div>
                                          </a>
                                      </li>
                                  </ul>
                              </li>
                              <li class="footer">
                                  <a href="javascript:void(0);">View All Tasks</a>
                              </li>
                          </ul>
                      </li>

          <!-- #END# Tasks -->
          <li class="pull-right"><a href="javascript:void(0);" class="js-right-sidebar" data-close="true"><i class="material-icons">more_vert</i></a></li> --}}
        </ul>
      </div>
    </div>
  </nav>
  <!-- #Top Bar -->

  <aside id="leftsidebar" class="sidebar">
    <!-- User Info -->
    <div class="user-info">
                    <div class="image">
                        <img src="{{secure_asset('images/admin/user.jpg')}}" width="48" height="48" alt="User" />
                    </div>
                    <div class="info-container">
                        <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{Auth::user()->name}} (Super Admin)</div>
                        <div class="email">{{Auth::user()->email}}</div>
                        {{--  <div class="btn-group user-helper-dropdown">
                            <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                            <ul class="dropdown-menu pull-right">
                                <li>
                                  
                                </li>
                            </ul>
                        </div>  --}}
                    </div>
                </div>
    <!-- #User Info -->
    <!-- Menu -->
    <div class="menu">
                    <ul class="list">
                        <li class="header">MAIN NAVIGATION</li>
                        <li class="{{ Request::path() == 'superadmin/home' ? 'active' : '' }}">
                            <a href="{{route('superadmin.home')}}">
                                <i class="material-icons">home</i>
                                <span>Home</span>
                            </a>
                        </li>
                        <li class="{{ Request::path() == 'superadmin/apartments' ? 'active' : '' }}">
                            <a href="{{route('superadmin.apartments')}}">
                                <i class="material-icons">location_city</i>
                                <span>Apartments</span>
                            </a>
                        </li>
                        <li class="{{ Request::path() == 'superadmin/payment' ? 'active' : '' }}">
                            <a href="{{route('superadmin.payments')}}">
                                <i class="material-icons">account_balance_wallet</i>
                                <span>Subscriptions</span>
                            </a>
                        </li>
                        {{-- <li>
                            <a href="{{route('superadmin.withdrawals')}}">
                                <i class="material-icons">assignment_returned</i>
                                <span>Withdrawal Requests</span>
                            </a>
                        </li> --}}
                        <li class="{{ Request::path() == 'superadmin/user' ? 'active' : '' }}">
                            <a href="{{route('superadmin.users')}}">
                                <i class="material-icons">group</i>
                                <span>Users</span>
                            </a>
                        </li>
                        {{-- <li>
                            <a href="{{route('superadmin.users')}}">
                                <i class="material-icons">settings</i>
                                <span>Settings</span>
                            </a>
                        </li> --}}
                    </ul>
                </div>
    <!-- #Menu -->
    <!-- Footer -->
    <div class="legal">
      <div class="copyright">
        Copyright &copy; 2018 <a href="javascript:void(0);">ParkingSphere.com</a>
      </div>
      {{-- <div class="version">
        <b>Version: </b> 1.0.5
      </div> --}}
    </div>
    <!-- #Footer -->
  </aside>

  <section class="content">
    <div class="container-fluid">
        @yield('content_body')
    </div>
  </section>

@endsection