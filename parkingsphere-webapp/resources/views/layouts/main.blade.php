<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <title>{{ config('app.name', 'Laravel') }}</title>
    <link rel="icon" href="{{secure_asset('images/admin/logo.png')}}" type="image/png" sizes="16x16">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=ABeeZee" rel="stylesheet">

    <!-- Bootstrap Core Css -->
    <link href="{{secure_asset('admin/plugins/bootstrap/css/bootstrap.css')}}" rel="stylesheet">

    {{-- Morris charts --}}
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">

    <!-- Waves Effect Css -->
    <link href="{{secure_asset('admin/plugins/node-waves/waves.css')}}" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="{{secure_asset('admin/plugins/animate-css/animate.css')}}" rel="stylesheet" />

    <link href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/responsive/2.2.1/css/responsive.bootstrap.min.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/select/1.2.5/css/select.bootstrap.min.css" rel="stylesheet">

    <link href="https://cdnjs.cloudflare.com/ajax/libs/material-design-lite/1.1.0/material.min.css" rel="stylesheet" />

    <link type="text/css" rel="stylesheet" href="{{secure_asset('admin/plugins/light-gallery/css/lightgallery.css')}}" /> 

    {{-- <link href="https://cdn.datatables.net/1.10.16/css/dataTables.material.min.css" rel="stylesheet" /> --}}
    
    <!-- Bootstrap Select Css -->
    <link href="{{secure_asset('admin/plugins/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet" />

    <!-- Bootstrap Material Datetime Picker Css -->
    <link href="{{secure_asset('admin/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}" rel="stylesheet" />


    <!-- Custom Css -->
    <link href="{{secure_asset('admin/css/style.css')}}" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="{{secure_asset('admin/css/themes/all-themes.css')}}" rel="stylesheet" />

    {{-- Google Charts API --}}
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

    @yield('style')

    <style>
        .btn:hover{
            color:inherit;
        }
        h2{
            line-height: 25px!important;
        }
        .round-badge{
            border-radius:10%; 
            padding: 2.5% 9.2px;
            font-size: 12px!important;
            color:#ffffff!important;
        }
        .modal{
            z-index:800!important;
        }
        .modal-backdrop{
            z-index:700!important;
        }
        .thumbnail {
            position: relative;
            width: 100px;
            height: 100px;
            display: inline-block;
            overflow: hidden;
        }
        .thumbnail img {
            position: absolute;
            left: 50%;
            top: 50%;
            height: 100%;
            width: auto;
            -webkit-transform: translate(-50%,-50%);
                -ms-transform: translate(-50%,-50%);
                    transform: translate(-50%,-50%);
        }
        .thumbnail img.portrait {
            width: 100%;
            height: auto;
        }

        /* Chat Box */
        .chat-box{
            position: fixed;
            bottom: 0;
            right: 0;
            z-index:1000000000;
            margin-right: 10px;
        }
        .box{
            transition: height .3s ease;
            width: 300px;
            overflow: hidden;
            overflow-y: scroll;
            height: 0px;
            background: white;
            color: black;
            z-index: 9999;
        }
        .box .bs-example{
            padding: 5px;
            margin-top:28px;
        }
        .box-reply{
            position: absolute;
            right: 0;
            bottom: 0;
            width: 100%;
            display: none;
        }
        .box-reply input[type=text]{
            width: 73%;
            padding: 5px;
            height:30px;
            border-radius: 2px;
            border:1px solid rgb(23,75,140);
            background: #ffffff;
            float: left;
        }
        .box-reply button{
            width: 25%;
            background-color: #ffffff!important;
            border:0px;
            color: rgb(23,75,140);
            margin-left:5px;
            float: right;
        }
        .btn-primary:hover{
            color: white!important;
        }
        .btn-default:hover{
            color: #333;
        }
        .chat-extend{
            height:350px;
            transition: height .3s ease;
            margin-bottom: 42px;
        }
        .chat-extend .box-reply{
            display: block;
            border:2px solid rgb(23,75,140);
            padding:5px;
            background: rgb(23,75,140);
            overflow: hidden;
        }
        .chat-open {
            text-align: center;
            border-top-left-radius: 5px;
            border-top-right-radius: 5px;
            font-size: 20px;
            border: 2px solid rgb(23,75,140);
            background: rgb(23,75,140);
            box-shadow: 0px 1px 1px 1px rgb(23,75,140);
            color: #eaeaea;
        }
        .chat-open,.chat-open-header {
            cursor: pointer;
        }
        .chat-open-header {
            padding: 5px;
            font-weight: 600;
            font-size: 15px;
            margin: 4px 0px;
        }
        .chat-open-header i:hover{
            color: #40C4FF;
        }

        .chat-box-header{
            background: gainsboro;
            border-top: 2px solid rgb(23,75,140);
            border-right: 2px solid rgb(23,75,140);
            border-left: 2px solid rgb(23,75,140);
            position: absolute;
            top:5;
            height:auto;
            z-index:100;
            width: 100%;
            right:0;
        }
        #complaint-chat .media-sender-left{
            float: left;
            clear:right;
            margin-bottom: 15px;
            position: relative;
            width: 80%;
            padding: 5px;
            background-color: #C5CAE9;
            border-bottom-left-radius: 10px;
            border-top-right-radius: 10px;
            border-bottom-right-radius: 10px;
        }
        #complaint-chat .media-sender-right{
            float: right;
            clear:left;
            margin-bottom: 15px;
            position: relative;
            width: 80%;
            padding: 5px;
            background-color: #BBDEFB;
            border-bottom-left-radius: 10px;
            border-top-left-radius: 10px;
            border-bottom-right-radius: 10px;
        }
        #complaint-chat .media-sender-left::before {
            border-color: transparent #C5CAE9 transparent transparent;
            border-style: solid;
            border-width: 0px 12px 12px 0;
            content: "";
            height: 0;
            left: -5px;
            position: absolute;
            top: 0;
            width: 0;
        }
        #complaint-chat .media-sender-right::before {
            border-color: transparent #BBDEFB transparent transparent;
            border-style: solid;
            border-width: 0px 12px 12px 0;
            content: "";
            height: 0;
            right: -5px;
            position: absolute;
            top: 0;
            width: 0;
            transform: rotate(270deg);
        }
    </style>
</head>
  <body @yield('body_class')>

    @yield('content')
    <!-- Scripts -->
    <script src="{{secure_asset('js/app.js')}}"></script>

    {{-- <script src="{{secure_asset('admin/js/pages/forms/form-wizard.js')}}"></script> --}}

    <!-- Custom Js -->
    <script src="{{secure_asset('admin/js/admin.js')}}"></script>

    {{-- <script src="{{secure_asset('admin/js/pages/forms/form-wizard.js')}}"></script> --}}

    <!-- Select Plugin Js -->
    <script src="{{secure_asset('admin/plugins/bootstrap-select/js/bootstrap-select.min.js')}}"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="{{secure_asset('admin/plugins/jquery-slimscroll/jquery.slimscroll.js')}}"></script>

    <!-- Waves Effect Plugin Js -->

    <script src="{{secure_asset('admin/plugins/node-waves/waves.min.js')}}"></script>

    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    {{-- <script src="https://cdn.datatables.net/1.10.16/js/dataTables.material.min.js"></script> --}}
    
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.1/js/responsive.bootstrap.min.js"></script>

    <!-- A jQuery plugin that adds cross-browser mouse wheel support. (Optional) -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.min.js"></script>

    <script src="{{secure_asset('admin/plugins/light-gallery/js/lightgallery.min.js')}}"></script>

    <!-- Include lg-thumbnail -->
    <script src="{{secure_asset('admin/plugins/light-gallery/js/lg-thumbnail.min.js')}}"></script>
    
    <!-- Include other lightgallery plugins (Optional) -->
    <script src="{{secure_asset('admin/plugins/light-gallery/js/lg-zoom.min.js')}}"></script>

    {{-- <script src="{{secure_asset('admin/js/pages/medias/image-gallery.js')}}"></script> --}}

    <script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.bootstrap.min.js"></script>
    <srcipt src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>
    <script src="https://cdn.datatables.net/select/1.2.5/js/dataTables.select.min.js"></script>

    <!-- Autosize Plugin Js -->
    <script src="{{secure_asset('admin/plugins/autosize/autosize.min.js')}}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>

    <!-- Moment Plugin Js -->
    {{-- <script src="{{secure_asset('admin/plugins/momentjs/moment.js')}}"></script> --}}

    <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="{{secure_asset('admin/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}"></script>

    <!-- Bootstrap Notify Plugin Js -->

    <script src="{{secure_asset('admin/plugins/bootstrap-notify/bootstrap-notify.min.js')}}"></script>

    {{-- <script src="{{secure_asset('plugins/payform/jquery.payform.min.js')}}"></script> --}}

    {{-- <script src="{{secure_asset('admin/js/pages/charts/flot.js')}}"></script> --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.js" type="text/javascript"></script>

    <!-- Demo Js -->
    <script src="{{secure_asset('admin/js/demo.js')}}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.16/vue.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.2.61/jspdf.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf-autotable/2.3.2/jspdf.plugin.autotable.js"></script>

    <script src="https://www.paypalobjects.com/api/checkout.js"></script>
    
    @yield('script')
    
    <!-- Demo Js -->
    {{-- <script src="{{secure_asset('admin/js/demo.js')}}"></script> --}}


    <script type="text/javascript">
    $(document).ready(function() {
        
        $('.datatable').DataTable( {
            responsive: true,
            columnDefs: [
                {
                    targets: [ 0, 1, 2 ],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });

        $("#chat-reply #message_body").keyup(function(event) {
            if (event.keyCode === 13) {
                chat.sendChatReply();
            }
        });

    //   var allowDismiss=true;
    //             $.notify({
    //                 message: 'New message has been received.'
    //             },
    //             {
    //                 type: 'bg-teal',
    //                 allow_dismiss: allowDismiss,
    //                 newest_on_top: true,
    //                 timer: 4000,
    //                 placement: {
    //                     from: 'top',
    //                     align: 'right'
    //                 },
    //                 animate: {
    //                     enter: 'animated bounceInRight',
    //                     exit: 'animated bounceOutRight'
    //                 },
    //                 template: '<div data-notify="container" class="bootstrap-notify-container alert alert-dismissible {0} ' + (allowDismiss ? "p-r-35" : "") + '" role="alert">' +
    //                 '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
    //                 '<span data-notify="icon"></span> ' +
    //                 '<span data-notify="title">{1}</span> ' +
    //                 '<span data-notify="message">{2}</span>' +
    //                 '<button class="btn btn-default btn-sm" style="margin-top:5px;" onclick="openChat('+4+')">Open Chat</button>' +
    //                 '<div class="progress" data-notify="progressbar">' +
    //                 '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
    //                 '</div>' +
    //                 '<a href="{3}" target="{4}" data-notify="url"></a>' +
    //                 '</div>'
    //             });

      autosize($('textarea'));
    });

    @if(Auth::guard('admin')->check())

    $(document).ready(function(){
        if(localStorage['chatOpen']=="true"){
            openChat(localStorage['chatConversationId']);
        }
        else{
            closeChat();
        }
    });

    function chatScrollToBottom(){
        $('#complaint-chat').animate({ scrollTop:$('#complaint-chat').find(".bs-example").height() }, 0);
    }
    // closeChat();

    function showChat(){
        var $sel=$('#complaint-chat');
        if($sel.hasClass("chat-extend")){
            $("#complaint-chat-box-open").html("keyboard_arrow_up");
            $sel.removeClass("chat-extend");
            localStorage['chatHide']=true;
            $sel.animate({ scrollTop:$sel.find(".bs-example").height() }, 0);
        }
        else{
            $("#complaint-chat-box-open").html("keyboard_arrow_down");
            $sel.addClass("chat-extend");
            localStorage['chatHide']=false;
            setTimeout(chatScrollToBottom, 1000);
        }
    }

    var chatConversation="";
    var chatMessages="";
    var chatUserAvatar="";
    var chatAdminAvatar="";
    var conversationId="";
    
    function openChat(id){
        var chatBox=$('#chat-box');
        var $sel=$('#complaint-chat');

        chatBox.slideDown('200');
        chatBox.find("#chat-reply #conversation_id").val(id);
        localStorage['chatOpen'] = true;
        localStorage['chatConversationId'] = id;
        
        chat.getConversations(localStorage["chatConversationId"])

        if(localStorage['chatHide']=="true"){
            $("#complaint-chat-box-open").html("keyboard_arrow_up");
            $sel.removeClass("chat-extend");
        }
        else{
            $("#complaint-chat-box-open").html("keyboard_arrow_down");
            $sel.addClass("chat-extend");
            setTimeout(chatScrollToBottom, 1000);
        }
    }

    function openChatView(id){
        window.location='{{url("admin/complaints/show")}}?id='+id;
    }

    function closeChat(){
        $('#chat-box').slideUp('200');
        localStorage['chatOpen'] = false;
    }

    var chat=new Vue({
        el:'#chat-instance',
        data:{
            conversation:'',
            messages:'',
            user_avatar:'',
            admin_avatar:''
        },
        methods:{
            getConversations(id){
                axios.get('{{url('admin/complaints/show')}}?id='+id+'&json=true')
                .then(function (response) {
                    chat.conversation=response.data.conversation;
                    chat.messages=response.data.messages;
                    chat.user_avatar=response.data.user_avatar;
                    chat.admin_avatar=response.data.admin_avatar;
                    chat.listen();
                })
                .catch(function (error) {
                    console.log(error);
                });
            },
            sendChatReply:function(){
                var form = $('#chat-reply');

                axios.post('{{route('admin.complaint.reply')}}',form.serialize())
                .then(function (response) {
                    if(response.data.return=="success"){
                        data=response.data.message;
                        data["user"]=response.data.user;
                        chat.messages.push(data);
                        form.find("input[type=text]").val('').focus();
                        chatScrollToBottom();
                    }
                })
                .catch(function (error) {
                    console.log(error);
                });
            },
            listen(){
                Io.on('message-'+chat.conversation.id,function(response){
                    if(response.return=="success"){
                        data=response.message;
                        data["user"]=response.user;
                        chat.messages.push(data);
                        chatScrollToBottom();
                    }
                });
                // Echo.channel('con-'+chat.conversation.id+'-messages')
                //     .listen('ComplaintEvent',(response)=>{
                //         if(response.return=="success"){
                //             data=response.message;
                //             data["user"]=response.user;
                //             chat.messages.push(data);
                //             chatScrollToBottom();
                //         }
                //     })
            }
        }
    });
    @endif
    </script>
  </body>
</html>
