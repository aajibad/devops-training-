@extends('layouts.core')

@section('content_body')

{{-- <div class="row clearfix" id="conversations">
  <div class="col-md-12">
      <div class="list-group">
          <div v-for="conversation in conversations" style="overflow:hidden; margin-bottom:10px;" class="list-group-item">
              <a :href="'{{url('admin/complaints/show')}}?id='+conversation.id" style="text-decoration:none; color:black;">
                <h6 class="list-group-item-heading" style="text-transform:uppercase">@{{conversation.title}}</h6>
              </a>
              <small>Created : @{{conversation.created_at}}</small>
              <button class="btn btn-primary pull-right waves-effect" style="margin:5px 0px;" v-on:click="extendChat(conversation.id)">Open Chat</button>
          </div>
      </div>
  </div>
</div> --}}

<div class="row clearfix" id="conversations">
  <div class="col-md-12">
    @if(!is_null($conversations))
      <div class="list-group">
          @foreach($conversations as $conversation)
            <div style="overflow:hidden; margin-bottom:10px;" class="list-group-item">
                <a href="{{url('admin/complaints/show').'?id='.$conversation->id}}" style="text-decoration:none; color:black;">
                  <h6 class="list-group-item-heading" style="text-transform:uppercase">{{$conversation->title}}</h6>
                </a>
                <small>Created : {{$conversation->created_at}}</small>
                <button class="btn btn-primary pull-right waves-effect" style="margin:5px 0px;" v-on:click="extendChat({{$conversation->id}})">Open Chat</button>
            </div>
          @endforeach
      </div>
      {{$conversations->links()}}
    @else
      <div class="list-group">
        <div class="list-group-item">
          <p style="text-align:center; margin:15px 0px;" class="font-18">No more complaints has been received.</p>
        </div>
      </div>
    @endif
  </div>
</div>
@endsection

@section('script')
<script>
  
  var conversation=new Vue({
            el: '#conversations',
            data: {
              apartment_id:'',
              conversations:[],
            },
            mounted(){
                this.listen();
            },
            created: function () {
              axios.get('{{route('admin.complaints',['json'=>true])}}')
              .then(function (response) {
                conversation.apartment_id=response.data.apartment_id;
                conversation.conversations=response.data.conversations;
                console.log(response);
              })
              .catch(function (error) {
                console.log(error);
              });
            },
            methods:{
              extendChat(id){
                openChat(id);
              },
              listen(){
                Io.on("complaint-{{Session::get('admin_apartment')->id}}",function(data){
                  // message=JSON.parse(data);
                  // conversation.conversations.push(data.message.conversation); 
                  // console.log(data);
                  $.get('{{route('admin.complaints')}}',function(html){
                    var content=$(html).find("#conversations");
                    $("#conversations").html(content);
                  })
                })
              }
            }
          })
</script>
@endsection