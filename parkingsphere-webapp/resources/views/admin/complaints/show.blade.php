@extends('layouts.core')

@section('style')
<style>
    #app .media-sender-left{
        float: left;
        clear:right;
        margin-bottom: 15px;
        padding: 15px;
        position: relative;
        width: 80%;
        background-color: #C5CAE9;
        border-bottom-left-radius: 10px;
        border-top-right-radius: 10px;
        border-bottom-right-radius: 10px;
    }
    #app .media-sender-right{
        float: right;
        clear:left;
        margin-bottom: 15px;
        position: relative;
        width: 80%;
        padding: 15px;
        background-color: #BBDEFB;
        border-bottom-left-radius: 10px;
        border-top-left-radius: 10px;
        border-bottom-right-radius: 10px;
    }
    #app .media-sender-left::before {
        border-color: transparent #C5CAE9 transparent transparent;
        border-style: solid;
        border-width: 0 29px 29px 0;
        content: "";
        height: 0;
        left: -14px;
        position: absolute;
        top: 0;
        width: 0;
    }
    #app .media-sender-right::before {
        border-color: transparent #BBDEFB transparent transparent;
        border-style: solid;
        border-width: 0px 29px 29px 0px;
        content: "";
        height: 0;
        right: -14px;
        position: absolute;
        top: 0;
        width: 0;
        transform: rotate(270deg);
    }
</style>
@endsection

@section('content_body')
<div id="app">
    <div class="row" id="complaint-chat-space">
        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2 style="text-transform:uppercase">
                        @{{conversation.title}}
                    </h2>
                </div>
                <div class="body" style="overflow:hidden;">
                    <div class="bs-example" data-example-id="media-alignment">
                        <div class="media-sender-left" v-for="message in messages" v-if="message.user_type=='user'">
                            <div class="media-left">
                                <a href="javascript:void(0);">
                                <img class="media-object img-circle" :src="user_avatar" width="64" height="64">
                                </a>
                            </div>
                            <div class="media-body">
                                <span class="badge bg-pink" style="text-transform:capitalize;">@{{conversation.user.name}}</span>
                                <span style="font-size:10px; clear:both;">@{{message.created_at}}</span>
                                <p style="text-align:left;">
                                @{{message.body}}
                                </p>
                            </div>
                        </div>
                        <div class="media-sender-right" v-else>
                            <div class="media-body">
                                <span class="badge bg-indigo" style="float:right;">You </span>
                                <span style="font-size:10px; float:right; clear:both;">@{{message.created_at}}</span>
                                <p>@{{message.body}}</p>
                            </div>
                            <div class="media-right">
                                <a href="#">
                                    <img class="media-object img-circle" :src="admin_avatar" width="64" height="64">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="body">
                    <form action="" id="reply" method="POST" onsubmit="return false;">
                        <div class="form-group">
                            <div class="form-line">
                                {{csrf_field()}}
                                <input type="hidden" name="conversation_id" id="conversation_id" value="{{$conversation->id}}">
                                <input type="hidden" name="user_id" id="user_id" value="{{Auth::guard('admin')->user()->id}}">
                                <input type="text" name="body" id="message_body" class="form-control" placeholder="Type your reply here..." style="overflow: hidden; word-wrap: break-word; height: 32px;" required>
                            </div>
                            <button type="button" v-on:click="sendReply()"  class="btn btn-primary btn-lg btn-block" style="margin-top:15px;">SEND</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $(document).ready(function(){
        $("#app").animate({ scrollTop: $("#app").find("#complaint-chat-space").height() }, 1000);
        $("#message_body").keyup(function(event) {
            if (event.keyCode === 13) {
                app.sendReply();
            }
        });
    })
  var app=new Vue({
            el: '#app',
            data: {
              conversation:'',
              messages:[],
              user_avatar:'',
              admin_avatar:''
            },
            mounted(){
                this.getConversations();
            },
            methods:{
                getConversations(){
                    axios.get('{{url('admin/complaints/show')}}?id={{$conversation->id}}&json=true')
                    .then(function (response) {
                        app.conversation=response.data.conversation;
                        app.messages=response.data.messages;
                        app.user_avatar=response.data.user_avatar;
                        app.admin_avatar=response.data.admin_avatar;
                        app.listen();
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
                },
                sendReply:function(){
                    var form = $('#reply');

                    axios.post('{{route('admin.complaint.reply')}}',form.serialize())
                    .then(function (response) {
                        if(response.data.return=="success"){
                            data=response.data.message;
                            data["user"]=response.data.user;
                            app.messages.push(data);
                            form.find("input[type=text]").val('').focus();
                        }
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
                },
                listen(){
                    Io.on("message-"+app.conversation.id,function(response){
                        data=response.message;
                        data["user"]=response.user;
                        app.messages.push(data);
                    })
                    // Echo.channel("con-"+app.conversation.id+"-messages")
                    // .listen('ComplaintEvent',(response)=>{
                    //     console.log(response);
                    //     if(response.return=="success"){
                    //         data=response.message;
                    //         data["user"]=response.user;
                    //         app.messages.push(data);
                    //     }
                    // });
                }
            }
          })
</script>
@endsection