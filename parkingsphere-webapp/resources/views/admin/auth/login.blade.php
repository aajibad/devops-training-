@extends('layouts.main')

@section('body_class')
  class="login-page" style="margin:0px auto; background-size:cover; background:linear-gradient(to right, rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0.7)), url({{secure_asset('images/admin/login.jpeg')}}) no-repeat;";
@endsection

@section('style')
<style>
    /* @keyframes animatedBackground {
        from {
            background-position: 0 0;
        }
        to {
            background-position: 100% 0;
        }
    }
    body {
        background-position: 0px 0px;
        animation: animatedBackground 10s ease-out infinite alternate;
    } */
    .material-checkbox {
    position: relative;
    display: inline-block;
    color: #555;
    cursor: pointer;
    font-family: "Roboto", "Segoe UI", BlinkMacSystemFont, system-ui, -apple-system;
    font-size: 14px;
    line-height: 18px;
    }

    .material-checkbox > input {
    appearance: none;
    -moz-appearance: none;
    -webkit-appearance: none;
    position: absolute;
    z-index: -1;
    left: -15px;
    top: -15px;
    display: block;
    margin: 0;
    border-radius: 50%;
    width: 48px;
    height: 48px;
    background-color: rgba(0, 0, 0, 0.42);
    outline: none;
    opacity: 0;
    transform: scale(1);
    -ms-transform: scale(0); /* Graceful degradation for IE */
    transition: opacity 0.5s, transform 0.5s;
    }

    .material-checkbox > input:checked {
    background-color: #2196f3;
    }

    .material-checkbox:active > input {
    opacity: 1;
    transform: scale(0);
    transition: opacity 0s, transform 0s;
    }

    .material-checkbox > input:disabled {
    opacity: 0;
    }

    .material-checkbox > input:disabled + span {
    color: #555;
    cursor: initial;
    }

    .material-checkbox > span::before {
    content: "";
    display: inline-block;
    margin-right: 10px;
    border: solid 2px rgba(0, 0, 0, 0.42);
    border-radius: 2px;
    width: 20px;
    height: 20px;
    vertical-align: -4px;
    transition: border-color 0.5s, background-color 0.5s;
    }

    .material-checkbox > input:checked + span::before {
    border-color: #2196f3;
    background-color: #2196f3;
    }

    .material-checkbox > input:active + span::before {
    border-color: #2196f3;
    }

    .material-checkbox > input:checked:active + span::before {
    border-color: transparent;
    background-color: rgba(0, 0, 0, 0.42);
    }

    .material-checkbox > input:disabled + span::before {
    border-color: rgba(0, 0, 0, 0.26);
    }

    .material-checkbox > input:checked:disabled + span::before {
    border-color: transparent;
    background-color: rgba(0, 0, 0, 0.26);
    }

    .material-checkbox > span::after {
    content: "";
    display: inline-block;
    position: absolute;
    top: 0;
    left: 0;
    width: 8px;
    height: 14px;
    border: solid 2px transparent;
    border-left: none;
    border-top: none;
    transform: translate(5.5px, 1px) rotate(45deg);
    -ms-transform: translate(5.5px, 2px) rotate(45deg);
    }

    .material-checkbox > input:checked + span::after {
    border-color: #fff;
    }
</style>
@endsection

@section('content')
<!-- <div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Login</div>
                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Login
                                </button>

                                <a class="btn btn-link" href="W">
                                    Forgot Your Password?
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> -->

<div class="logo" style="padding:10px 0px; text-align:center;">
    <a href="javascript:void(0);">
        <img src="{{secure_asset('images/admin/logo-with-text.png')}}" style="width:50%;" alt="parkingsphere">
    </a>
</div>
<div class="login-box">
  <div class="card">
    <div class="body">
      <form id="sign_in" class="form-horizontal" method="POST" action="{{route('admin.login')}}">
          {{ csrf_field() }}
        <div class="msg">Sign in to start your session</div>
        <div class="input-group">
          <span class="input-group-addon">
                          <i class="material-icons">person</i>
                      </span>
          <div class="form-line">
            <input id="email" type="email" class="form-control" name="email" placeholder="Email" value="{{ old('email') }}" required autofocus>
            @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
          </div>
        </div>
        <div class="input-group">
          <span class="input-group-addon">
                          <i class="material-icons">lock</i>
                      </span>
          <div class="form-line">
            <input id="password" type="password" class="form-control" placeholder="Password" name="password" required>
            @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
          </div>
        </div>
        <div class="row">
          <div class="col-xs-8 p-t-5">
            <!-- <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }} class="filled-in chk-col-pink">
            <label for="rememberme">Remember Me</label> -->
            <label class="material-checkbox">
                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                <span>Remember Me</span>
            </label>
          </div>
          <div class="col-xs-4">
            <button class="btn btn-block bg-pink waves-effect" type="submit">SIGN IN</button>
          </div>
        </div>
        <div class="row m-t-15 m-b--20">
          <div class="col-xs-6">
            <a href="{{ route('admin.register') }}">Register Now</a>
          </div>
          <div class="col-xs-6 align-right">
            <a href="{{ route('admin.password.request') }}">Forgot Password?</a>
          </div>
        </div>
        <div style="margin:10px 0px; min-height:60px; height:auto;">
            <p style="text-align:center;">Or login with</p>
            {{-- <div style="width:49%; float:left;">
                <a href="" class="btn btn-block btn-lg bg-indigo waves-effect">FACEBOOK</a>
            </div> --}}
            <div style="width:100%;"> 
                <a href="{{url('admin/login/google')}}" class="btn btn-block btn-lg bg-red waves-effect">GOOGLE</a>                </div>
            </div>
        </div>
      </form>
    </div>
  </div>
</div>

@endsection

@section('script')

  <!-- Validation Plugin Js -->
  <script src="{{secure_asset('admin/plugins/jquery-validation/jquery.validate.js')}}"></script>
  <script src="{{secure_asset('admin/js/pages/examples/sign-in.js')}}"></script>

@endsection
