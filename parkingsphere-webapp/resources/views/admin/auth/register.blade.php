@extends('layouts.main')

@section('body_class')
  class="signup-page"
@endsection

@section('content')
<div class="signup-box">
    <div class="logo" style="margin-bottom:0px;">
      <a href="javascript:void(0);">
          <img src="{{secure_asset('images/admin/logo.png')}}" style="width:70%; position:relative; bottom:20px;" alt="parkingsphere">
      </a>
    </div>
    <div class="card" style="position:relative; bottom:35px;">
        <div class="body">
            <form id="sign_up" method="POST" action="{{ route('admin.register') }}">
                {{csrf_field()}}
                <div class="msg">Register a new membership</div>
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="material-icons">person</i>
                    </span>
                    <div class="form-line">
                        <input id="name" type="text" class="form-control" name="name" value="{{ $name }}" placeholder="Username" required autofocus>
                        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="material-icons">email</i>
                    </span>
                    <div class="form-line">
                        <input id="email" type="email" class="form-control" name="email" value="{{ $email }}" placeholder="Email" required>
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="material-icons">lock</i>
                    </span>
                    <div class="form-line">
                        <input id="password" type="password" class="form-control" name="password" minlength="6" placeholder="Password" required>
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="material-icons">lock</i>
                    </span>
                    <div class="form-line">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" minlength="6" placeholder="Confirm Password" required>
                    </div>
                </div>
                {{-- <div class="form-group">
                    <input type="checkbox" name="terms" id="terms" class="filled-in chk-col-pink">
                    <label for="terms">I read and agree to the <a href="javascript:void(0);">terms of usage</a>.</label>
                </div> --}}
                <input type="hidden" name="avatar" value="{{$avatar}}">
                <button class="btn btn-block btn-lg bg-pink waves-effect" type="submit">SIGN UP</button>
                <div class="m-t-25 m-b--5 align-center" style="width:100%;">
                  <a href="{{url('admin/login')}}">You already have a membership?</a>
                </div>
            </form>
        </div>
    </div>
</div>

{{-- <div class="container">
  <div class="panel panel-default">
    <div class="panel-body">

      <div class="stepper">
        <ul class="nav nav-pills" role="tablist">
          <li role="presentation" class="active">
            <a class="persistant-disabled" href="#stepper-step-1" data-toggle="tab" aria-controls="stepper-step-1" role="tab" title="Step 1">
              <span class="round-tab">Basic Information</span>
            </a>
          </li>
          <li role="presentation" class="disabled">
            <a class="persistant-disabled" href="#stepper-step-2" data-toggle="tab" aria-controls="stepper-step-2" role="tab" title="Step 2">
              <span class="round-tab">Apartment Information</span>
            </a>
          </li>
          <li role="presentation" class="disabled">
            <a class="persistant-disabled" href="#stepper-step-3" data-toggle="tab" aria-controls="stepper-step-3" role="tab" title="Step 3">
              <span class="round-tab">Subscription Detail</span>
            </a>
          </li>
        </ul>
        <form id="sign_up" method="POST" action="{{ route('admin.register') }}" enctype="multipart/form-data">
          {{ csrf_field() }}
          <div class="tab-content">
            <div class="tab-pane fade in active" style="padding:25px 0px; "role="tabpanel" id="stepper-step-1">
              <div class="col-md-12">
                <div class="form-group">
                                    <label for="name">Username</label>
                                    <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Username" required autofocus>
                                    @if ($errors->has('name'))
                                      <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                      </span>
                                    @endif
                                  </div>
                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email Address" required>
                                    @if ($errors->has('email'))
                                    <span class="help-block">
                                      <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                  </div>
                <div class="form-group">
                                    <label for="password">Password</label>
                                    <input id="password" type="password" class="form-control" name="password" minlength="6" placeholder="Password" required>
                                    @if ($errors->has('password'))
                                    <span class="help-block">
                                      <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                  </div>
                <div class="form-group">
                                    <label for="password-confirm">Re-type Password</label>
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" minlength="6" placeholder="Confirm Password" required>
                                  </div>
                <ul class="list-inline pull-right">
                  <li>
                    <a class="btn btn-primary next-step">Next</a>
                  </li>
                </ul>
              </div>
            </div>
            <div class="tab-pane fade" role="tabpanel" style="padding:25px 0px; " id="stepper-step-2">
              <div class="col-md-12">
                <div class="form-group">
                                    <label for="apartment_logo">Logo / Banner</label>
                                    <input id="apartment_logo" type="file" class="form-control" name="apartment_logo" value="" placeholder="apartment banner/logo" required autofocus>
                                  </div>
                <div class="form-group">
                                    <label for="apartment_name">Name</label>
                                    <input id="apartment_name" type="text" class="form-control" name="apartment_name" value="" required autofocus>
                                  </div>
                <div class="form-group">
                                    <label for="apartment_phone">Contact</label>
                                    <input id="apartment_phone" type="text" class="form-control" name="apartment_phone" value="" placeholder="" required>
                                  </div>
                <div class="form-group">
                  <label for="apartment_address">Address</label>
                  <input id="apartment_address" type="text" class="form-control" name="apartment_address" required>
                </div>
                <ul class="list-inline pull-right">
                  <li>
                    <a class="btn btn-default prev-step">Back</a>
                  </li>
                  <li>
                    <a class="btn btn-primary next-step">Next</a>
                  </li>
                </ul>
              </div>
            </div>
            <div class="tab-pane fade" role="tabpanel" style="padding:25px 0px; " id="stepper-step-3">
              <div class="col-md-12">
                <div class="form-group">
                  <div class="col-md-4">
                                    <input id="monthly" type="radio" class="form-check-input tgl-radio-tab-child" name="subscription" value="month">
                                    <label for="monthly" class="radio-inline monthly">
                                      Monthly
                                    </label>
                                    <i class="fas fa-check-circle"></i>
                                  </div>
                  <div class="col-md-4">
                                    <input id="3_months" type="radio" class="form-check-input tgl-radio-tab-child" name="subscription" value="3_months">
                                    <label for="3_months" class="radio-inline">
                                      <span>3</span>
                                      <br>Months
                                    </label>
                                    <i class="fas fa-check-circle"></i>
                                  </div>
                  <div class="col-md-4">
                                    <input id="1_year" type="radio" class="form-check-input tgl-radio-tab-child" name="subscription" value="1_year">
                                    <label for="1_year" class="radio-inline">
                                      <span>1</span>
                                      <br>Year
                                    </label>
                                    <i class="fas fa-check-circle"></i>
                                  </div>
                </div>
                <div class="col-md-12" style="margin-top:5%;">
                  <ul class="list-inline pull-right" style="margin:10px 0px;">
                    <li>
                      <a class="btn btn-default prev-step">Back</a>
                    </li>
                    <li>
                      <!-- <a class="btn btn-primary next-step">Submit Payment</a> -->
                      <input type="submit" class="btn btn-primary" value="Finish">
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>


    </div>
  </div>
</div> --}}



<!-- <section class="mdl-stepper-demo">
  <div class="mdl-grid">
    <div class="mdl-cell mdl-cell--12-col">
      <!-- markup -->
      <!-- <form id="sign_up" method="POST" action="{{ route('admin.register') }}">
        {{ csrf_field() }}
        <ul class="mdl-stepper mdl-stepper--feedback mdl-stepper--horizontal" id="demo-stepper-horizontal-linear-feedback">
        <li class="mdl-step" data-step-transient-message="Step 1 looks great! Step 2 is coming up.">
          <span class="mdl-step__label">
            <span class="mdl-step__title">
              <span class="mdl-step__title-text">Basic Information</span>
          </span>
          </span>
          <div class="mdl-step__content">
            <div class="form-group">
              <label for="name">Username</label>
              <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="username" required autofocus>
              @if ($errors->has('name'))
                <span class="help-block">
                  <strong>{{ $errors->first('name') }}</strong>
                </span>
              @endif
            </div>
            <div class="form-group">
              <label for="email">Email</label>
              <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email Address" required>
              @if ($errors->has('email'))
              <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
              </span>
              @endif
            </div>
            <div class="form-group">
              <label for="password">Password</label>
              <input id="password" type="password" class="form-control" name="password" minlength="6" placeholder="Password" required>
              @if ($errors->has('password'))
              <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
              </span>
              @endif
            </div>
            <div class="form-group">
              <label for="password-confirm">Re-type Password</label>
              <input id="password-confirm" type="password" class="form-control" name="password_confirmation" minlength="6" placeholder="Confirm Password" required>
            </div>
        </div>
          <div class="mdl-step__actions">
            <button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--colored" data-stepper-next>
          Continue
        </button>
            <button class="mdl-button mdl-js-button mdl-js-ripple-effect" data-stepper-cancel>
          Cancel
        </button> -->
            <!-- <button class="mdl-button mdl-js-button mdl-js-ripple-effect" data-stepper-back>
          Back
        </button> -->

          <!-- </div>
        </li>
        <li class="mdl-step" data-step-transient-message="Step 2 looks great! Step 3 is coming up.">
          <span class="mdl-step__label">
        <span class="mdl-step__title">
          <span class="mdl-step__title-text">Apartment Information</span>
          </span>
          </span>
          <div class="mdl-step__content">
            <div class="form-group">
              <label for="apartment_logo">Logo / Banner</label>
              <input id="apartment_logo" type="text" class="form-control" name="apartment_logo" value="" placeholder="apartment banner/logo" required autofocus>
            </div>
            <div class="form-group">
              <label for="apartment_name">Name</label>
              <input id="apartment_name" type="text" class="form-control" name="apartment_name" value="" placeholder="apartment name" required autofocus>
            </div>
            <div class="form-group">
              <label for="apartment_phone">Contact</label>
              <input id="apartment_phone" type="text" class="form-control" name="apartment_phone" value="" placeholder="apartment contact" required>
            </div>
            <div class="form-group">
              <label for="apartment_address">Address</label>
              <input id="apartment_address" type="text" class="form-control" name="apartment_address" required>
            </div>
          </div>
          <div class="mdl-step__actions">
            <button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--colored" data-stepper-next>
          Continue
        </button>
            <button class="mdl-button mdl-js-button mdl-js-ripple-effect" data-stepper-cancel>
          Cancel
        </button> -->
            <!-- <button class="mdl-button mdl-js-button mdl-js-ripple-effect" data-stepper-back>
          Back
        </button> -->
          <!-- </div>
        </li>
        <li class="mdl-step" data-step-transient-message="Step 3 looks great! Thanks.">
          <span class="mdl-step__label">
        <span class="mdl-step__title">
          <span class="mdl-step__title-text">Subscription Details</span>
          </span>
          </span>
          <div class="mdl-step__content">
            <div class="form-group subscription">
              <div class="tgl-radio-tabs">
                <div>
                  <div class="col-md-4">
                    <input id="monthly" type="radio" class="form-check-input tgl-radio-tab-child" name="subscription" value="month">
                    <label for="monthly" class="radio-inline monthly">
                      Monthly
                    </label>
                    <i class="fas fa-check-circle"></i>
                  </div>
                  <div class="col-md-4">
                    <input id="3_months" type="radio" class="form-check-input tgl-radio-tab-child" name="subscription" value="3_months">
                    <label for="3_months" class="radio-inline">
                      <span>3</span>
                      <br>Months
                    </label>
                    <i class="fas fa-check-circle"></i>
                  </div>
                  <div class="col-md-4">
                    <input id="1_year" type="radio" class="form-check-input tgl-radio-tab-child" name="subscription" value="1_year">
                    <label for="1_year" class="radio-inline">
                      <span>1</span>
                      <br>Year
                    </label>
                    <i class="fas fa-check-circle"></i>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="mdl-step__actions">
            <button type="submit" class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--colored" data-stepper-next>Finish</button>
            <button class="mdl-button mdl-js-button mdl-js-ripple-effect" data-stepper-cancel>
          Cancel
        </button> -->
            <!-- <button class="mdl-button mdl-js-button mdl-js-ripple-effect" data-stepper-back>
          Back
        </button> -->
          <!-- </div>
        </li>
      </ul>
      </form>
    </div>
  </div>

</section> -->
@endsection
