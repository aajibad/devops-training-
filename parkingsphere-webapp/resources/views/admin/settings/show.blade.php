<div class="block-header">
        <h2>Apartment Settings</h2>
    </div>
    
    <!-- Example Tab -->
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-12">
            <div class="card">
                <div class="header">
                    <form action="{{route('admin.settings.permit.add')}}" method="POST">
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-md-9">
                                <div class="form-group" style="margin-bottom:0px;">
                                    <div class="form-line">
                                        <input type="number" class="form-control" name="total_permit" placeholder="No of floor permits" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <input type="hidden" value="floor" name="permit_type">
                                <input type="submit" class="btn btn-primary btn-block btn-lg" value="Add">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="body bg-teal">
                    <h4 style="margin:0px;">Available Floor Permits <button class="btn btn-default pull-right" onclick="showHistory('{{route('admin.settings.permit.history',['permit_type'=>'floor'])}}')">History</button></h4>
                    <p style="margin:2px 0px;" class="font-32">
                        {{is_null($floor_permit)?'Not Set':$floor_permit->available_permit}}
                    </p>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-12">
            <div class="card">
                <div class="header">
                    <form action="{{route('admin.settings.permit.add')}}" method="POST">
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-md-9">
                                <div class="form-group" style="margin-bottom:0px;">
                                    <div class="form-line">
                                        <input type="number" class="form-control" name="total_permit" placeholder="No of building permits" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <input type="hidden" value="building" name="permit_type">
                                <input type="submit" class="btn btn-primary btn-block btn-lg" value="Add">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="body bg-indigo">
                    <h4 style="margin:0px;">Available Building Permits <button class="btn btn-default pull-right" onclick="showHistory('{{route('admin.settings.permit.history',['permit_type'=>'building'])}}')">History</button></h4>
                    <p style="margin:2px 0px;" class="font-32">
                        {{is_null($building_permit)?'Not Set':$building_permit->available_permit}}
                    </p>
                </div>
            </div>
        </div>
        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
            <div class="card">
                {{-- <div class="header">
                    <h2>Apartments</h2>
                </div> --}}
                <div class="body">
                    <form method="post" action="{{route('admin.settings.create')}}">
                            {{ csrf_field() }}
                            
                            {{-- <b>Available parking permits</b>
                            <div class="row clearfix">
                                <div class="col-lg-6 col-md-6 col-sm-8 col-xs-7">
                                    <label for="no_of_floor_permit">Floor</label>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="number" id="no_of_floor_permit" name="no_of_floor_permit" class="form-control" placeholder="Enter no of floor permits" value="{{empty($option->no_of_floor_permit)?'':$option->no_of_floor_permit}}">
                                        </div>
                                    </div>
                                </div>
    
                                <div class="col-lg-6 col-md-6 col-sm-8 col-xs-7">
                                    <label for="no_of_building_permit">Building</label>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="number" id="no_of_building_permit" name="no_of_building_permit" class="form-control" placeholder="Enter no of building permits" value="{{empty($option->no_of_building_permit)?'':$option->no_of_building_permit}}">
                                        </div>
                                    </div>
                                </div>
                            </div> --}}
    
                            <b>Parking permit rates & discounts</b>
                            <div class="row clearfix">
                                <div class="col-lg-6 col-md-6 col-sm-8 col-xs-7">
                                    
                                    <label for="amount_permit_monthly">Monthly</label>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="number" id="amount_permit_monthly" name="amount_permit_monthly" class="form-control" placeholder="Enter monthly rate" value="{{empty($option->amount_permit_monthly)?'':$option->amount_permit_monthly}}"> 
                                        </div>
                                    </div>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="number" id="discount_permit_monthly" name="discount_permit_monthly" class="form-control" placeholder="Enter monthly discount %" value="{{empty($option->discount_permit_monthly)?'':$option->discount_permit_monthly}}"> 
                                        </div>
                                    </div>
                                </div>    
                                <div class="col-lg-6 col-md-6 col-sm-8 col-xs-7">
                                    <label for="amount_permit_yearly">Yearly</label>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="number" id="amount_permit_yearly" name="amount_permit_yearly" class="form-control" placeholder="Enter yearly rate" value="{{empty($option->discount_permit_yearly)?'':$option->discount_permit_yearly}}">
                                        </div>
                                    </div>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="number" id="discount_permit_yearly" name="discount_permit_yearly" class="form-control" placeholder="Enter yearly discount %" value="{{empty($option->discount_permit_yearly)?'':$option->discount_permit_yearly}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
    
                            <b>Visitor's parking permit</b>
                            <div class="row clearfix">
                                <div class="col-lg-6 col-md-6 col-sm-8 col-xs-7">
                                    <label for="max_visitor_permit">Max no of visitor parking permits</label>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="number" id="max_visitor_permit" name="max_visitor_permit" class="form-control" placeholder="Enter permit duration" value="{{empty($option->max_visitor_permit)?'':$option->max_visitor_permit}}">
                                        </div>
                                    </div>
                                </div>    
                                <div class="col-lg-6 col-md-6 col-sm-8 col-xs-7">
                                    <label for="max_visitor_permit_duration">Max duration of visitor parking permits</label>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="number" id="max_visitor_permit_duration" name="max_visitor_permit_duration" class="form-control" placeholder="Enter no of permits" value="{{empty($option->max_visitor_permit_duration)?'':$option->max_visitor_permit_duration}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <p style="font-weight:700;">Apartment Parking Policy</p>
                            
                            <!-- CKEditor -->
                            
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    @if(Session::get('admin_permission')=='read_write')
                                    <textarea id="ckeditor" name="apartment_parking_policy">
                                        {!! empty($option->apartment_parking_policy)?'':$option->apartment_parking_policy !!}
                                    </textarea>
                                    @else
                                        {!! empty($option->apartment_parking_policy)?'No contents here.':$option->apartment_parking_policy !!}
                                    @endif
                                </div>
                            </div>
                            <!-- #END# CKEditor -->
    
                            <button type="submit" class="btn btn-primary m-t-15 btn-lg waves-effect" style="margin-top:0px;">SAVE</button>
                          </form>
                        
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            {{-- <div class="block-header">
                <h2>Subscriptions</h2>
            </div>     --}}
            <div class="card">
                <div class="header">
                    <h2>
                        @if($apartment->subscription_type=='monthly')
                            Monthly
                        @elseif($apartment->subscription_type=='3_months')
                            3 Months
                        @elseif($apartment->subscription_type=='1_year')
                            1 Year
                        @endif
                        Subscription
                    </h2>
                </div>
                <div class="body bg-default">
                    <p style="margin:0px;" class="font-20">
                        ${{number_format($super_admin_option->getRate(),2,'.',',')}} ({{$super_admin_option->getDiscountRate()}}% Discount)
                    </p>
                    <br>
                    <p style="margin:0px;" class="font-24 col-teal">
                        Total Payable ${{number_format($super_admin_option->getAmount(),2,'.',',')}}
                    </p>
                    @if(Session::get('admin_permission')=='read_write')
                        {{-- <a href="{{route('admin.settings.subscription',['type'=>'monthly'])}}" class="btn btn-block waves-effect {{ $apartment->subscription_type=='monthly'?'btn-primary':'btn-default'}} btn-lg">Monthly</a>
                        <a href="{{route('admin.settings.subscription',['type'=>'3_months'])}}" class="btn btn-block waves-effect {{ $apartment->subscription_type=='3_months'?'btn-primary':'btn-default'}} btn-lg">3 Months</a>
                        <a href="{{route('admin.settings.subscription',['type'=>'1_year'])}}" class="btn btn-block waves-effect {{ $apartment->subscription_type=='1_year'?'btn-primary':'btn-default'}} btn-lg">Yearly</a> --}}
                        <a href="{{route('admin.payments')}}" class="btn btn-primary btn-block btn-lg" style="margin-top:20px;">Upgrade Plan</a>    
                    @endif
                </div>
            </div>
    </div>
    
    <!-- Set Parking Permit History -->
    <div class="modal fade" id="setParkingPermitHistory" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="largeModalLabel">Set Parking Permit History</h4>
                </div>
                <div class="modal-body" id="setParkingPermitHistoryContent">
                    <p style="margin:2%; text-align:center; font-weight:400;">Please wait...</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Example Tab -->
    