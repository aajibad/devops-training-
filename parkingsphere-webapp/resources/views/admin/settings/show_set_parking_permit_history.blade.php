<table class="mdl-data-table table-bordered datatable" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>No of permits</th>
                <th>Set date</th>
            </tr>
        </thead>
        <tbody>
        @foreach($histories as $history)
            <tr>
                <td>{{$history->total_permit}}</td>
                <td>{{$history->created_at}}</td>
            </tr>
        @endforeach
        <tbody>
    </table>