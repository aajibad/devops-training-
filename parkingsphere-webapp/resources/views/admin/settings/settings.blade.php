@extends('layouts.core')

@section('content_body')
  @include('admin.settings.show')
@endsection

@section('script')

  @if(Session::get('admin_permission')=='read_only')
  <script type="text/javascript">
    $('input').prop('disabled',true);
  </script>
  @endif

  <script>
  function showHistory(url){
        $modal=$('#setParkingPermitHistory');
        $modal.modal();
        $.get(url,function(html){
            $modal.find('#setParkingPermitHistoryContent').html(html);
            // $modal.find('#showParkingPermitHistoryTable').dataTable();
        });
    }
    </script>

  <!-- Ckeditor -->
  <script src="{{secure_asset('admin/plugins/ckeditor/ckeditor.js')}}"></script>
  <script src="{{secure_asset('admin/js/plugins/tinymce/tinymce.min.js')}}"></script>
  <script src="{{secure_asset('admin/js/pages/forms/editors.js')}}"></script>

@endsection
