@extends('layouts.core')

@section('content_body')
  @include('admin.wallets.create')
  @include('admin.wallets.list')
@endsection

@section('script')

@endsection