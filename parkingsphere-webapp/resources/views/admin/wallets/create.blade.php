@if(Session::get('admin_permission')=='read_write')
    <div class="row clearfix">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Submit your withdrawal request
                    </h2>
                </div>
                <div class="body">
                    <form action="" method="post">
                        <div class="input-group"> 
                            <span class="input-group-addon" id="basic-addon1">$</span>
                            <div class="form-line">
                                <input type="text" name="amount" id="amount" class="form-control input-lg" value="350.00" aria-describedby="basic-addon1">
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-lg">Submit Now</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        
    </div>
@endif