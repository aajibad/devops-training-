<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="header">
        <h2>Transactions</h2>
      </div>
      <table id="example" class="mdl-data-table datatable" cellspacing="0" width="100%">
        <thead>
            <tr>
              <th>User Id</th>
              <th>Username</th>
              <th>Transaction</th>
              <th>Amount</th>
              <th>Service charge</th>
              <th>Receivable</th>
              <th>Paid Date</th>
            </tr>
        </thead>
        <tbody>
            <tr>
              <td>#453234</td>
              <td>John Smith</td>
              <td>New parking permit purchase</td>
              <td>+ $330.00</td>
              <td>- $30.00</td>              
              <td>$300.00</td>
              <td>03.02.2018</td>
            </tr>
        <tbody>
      </table>
    </div>
  </div>
</div>
