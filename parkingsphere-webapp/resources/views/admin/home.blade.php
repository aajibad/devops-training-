@extends('layouts.core')

@section('style')
<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
<style>
    .cover{
        width:100%; 
        height:auto; 
        background-image:url('{{secure_asset('images/admin/payment-cover.svg')}}');
    }
    .mask{
        width:100%; 
        min-height:300px; 
        background:rgba(0,0,0,0.6); 
        padding:45px;
    }
    .mask h3,h4{
        color: white;
        font-family: 'Nunito', sans-serif;
        margin: 0px;
    }
    .modal h4{
        color:grey;
    }
</style>
@endsection

@section('content_body')
{{-- @foreach($statistics as $d)
    {{dd(empty($d->permits))}}
@endforeach --}}
<!-- Hover Expand Effect -->
  <div class="block-header">
      
  </div>

  <?php 
    date_default_timezone_set('America/New_York');
    $time = date("H");
    /* Set the $timezone variable to become the current timezone */
  ?>

<div class="row">
<div id="app-clock">
    <div class="col-md-9">
        @if ($time < "12")
            <h3 class="col-cyan" style="margin:0px; text-transform:capitalize;">
                {{"Good morning !"}} {{Auth::user()->name}}
            </h3>
        @elseif ($time >= "12" && $time < "17")
            <h3 class="col-orange" style="margin:0px; text-transform:capitalize;">
                {{"Good afternoon !"}} {{Auth::user()->name}}
                {{-- <small>Its time now</small>    --}}
            </h3>
        @elseif ($time >= "17" && $time < "19")
            <h3 class="col-teal" style="margin:0px; text-transform:capitalize;">
                {{"Good Evening !"}} {{Auth::user()->name}}
            </h3> 
        @elseif ($time >= "19")
            <h3 class="col-blue-grey" style="margin:0px; text-transform:capitalize;">
                {{"Good Night !"}} {{Auth::user()->name}}
            </h3>
        @endif
        <h6 style="margin-top:3px;" class="col-blue-grey">Welcome to Housing Manager Dashboard ({{Session::get('admin_apartment')->name}})</h6>
    </div>
    <div class="col-md-3">
        <b v-text="currentDate" class="col-teal"></b>
        <h3 v-text="currentTime" style="margin-top:0px;" class="col-blue-grey"></h3>
    </div>
</div>
</div>

  @if(Session::get('admin_apartment')->status=="blocked")
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="alert alert-danger">
            <div class="content">
                <strong>Warning !</strong> cannot access our services any more.
                {{--  <div class="number">10 OF 35</div>  --}}
            </div>
        </div>
    </div>
  </div>
  @endif

  @if($subscription->getStatus()=="required")
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin:25px 0px;">
        <div class="cover">
            <div class="mask">
                <h3>Welcome to ParkingSphere,</h3>
                <h4>
                    Your initial subscription payment required for access our services.  
                    <br>Kindly make your payments.
                    <br>Thankyou.
                </h4>
                <a href="{{route('admin.payments')}}" class="btn btn-primary btn-lg waves-effect" style="margin-top:20px;">Make Payment</a>
            </div>
        </div>
    </div>
  </div>
  @elseif($subscription->getStatus()=="expired")
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin:25px 0px;">
        <div class="cover">
            <div class="mask">
                <h3>Dear customer,</h3>
                <h4>
                    Your subscription has been expired.
                    <br>Please renew your subscription.
                    <br>Thankyou.
                </h4>
                <a href="{{route('admin.payments')}}" class="btn btn-primary btn-lg waves-effect" style="margin-top:20px;">Renew Subscription</a>
                <a href="{{route('admin.payments')}}" class="btn btn-default btn-lg waves-effect" style="margin-top:20px;">Upgrade Plan</a>
            </div>
        </div>
    </div>
  </div>
  @endif

  <div class="row">
    {{-- <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>AVAILABLE VISITOR PARKING PERMITS</h2>
            </div>
            <div class="body">
                <div id="statistic_visitor_parking_permits_donut"></div>
            </div>
        </div>
    </div> --}}
    
    <div id="realtime_analytics">
    
        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
            <div id="statistic_parking_permits_donut" style="width:100%; height:350px;"></div>
        </div>

        <div class="col-md-4">
            <div class="row" id="summaryInfoBox">
                <div class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
                    <div class="info-box-2 bg-pink hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">timelapse</i>
                        </div>
                        <div class="content">
                            <div class="text">PARKING PERMITS</div>
                            <div class="number">
                              {{$floor_permits+$building_permits}} OF 
                              {{$available_floor_permits->available_permit+$available_building_permits->available_permit}}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
                    <div class="info-box-2 bg-indigo hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">drive_eta</i>
                        </div>
                        <div class="content">
                            <div class="text">TOWED VEHICLES</div>
                            <div class="number">{{count($towedVehicles)}}</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
                    <div class="info-box-2 bg-blue hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">attach_money</i>
                        </div>
                        <div class="content">
                            <div class="text">TODAY'S REVENUE</div>
                            <div class="number">${{$todays_revenue}}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="col-md-12" style="margin-top:15px;">
            <div class="btn-group btn-group-lg" style="margin-bottom:15px;" role="group" aria-label="Large button group">
                <button class="analyze btn {{$statistic_date_type=='today'?'btn-default':'btn-primary'}} waves-effect" data-action="today" onclick="getAnalytic('{{route('admin.home',['data'=>'today'])}}')">Today</button>
                <button class="analyze btn {{$statistic_date_type=='week'?'btn-default':'btn-primary'}} waves-effect" data-action="week" onclick="getAnalytic('{{route('admin.home',['data'=>'week'])}}')">Last Week</button>
                <button class="analyze btn {{$statistic_date_type=='last_3_months'?'btn-default':'btn-primary'}} waves-effect" data-action="last_3_months" onclick="getAnalytic('{{route('admin.home',['data'=>'last_3_months'])}}')">Last Month</button>
            </div>
        </div>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="body">
                    <div id="statistic_parking_permits_bar" style="width:100%; height:400px;"></div>
                </div>
            </div>
        </div>

        <script type="text/javascript">

            // Morris.Donut({
            //     element: 'statistic_parking_permits_donut',
            //     data: [
            //         {label: "Total", value: totalPermits},
            //         {label: "Available", value: availablePermits},
            //         {label: "Sold", value: soldPermits}
            //     ],
            //     colors:[
            //         '#4B8F8C','#D80032','#484D6D'
            //     ]
            // });

            // Morris.Area({
            //     element: 'statistic_parking_permits_bar',
            //     data: statistic_data,
            //     xkey: 'y',
            //     ykeys: ['a', 'b' ,'c'],
            //     labels: ['Parking Permit', 'Visitor Parking Permit' , 'Towed Vehicles']
            // });

            
            google.charts.load('current', {'packages':['corechart']});
            google.charts.setOnLoadCallback(drawChart);

            
            setInterval(function(){
                var statistics=[];
                // statistics.push(['Date', 'Parking Permit Revenue','Parking Permit', 'Visitor Parking Permit', 'Towed Vehicles']);
                statistics.push(['Date','Parking Permit', 'Visitor Parking Permit', 'Towed Vehicles']);
                
                activeBtn=$('.analyze.btn.btn-default');
                action=activeBtn.data("action");
                url='{{route("admin.home")}}'+'?data='+action+'&json=true';

                $.get(url,function(res){
                    JSON.parse(res.forLine).forEach(function(value){
                        statistics.push([
                            value.date,
                            // value.parking_permits_revenue,
                            value.parking_permits,
                            value.visitor_parking_permits,
                            value.towed_vehicles
                        ]);
                    });
                    
                    drawChart(statistics);
                });
            }, 2500);

            function drawChart(statisticData) {
                // var data = google.visualization.arrayToDataTable([
                // ['Date', 'Parking Permit Revenue','Parking Permit', 'Visitor Parking Permit', 'Towed Vehicles'],
                // @foreach($statistics as $data)
                //     [
                //         '{{$data["date"]}}',
                //         {{$data["parking_permits_revenue"]}},
                //         {{$data["parking_permits"]}},
                //         {{$data["visitor_parking_permits"]}},
                //         {{$data["towed_vehicles"]}},
                //     ],
                // @endforeach
                // ]);
                console.log(statisticData);
                var data = google.visualization.arrayToDataTable(statisticData);

                var options = {
                    title: 'ParkingSphere Quick Anlaytics',
                    hAxis: {title: 'Date',  titleTextStyle: {color: '#333'},slantedText:true},
                    // curveType: 'function',
                    legend: {position: 'top', maxLines: 3},
                    vAxis: {minValue: 0},
                    animation:{
                        duration:1000,
                        easing:'out'
                    },
                    pointSize:5
                };

                var chart = new google.visualization.LineChart(document.getElementById('statistic_parking_permits_bar'));
                chart.draw(data, options);
            }

            google.charts.setOnLoadCallback(drawPieChart);

            setInterval(function(){
                var statistics=[];
                statistics.push(['Data','Values']);
                
                activeBtn=$('.analyze.btn.btn-default');
                action=activeBtn.data("action");
                url='{{route("admin.home")}}'+'?data='+action+'&json=true';

                $.get(url,function(res){
                    // console.log(res.forPie);
                    res.forPie.forEach(function(value){
                        statistics.push([value.name,value.value]);
                    });
                    
                    drawPieChart(statistics);
                });

                $.get('{{route("admin.home")}}',function(res){
                    // console.log(res.forPie);
                    var elInfoBox=$(res).find("#summaryInfoBox");
                    $("#summaryInfoBox").html(elInfoBox);
                });
            }, 2500);

            function drawPieChart(statisticData) {

                console.log(statisticData);
                var data = google.visualization.arrayToDataTable(statisticData);

                var options = {
                    title: 'Towed Vehicles, Total, Sold & Available Parking Permits',
                    pieHole: 0.4,
                    slices: {
                        0: { color: '#3F51B5' },
                        1: { color: '#E91E63' },
                        2: { color: '#9C27B0' },
                        3: { color: '#009688' },    
                    }
                };

                var chart = new google.visualization.PieChart(document.getElementById('statistic_parking_permits_donut'));
                chart.draw(data, options);
            }
        </script>
    </div>
  </div>
  {{-- <div class="row">
    
  </div> --}}

    {{-- <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="header">
                    <h2>Send push notification</h2>
                </div>
                <div class="body">
                    <form action="" method="POST">
                        <div class="row">
                            <div class="col-md-4">
                                <select class="form-control" multiple>
                                    <option>All tenants</option>
                                    <option>User 1</option>
                                    <option>User 1</option>
                                    <option>User 1</option>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" style="margin-bottom:0px;">
                                    <div class="form-line">
                                        <input type="text" class="form-control" placeholder="Message" autofocus>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <input type="submit" class="btn btn-primary btn-block btn-lg wave-effect" value="Send Now">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
  </div> --}}
  <!-- #END# Hover Expand Effect -->

  {{-- <div class="row clearfix">
    <!-- Pie Chart -->
    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>Parking permits</h2>
            </div>
            <div class="body">
                <div id="pie_chart" class="flot-chart"></div>
            </div>
        </div>
    </div>
    <!-- #END# Pie Chart -->
</div> --}}

  <div class="row" style="margin-top:15px;">
    <div class="col-md-12">
      <div class="card">
        <div class="header">
          <h2>Purchased Parking Permits</h2>
        </div>
        <div class="body">
            <table id="example" class="mdl-data-table datatable" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Avatar</th>
                    <th>Purchased By</th>
                    <th>Parking Spot</th>
                    <th>Payment Term</th>
                    <th>Status</th>
                    <th>Expired At</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($parkingPermits as $p_permit)
                    @if(true)
                    <tr>
                        <td><img style="width:80px; height:80px;" src="{{$p_permit->avatar==null?secure_asset('images/admin/user.jpg'):secure_asset('images/user_avatar'.$p_permit->avatar)}}"></td>
                        <td>{{strtoupper($p_permit->user->name)}}</td>
                        <td>{{strtoupper($p_permit->parking_spot)}}</td>
                        <td>
                            {{$p_permit->payment_term=='monthly'?'MONTHLY':'YEARLY'}}</td>
                        <td>
                            @if($p_permit->status=='active')
                                <span class="label label-success">Active</span>
                            @elseif($p_permit->status=='pending')
                                <span class="label label-info">Pending</span>
                            @else
                                <span class="label label-danger">Expired</span>
                            @endif
                        </td>
                        <td>
                            {{$p_permit->expired_on}}
                        </td>
                        <td>
                            {{-- <button type="button" class="btn btn-primary btn-sm btn-block" data-toggle="modal" data-target="#sendNotification">Notify</button> --}}
                            <button type="button" class="btn btn-block" onclick="showVehicle('{{route('user.vehicle',['id'=>$p_permit->vehicle->id])}}')">Vehicle Info</button>
                        </td>
                    </tr>
                    @endif
                @endforeach
            </tbody>
            </table>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="header">
          <h2>Visitor Parking Permits</h2>
        </div>
        <div class="body">
            <table id="example" class="mdl-data-table datatable" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>Avatar</th>
                    <th>Generated By</th>
                    <th>Parking Spot</th>
                    <th>Status</th>
                    <th>Start From</th>
                    <th>Expired At</th>
                    {{-- <th>Action</th> --}}
                </tr>
                </thead>
                <tbody>
                    @foreach($visitorParkingPermits as $vp_permit)
                        <tr>
                            <td>
                                <img src="{{is_null($vp_permit->avatar)?secure_asset('images/admin/user.jpg'):secure_asset('images/user_avatar'.$vp_permit->avatar)}}" style="width:80px; height:80px;">
                            </td>
                            <td>{{$vp_permit->user->name}}</td>
                            <td>{{$vp_permit->parking_spot}}</td>
                            <td>{{$vp_permit->status}}</td>
                            <td>
                                {{$vp_permit->start_from}}
                            </td>
                            <td>
                                {{$vp_permit->expired_on}}
                            </td>
                            {{-- <td>
                            <button type="button" class="btn btn-block bg-pink waves-effect" data-toggle="modal" data-target="#visitorParkingPermit">More</button>
                            </td> --}}
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
      </div>
    </div>
  </div>
  
    <div class="modal fade" id="sendNotification" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="largeModalLabel">Send Notification</h4>
                </div>
                <div class="modal-body" id="vehicle-info-content">
                    <form class="form-horizontal">
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-5 form-control-label">
                                <label for="password_2">Message</label>
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="password_2" class="form-control" placeholder="Type your message">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-offset-3 col-md-offset-3 col-sm-offset-4 col-xs-offset-5">
                                <button type="button" class="btn btn-primary btn-lg m-t-15 waves-effect">SEND</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- More Info Parking Permit Model -->
    <div class="modal fade" id="parkingPermit" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="largeModalLabel">Vehicle Info</h4>
                </div>
                <div class="modal-body" id="vehicle-info-content">
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                </div>
            </div>
        </div>
    </div>

    <!-- More Info Parking Permit Model -->
    <div class="modal fade" id="visitorParkingPermit" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="largeModalLabel">Visitor Parking Permit</h4>
                </div>
                <div class="modal-body">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin sodales orci ante, sed ornare eros vestibulum ut. Ut accumsan
                    vitae eros sit amet tristique. Nullam scelerisque nunc enim, non dignissim nibh faucibus ullamcorper.
                    Fusce pulvinar libero vel ligula iaculis ullamcorper. Integer dapibus, mi ac tempor varius, purus
                    nibh mattis erat, vitae porta nunc nisi non tellus. Vivamus mollis ante non massa egestas fringilla.
                    Vestibulum egestas consectetur nunc at ultricies. Morbi quis consectetur nunc.
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link waves-effect">SAVE CHANGES</button>
                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        function getAnalytic(url){
            $.get(url,function(res){
                grab_analytics=$(res).find('#realtime_analytics');
                $('#realtime_analytics').html(grab_analytics);
            })
        };

        var statistic_data;

        function showVehicle(url){
            var $modal=$('#parkingPermit');
            $modal.modal("show");
            $.get(url,function(html){
                $modal.find('#vehicle-info-content').html(html);
                $("#visitor_vehicle_lightgallery").lightGallery({
                    thumbnail:true,
                    animateThumb: true,
                    download: false,
                    selector: '.item'
                });
            })
        }

        $("#visitor_vehicle_lightgallery").lightGallery({
            thumbnail:true,
            animateThumb: true,
            download: false,
            selector: '.item'
        }); 

        var app=new Vue({
            el:"#app-clock",
            data:{
                currentTime: null,
                currentDate: null
            },
            methods: {
                updateCurrentTime() {
                    this.currentTime = moment().format('LTS');
                },
                updateCurrentDate() {
                    this.currentDate = moment().format('LL');
                }
            },
            created() {
                moment.locale(); 
                this.currentTime = moment().format('LTS');
                this.currentDate = moment().format('LL');
                setInterval(() => this.updateCurrentTime(), 1 * 1000);
                setInterval(() => this.updateCurrentDate(), 1 * 1000);
            }
        })
    </script>
@endsection
