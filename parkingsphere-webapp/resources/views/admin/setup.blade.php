@extends('layouts.main')

@section('body_class')
    style="background:rgb(23,75,140);"
@endsection

@section('style')
<style>
    #map {
        height: 300px;
        width: 100%;
        margin-bottom:20px;
    }
    #description {
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
    }

    #infowindow-content .title {
        font-weight: bold; 
    }

    #infowindow-content {
        display: none;
    }

    #map #infowindow-content {
        display: inline;
    }

    #title {
        color: #fff;
        background-color: #4d90fe;
        font-size: 25px;
        font-weight: 500;
        padding: 6px 12px;
    }

    .subscription-grp-buttons label{
        padding: 25px 30px;
        max-height:100px;
    }   
    .subscription-grp-buttons label.btn:hover {    			
        background: gainsboro;
    }
    .subscription-grp-buttons label.btn {    			
        background: white;
        color: black;
        margin-left: 0px!important;				
    }
    .subscription-grp-buttons label.btn.active{				
        background: rgb(23,75,140);
        color: white;				
    }
</style>
@endsection

@section('content')
<div class="container-fluid">
    <div class="row clearfix" style="margin:4% 0px 0px 0px;">
        <div class="col-lg-8 col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>PARKINGSPHERE INITIAL SETUP</h2>
                </div>
                <div class="body">
                    @if(Session::has('return') && Session::get('return')=="failed")
                    <div class="alert alert-danger">
                        {!!Session::get('message')!!}
                    </div>
                    @endif
                    <form id="wizard_with_validation" action="{{route('setup.create')}}" method="POST" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <h5>Apartment Information</h5>
                        <fieldset>
                            <div class="form-group">
                                <div class="form-line">
                                    <label for="apartment_logo">Logo / Banner</label>
                                    <input id="apartment_logo" type="file" class="form-control" name="apartment_logo" value="" placeholder="Select your banner/logo" required aria-required="true">
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="form-line">
                                    <label for="apartment_name">Name</label>
                                    <input id="apartment_name" type="text" class="form-control" name="apartment_name" value="" placeholder="Enter your apartment name" required aria-required="true">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-line">
                                    <label for="apartment_phone">Contact</label>
                                    <input id="apartment_phone" type="tel" class="form-control" name="apartment_phone" value="" placeholder="Enter your phone number" required aria-required="true">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-line">
                                    <label for="apartment_address">Address</label>
                                    <input id="pac-input" type="text" class="form-control" name="apartment_address" placeholder="Enter your location" required aria-required="true">
                                </div>
                            </div>
                            <div id="map"></div>
                            <div id="infowindow-content">
                                <img src="" width="16" height="16" id="place-icon">
                                <span id="place-name"  class="title"></span><br>
                                <span id="place-address"></span>
                            </div>

                            <div class="form-group" >
                                <label style="margin-bottom:15px">Choose your subscription plan</label>
                                <br>
                                <div class="btn-group subscription-grp-buttons" data-toggle="buttons">
                                    <label class="btn active">
                                        <input name="subscription" type="radio" id="monthly" value="monthly" checked>
                                        MONTHLY
                                    </label>
                        
                                    <label class="btn">
                                        <input name="subscription" type="radio" id="3_months" value="3_months">
                                        3 MONTHS
                                    </label>
                        
                                    <label class="btn">
                                        <input name="subscription" type="radio" id="yearly" value="1_year">
                                        1 YEAR
                                    </label>
                                </div>

                                {{-- <input name="subscription" type="radio" id="monthly" class="radio-col-indigo" value="monthly" required>
                                <label for="monthly">MONTHLY</label>
                                <input name="subscription" type="radio" id="3_months" class="radio-col-indigo" value="3_months" required>
                                <label for="3_months">3 MONTHS</label>
                                <input name="subscription" type="radio" id="yearly" class="radio-col-indigo" value="1_year" required>
                                <label for="yearly">1 YEAR</label> --}}
                            </div>

                        </fieldset>
                        <h5>Terms &amp; Conditions</h5>
                        <fieldset>
                            <div style="width:100%; max-height: 200px; margin: 10px 0px; font-family: 'ABeeZee', sans-serif; font-size:16px; overflow-y: scroll;">
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
                                when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, 
                                remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                                <br><br>
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
                                when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, 
                                remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                                <br><br>
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
                                when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, 
                                remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                            </div>
                            <input id="acceptTerms-2" name="accept_terms" type="checkbox" required aria-required="true" value="false">
                            <label for="acceptTerms-2">I agree with the Terms and Conditions.</label>
                        </fieldset>
                        <input type="submit" class="btn btn-primary btn-lg" value="CONTINUE" style="margin-top:25px;">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    // This example requires the Places library. Include the libraries=places
    // parameter when you first load the API. For example:
    // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

    function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: -33.8688, lng: 151.2195},
        zoom: 13
        });
        var card = document.getElementById('pac-card');
        var input = document.getElementById('pac-input');
        var types = document.getElementById('type-selector');
        var strictBounds = document.getElementById('strict-bounds-selector');

        map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);

        var autocomplete = new google.maps.places.Autocomplete(input);

        // Bind the map's bounds (viewport) property to the autocomplete object,
        // so that the autocomplete requests use the current map bounds for the
        // bounds option in the request.
        autocomplete.bindTo('bounds', map);

        var infowindow = new google.maps.InfoWindow();
        var infowindowContent = document.getElementById('infowindow-content');
        infowindow.setContent(infowindowContent);
        var marker = new google.maps.Marker({
        map: map,
        anchorPoint: new google.maps.Point(0, -29)
        });

        autocomplete.addListener('place_changed', function() {
        infowindow.close();
        marker.setVisible(false);
        var place = autocomplete.getPlace();
        if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            window.alert("No details available for input: '" + place.name + "'");
            return;
        }

        // If the place has a geometry, then present it on a map.
        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
        } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);  // Why 17? Because it looks good.
        }
        marker.setPosition(place.geometry.location);
        marker.setVisible(true);

        var address = '';
        if (place.address_components) {
            address = [
            (place.address_components[0] && place.address_components[0].short_name || ''),
            (place.address_components[1] && place.address_components[1].short_name || ''),
            (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
        }

        infowindowContent.children['place-icon'].src = place.icon;
        infowindowContent.children['place-name'].textContent = place.name;
        infowindowContent.children['place-address'].textContent = address;
        infowindow.open(map, marker);
        });

        // document.getElementById('use-strict-bounds')
        //     .addEventListener('click', function() {
        //     console.log('Checkbox clicked! New state=' + this.checked);
        //     autocomplete.setOptions({strictBounds: this.checked});
        //     });
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAmaGv748QsR8r6dOIbrW1vgXU1WjfJaKI&libraries=places&callback=initMap" async defer></script>
@endsection