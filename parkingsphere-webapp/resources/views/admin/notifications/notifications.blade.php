@extends('layouts.core')

@section('style')
<style>
  #select-users .btn:hover{
    color:black!important;
  }
</style>
@endsection

@section('content_body')
<div id="app"></div>
  @include('admin.notifications.create')
  @include('admin.notifications.list')
@endsection

@section('script')
  <script>
    var app=new Vue({
      el:'#app',
      data:{
        notification:''
      },
      mounted(){
        this.listen();
      },
      methods:{
        listen(){
          window.Echo.private('notification-user-1-apartment-1')
              .listen('UserNotificationEvent',(notification)=>{
                console.log(notification);
                this.notification=notification;
              })
        }
      }

    });

    $(document).ready(function(){
      checkType();

      $("#user-type").on("change",function(){
        checkType();
      });
    });

    function checkType(){
      var all=$("#notification_type_all");
      var individual=$("#notification_type_individual");

      if(all.prop("checked")){
        $("#select-users").slideUp();
      }
      else{
        $("#select-users").slideDown();
      }
    }
  </script>
@endsection