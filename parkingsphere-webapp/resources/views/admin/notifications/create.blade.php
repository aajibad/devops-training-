<div class="row">
    <div class="col-md-8">
        <div class="card">
            <div class="header">
                <h2>Send Announcement</h2>
            </div>
            <div class="body">
                <form action="{{route('admin.notification.send')}}" class="form-horizontal" method="post">
                    {{csrf_field()}}
                    <div class="row clearfix">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
                            <label for="password_2">Announcement Title</label>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" class="form-control" placeholder="Type your title" name="title">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
                            <label for="password_2">Announcement Body</label>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">                        
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" class="form-control" placeholder="Type your message here.." name="message">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix" id="user-type">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
                            <label for="password_2">User Type</label>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">                        
                            <div class="form-group">
                                <input type="radio" name="notification_type" id="notification_type_all"  class="filled-in radio-col-indigo" value="all" required checked>
                                <label for="notification_type_all" style="margin-top:5px;">All Users</label>
                                
                                <input type="radio" name="notification_type" id="notification_type_individual"  class="filled-in radio-col-indigo" value="individual" required>
                                <label for="notification_type_individual" style="margin-top:5px;">Group / Individual</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-8 col-md-8 col-sm-4 col-xs-4 col-md-offset-4">
                            <div id="select-users" style="width:100%; height:auto;">
                                <select name="selected_user[]" data-selected-text-format="count" data-live-search="true" multiple>
                                    <optgroup data-max-options="25">
                                        <option value="" disabled>No users selected</option>
                                        @foreach($users as $user)
                                            <option value="{{$user->id}}">{{$user->name}}</option>
                                        @endforeach
                                    </optgroup> 
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-offset-4 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">
                            <button type="submit" class="btn btn-primary btn-lg waves-effect">SEND</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>