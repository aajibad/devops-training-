@extends('layouts.core')

@section('style')
  <link href="https://fonts.googleapis.com/css?family=ABeeZee" rel="stylesheet">
@endsection

@section('content_body')
  @include('admin.payments.create')
  @include('admin.payments.list')
@endsection

@section('script')
<script>
  $('.date').bootstrapMaterialDatePicker();
</script>
@endsection