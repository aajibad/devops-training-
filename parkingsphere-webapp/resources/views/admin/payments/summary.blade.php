@extends('layouts.core')

@section('content_body')
<div class="row">
    <div class="col-lg-7 col-md-7 {{$payment_type=="confirm"?'col-md-offset-2':''}}">
        @if($payment_type=="confirm")
            <div class="alert alert-success">
                {!!$message!!}
            </div>
        @endif
        <div class="card">
            <div class="header">
                <h2>
                    @if($payment_type=="confirm")
                        Subscription Plan Confirmation
                        <small>Confirm your subscription and billing agreement.</small>
                    @else
                        Subscription Summary
                        <small>Confirm your subscription summary and make payment.</small>
                    @endif
                </h2>
            </div>
            <div class="body">
                <table class="table table-bordered">
                    <tbody>
                        <tr>
                            <th>Current Subscription</th>
                            <td>
                                {{strtoupper(str_replace('_',' ',$subscription->subscription_type))}}
                            </td>
                        </tr>
                        <tr>
                            <th>Auto Subscription</th>
                            <td>
                                <div class="demo-google-material-icon">
                                @if($auto_subscription)
                                    <i class="material-icons" style="color:green; font-weight:600;">done</i>
                                @else
                                    <i class="material-icons" style="color:red; font-weight:600;">clear</i>
                                @endif
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Billing date</th>
                            <td>{{date('Y M d H:m:s')}}</td>
                        </tr>
                        <tr>
                            <th>Next billing date</th>
                            <td>{{date('Y M d H:m:s',strtotime('+'.$subscription->getSubscriptionPeriod()['interval'].' '.$subscription->getSubscriptionPeriod()['frequency']))}}</td>
                        </tr>
                        <tr>
                            <th>Amount</th>
                            <td class="col-blue">${{number_format($subscription->getRate(),2,'.',',')}}</td>
                        </tr>
                        <tr>
                            <th>Discount</th>
                            <td class="col-pink">
                                - ${{number_format($subscription->getDiscount(),2,'.',',')}} ({{$subscription->getDiscountRate()}}%)
                            </td>
                        </tr>
                        <tr>
                            <th class="font-24">Total Payable</th>
                            <th class="font-24 col-green">${{number_format($subscription->getAmount(),2,'.',',')}}</th>
                        </tr>
                    </tbody>
                </table>
                @if($payment_type=="confirm")
                <form method="POST" action="{{route($route)}}">
                    {{csrf_field()}}
                    <input type="hidden" name="token" value="{{$queries->token}}">
                    <input type="hidden" name="subscription_id" value="{{$subscription->id}}">
                    <input type="hidden" name="auto_subscription" value="{{$auto_subscription!=1?0:1}}">
                    <button type="submit" class="btn btn-primary btn-lg"><b>{{$action}}</b></button>
                    <a href="https://www.paypal.com/webapps/mpp/paypal-popup" title="How PayPal Works" onclick="javascript:window.open('https://www.paypal.com/webapps/mpp/paypal-popup','WIPaypal','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=1060, height=700'); return false;">
                        <img src="https://www.paypalobjects.com/webstatic/mktg/logo/pp_cc_mark_37x23.jpg" style="width:59px; height:auto; float:right;">
                    </a>
                    <a href="https://www.paypal.com/webapps/mpp/paypal-popup" title="How PayPal Works" onclick="javascript:window.open('https://www.paypal.com/webapps/mpp/paypal-popup','WIPaypal','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=1060, height=700'); return false;">
                        <img src="https://www.paypalobjects.com/digitalassets/c/website/marketing/na/us/logo-center/9_bdg_secured_by_pp_2line.png" style="width:115px; margin-right:10px; height:auto; float:right;">
                    </a>
                </form>      
                @endif
                    
                {{-- <button class="btn btn-primary btn-lg btn-block">Confirm & Pay with PayPal</button> --}}
            </div>
        </div>
    </div>
    @if($payment_type!="confirm")
    <div class="col-md-5">
        <div class="card">
            <div class="header">
                <h2 style="text-align:center;">Pay Now with PayPal</h2>
            </div>
            <div class="body">
                {{-- <a href="https://www.paypal.com/webapps/mpp/paypal-popup" title="How PayPal Works" onclick="javascript:window.open('https://www.paypal.com/webapps/mpp/paypal-popup','WIPaypal','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=1060, height=700'); return false;">
                    <img src="https://www.paypalobjects.com/webstatic/mktg/logo/AM_mc_vs_dc_ae.jpg" style="width:100%;" border="0" alt="PayPal Acceptance Mark">
                </a>
                <p class="font-14" style="text-align:center; font-weight:600;">WE ACCEPT CARD PAYMENTS</p> --}}
                <input type="hidden" id="subscription_id" name="subscription_id" value="{{$subscription->id}}">
                <div id="paypal-button" style="width:100%; margin:10px auto;"></div>
            </div>
        </div>
    </div>
    @endif
</div>
@endsection

@if($payment_type!="confirm")
    @section('script')
    <script>
        paypal.Button.render({

            // Set your environment

            env: 'sandbox', // sandbox | production

            // Specify the style of the button
            style: {
                layout: 'vertical',
                size:  'responsive', // small | medium | large | responsive
                shape: 'pill',   // pill | rect
                color: 'gold'   // gold | blue | silver | black
            },

            funding: {
                allowed: [ paypal.FUNDING.CARD, paypal.FUNDING.CREDIT ],
                disallowed: [ ]
            },
            // PayPal Client IDs - replace with your own
            // Create a PayPal app: https://developer.paypal.com/developer/applications/create

            // client: {
            //     sandbox:    'AVZuaKJOhb-oKqG1Fdc29FULSUhJitHyRfe0Rws1yW_nqKSI_mcVGzGpCRQOCTPmGfOY8U6YQNsFPQWe',
            //     production: ''
            // },

            client: {
                sandbox:    'AS1vEl8a_KLs5QvITmjr2TfZczNfPwP5rhPGvxz-uzNiJRwtiFcw1Q6nypm6RYlCPn1RrFvxY6k2ll6z',
                production: ''
            },

            // Wait for the PayPal button to be clicked

            payment: function() {
                var data = {
                    subscription_id: $('#subscription_id').val()
                };

                return paypal.request.post('{{secure_url('/admin/payment/prepare')}}',data).then(function(res) {
                    return res.payment.id;
                });
            },

            // Wait for the payment to be authorized by the customer

            onAuthorize: function(data, actions) {
                var data = {
                    payment_id: data.paymentID,
                    payer_id: data.payerID,
                    subscription_id: $('#subscription_id').val()
                };

                return paypal.request.post('{{secure_url('/admin/payment/proceed')}}',data).then(function(res) {
                    window.location = res.redirect;
                });
            },

            onCancel: function(data, actions) {
                window.location = '{{secure_url('/admin/payment')}}';
            },

            onError: function(err) {
                console.log(err)
                window.location = '{{secure_url('/admin/payment')}}';
            }

        }, '#paypal-button');
    </script>
    @endsection
@endif