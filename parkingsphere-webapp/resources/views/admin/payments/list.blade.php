<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="header">
        <h2>Payment History</h2>
      </div>
      <div class="body">
        <table id="example" class="mdl-data-table datatable" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th>Subscription Term</th>
              <th>Price</th>
              <th>Discount</th>
              <th>Total</th>
              <th>Payment Status</th>
              <th>Payee</th>
              <th>Paid Date</th>
            </tr>
          </thead>
          <tbody>
            @foreach($subscription_history as $history)
              <tr>
                <td>
                  @if($history->subscription_type=='monthly')
                    MONTHLY
                  @elseif($history->subscription_type=='3_months')
                    3 MONTHS
                  @elseif($history->subscription_type=='1_year')
                    1 YEAR
                  @endif
                </td>
                <td>{{$history->amount}}</td>
                <td>{{$history->discount}}</td>
                <td>{{$history->total}}</td>
                <td>
                  @if($history->payment_status=='pending')
                    <span class="label label-info">{{strtoupper($history->payment_status)}}</span>
                  @elseif($history->payment_status=='approved')
                    <span class="label label-success">{{strtoupper($history->payment_status)}}</span>
                  @elseif($history->payment_status=='failed')
                    <span class="label label-danger">{{strtoupper($history->payment_status)}}</span>
                  @endif
                </td>
                <td>{{$history->admin->name}}</td>
                <td>{{$history->created_at}}</td>
              </tr>
            @endforeach
          <tbody>
        </table>
      </div>
    </div>
  </div>
</div>
