<div class="row clearfix">
    <div class="col-md-12">
        @if(Session::get('return')=="success")
            <div class="alert bg-green alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                {!!Session::get('message')!!}
            </div>
        @elseif(Session::get('return')=="warning")
            <div class="alert alert-warning alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                {!!Session::get('message')!!}
            </div>
        @elseif(Session::get('return')=="failed")
            <div class="alert bg-red alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                {!!Session::get('message')!!}
            </div>
        @endif
        <div class="card">
            <div class="header">
                SUBSCRIPTION DETAILS
            </div>
            <div class="body">
                <div class="table-responsive">
                    <form action="{{route('admin.subscription.review')}}" method="">
                        <table class="table table-hover dashboard-task-infos">
                            <thead>
                                <tr>
                                    <th>Subscription</th>
                                    <th>Amount</th>
                                    @if(!$apartment->subscription->auto_subscription)
                                        <th>Status</th>
                                        @if($apartment->subscription->getStatus()!='required')
                                            <th>Remaining days</th>
                                            <th>Expiry date</th>
                                        @endif
                                    @else
                                        <th>Payer Email</th>
                                    @endif
                                    <th>Auto Subscription</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>{{strtoupper(str_replace('_',' ',$apartment->subscription->subscription_type))}}</td>
                                    <td>${{number_format($apartment->subscription->getAmount(),2,'.',',')}}</td>
                                    @if(!$apartment->subscription->auto_subscription)
                                        <td>
                                            @if($apartment->subscription->getStatus()=='active')
                                                <span class="label label-success">ACTIVE</span>
                                            @elseif($apartment->subscription->getStatus()=='expired')
                                                <span class="label label-danger">EXPIRED</span>
                                            @elseif($apartment->subscription->getStatus()=='required')
                                                <span class="label label-warning">REQUIRED</span>
                                            @endif
                                        </td>
                                        @if($apartment->subscription->getStatus()!='required')
                                            <td>{{$apartment->subscription->remainingDays()}}</td>
                                            <td>{{$apartment->subscription->expiry_date}}</td>
                                        @endif
                                    @else
                                        <td>{{$apartment->subscription->payer_email}}</td>
                                    @endif
                                    <td>
                                        <div class="switch">
                                            <label><input type="checkbox" id="auto_subscription" name="auto_subscription" {{$apartment->subscription->auto_subscription?'checked':''}}><span class="lever switch-col-blue-grey"></span>{{$apartment->subscription->auto_subscription?'ON':'OFF'}}</label>
                                        </div>
                                        <div id="cancel-bill-plan" style="display:none">
                                            <button type="button" class="btn btn-danger" onclick="$('#cancel-bill-form').submit();" >Cancel auto subscription</button >
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <input type="hidden" name="subscription_id" value="{{$apartment->subscription->id}}">
                        <input type="hidden" name="payment_type" value="review">
                        <button class="btn bg-indigo btn-lg" onclick="event.preventDefault(); $('#upgrade-plan').slideToggle('slow');">Upgrade</button>
                        @if(!$apartment->subscription->auto_subscription)
                            <input type="submit" class="btn btn-primary btn-lg" value="{{$apartment->subscription->getStatus()=='expired' || $apartment->subscription->getStatus()=='active'?'Renew':'Pay Now'}}">
                        @endif
                    </form> 
                    <form action="{{route('admin.subscription.plan.cancel')}}" id="cancel-bill-form" method="POST">                                                
                        {{csrf_field()}}
                        <input type="hidden" name="subscription_id" value="{{$apartment->subscription->id}}">
                    </form>
                    <div id="upgrade-plan" style="width:100%; height:auto; display:none;">
                        <div class="card" style="margin-top:15px;">
                            <div class="header bg-blue-grey">
                                <h2>Choose your subscription plan</h2>
                                <br>
                                <div class="btn-group btn-group-justified" role="group" aria-label="Justified button group">
                                    <a href="{{route('admin.payment.upgrade',['subscription_id'=>$apartment->subscription->id,'type'=>'monthly'])}}" role="button" class="btn waves-effect {{ $apartment->subscription->subscription_type=='monthly'?'btn-primary':'btn-default'}} btn-lg">Monthly</a>
                                    <a href="{{route('admin.payment.upgrade',['subscription_id'=>$apartment->subscription->id,'type'=>'3_months'])}}" role="button" class="btn waves-effect {{ $apartment->subscription->subscription_type=='3_months'?'btn-primary':'btn-default'}} btn-lg">3 Months</a>
                                    <a href="{{route('admin.payment.upgrade',['subscription_id'=>$apartment->subscription->id,'type'=>'1_year'])}}" role="button" class="btn waves-effect {{ $apartment->subscription->subscription_type=='1_year'?'btn-primary':'btn-default'}} btn-lg">Yearly</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- Billing plan agreement modal --}}
<div class="modal fade" id="agreementModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">AUTO SUBSCRIPTION PLAN AGREEMENT</h4>
            </div>
            <div class="modal-body">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin sodales orci ante, sed ornare eros vestibulum ut. Ut accumsan
                vitae eros sit amet tristique. Nullam scelerisque nunc enim, non dignissim nibh faucibus ullamcorper.
                Fusce pulvinar libero vel ligula iaculis ullamcorper. Integer dapibus, mi ac tempor varius, purus
                nibh mattis erat, vitae porta nunc nisi non tellus. Vivamus mollis ante non massa egestas fringilla.
                Vestibulum egestas consectetur nunc at ultricies. Morbi quis consectetur nunc.
            </div>
            <div class="modal-footer">
                <form action="{{route('admin.subscription.plan.create')}}" method="POST">
                    {{csrf_field()}}
                    <input type="hidden" name="subscription_id" value="{{$apartment->subscription->id}}">
                    <button type="submit" class="btn btn-link waves-effect">{{$apartment->subscription->billing_plan?'I AGREE & ACTIVATE':'I AGREE'}}</button>
                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal" onclick="window.location='{{url('admin/payment')}}'">CANCEL</button>
                </form>
            </div>
        </div>
    </div>
</div>

@section('script')
<script>
    $('#auto_subscription').change(function(){
        if(this.checked){
            $('#agreementModal').modal();
        }else{
            $('#cancel-bill-plan').slideDown();
        }
    });
</script>
@endsection