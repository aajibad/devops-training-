@extends('layouts.core')

@section('content_body')
  <div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="header">
        <h2 class="font-20">List of approvals</h2>
      </div>
      <div class="body">
        <table id="example" class="mdl-data-table datatable" cellspacing="0" width="100%">
          <thead>
              <tr>
                <th>Avatar</th>
                <th>Username</th>
                <th>Email</th>
                <th>Apartment</th>
                <th>Action</th>
              </tr>
          </thead>
          <tbody>
            @foreach($approvals as $user)
              <tr>
                  <td style="width:100px;">
                    <img src="{{ secure_asset(is_null($user->avatar)?'images/admin/user.jpg':$user->avatar) }}" style="width:40%; heigth:40%;" class="img-responsive">
                  </td>
                  <td>{{strtoupper($user->name)}}</td>
                  <td>{{$user->email}}</td>
                  <td>{{$user->apartment->name}}</td>
                  <td>{{$user->created_at}}</td>
                  <td>
                    <form action="{{route('admin.user.approve')}}" method="post">
                        {{csrf_field()}}
                        <input type="hidden" name="id" value="{{$user->id}}">
                        @if($user->status=='pending' || $user->status=='blocked')
                            <input type="submit" class="btn btn-success" value="Activate">
                        @elseif($user->status=='active')
                            <input type="submit" class="btn btn-danger" value="Block">
                        @endif
                    </form>
                  </td>
              </tr>
            @endforeach
          <tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection
