@extends('layouts.core')

@section('style')
<style>
  .btn:hover{
    color: white!important;
  }
  .btn:focus{
    color: white!important;
  }
</style>
@endsection

@section('content_body')
  <div class="row" style="margin-bottom:25px;">
    @if(Session::get('admin_permission')=='read_write')
      <div class="col-md-3">
        <button class="btn bg-blue-grey btn-block btn-lg waves-effect" data-toggle="modal" data-target="#newManagerModal">Add New Manager</button>
      </div>
    @endif
    @if(Session::get('admin_permission')=='read_write')
      <div class="col-md-3">
        <button class="btn bg-teal btn-block btn-lg waves-effect" data-toggle="modal" data-target="#newUserModal">Add New User</button>
      </div>
    @endif
  </div>
  @include('admin.users.create')
  @include('admin.users.list')
@endsection

@section('script')
<script>
  function showUserHistory(url){
    var $modal=$("#showUserHistoryModal");
    $modal.modal("show");
    $.get(url,function(res){
      $modal.find("#showUserHistoryContent").html(res);
      $modal.find(".datatable").dataTable({
        responsive:true       
      });
    })
  }
</script>
@endsection