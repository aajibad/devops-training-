<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="header">
        <h2 class="font-20">Housing Managers</h2>
      </div>
      <div class="body">
        <table id="example" class="mdl-data-table datatable" cellspacing="0" width="100%">
          <thead>
              <tr>
                <th>Avatar</th>
                <th>Username</th>
                <th>Email</th>
                <th>Status</th>
                <th>Apartments</th>
                <th>Registered</th>
                {{-- <th>Action</th> --}}
              </tr>
          </thead>
          <tbody>
            @foreach($apartment->admins as $admin)
              <tr>
                  <td width=130>
                    <img src="{{ secure_asset(is_null($admin->avatar)?'images/admin/user.jpg':$admin->avatar) }}" style="width:40%; heigth:40%;" class="img-responsive">
                  </td>
                  <td>{{$admin->name}}</td>
                  <td>{{$admin->email}}</td>
                  <td>
                      @if($admin->status=='ACTIVE')
                        <span class="label label-success">ACTIVE</span>
                      @elseif($admin->status=='CREATED') 
                        <span class="label label-info">CREATED</span>
                      @else
                        <span class="label label-danger">BLOCKED</span>
                      @endif
                  </td>
                  <td>{{count($admin->apartments)}}</td>
                  <td>{{$admin->created_at}}</td>
                  {{-- <td>
                    @if(Session::get('admin_permission')=='read_write')
                      @if($admin->id!=Auth::user()->id)
                        <div class="btn-group">
                            <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                Change Permission <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                                <li><a href="{{route('admin.user.permission',['id'=>$admin->id,'permission'=>'read_write'])}}" class=" waves-effect waves-block">Read Write</a></li>
                                <li><a href="{{route('admin.user.permission',['id'=>$admin->id,'permission'=>'read_only'])}}" class=" waves-effect waves-block">Read Only</a></li>
                            </ul>
                        </div>  
                      @endif
                    @endif
                  </td> --}}
              </tr>
            @endforeach
          <tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="header">
        <h2 class="font-20"> {{Session::get('admin_apartment')->name}} Users</h2>
      </div>
      <div class="body">
        <table id="example" class="mdl-data-table datatable" cellspacing="0" width="100%">
          <thead>
              <tr>
                <th>Unique ID</th>
                <th>Avatar</th>
                <th>Username</th>
                <th>Email</th>
                <th>Room No</th>
                <th>Phone No</th>
                <th>Status</th>
                <th>Vehicle</th>
                <th>Firstname</th>
                <th>Lastname</th>
                <th>Registered</th>
                <th>Action</th>
              </tr>
          </thead>
          <tbody>
            @foreach($apartment->users as $user)
              <tr>
                  <td>{{$user->unique_id}}</td>
                  <td style="width:50px;">
                    <img src="{{ secure_asset(is_null($user->avatar)?'images/admin/user.jpg':$user->avatar) }}" style="width:40%; heigth:40%;" class="img-responsive">
                  </td>
                  <td>{{$user->name}}</td>
                  <td>{{$user->email}}</td>
                  <td>{{$user->room_no}}</td>
                  <td>{{$user->phone_no}}</td>
                  <td>
                    @if($user->status=='active')
                      <span class="label label-success">ACTIVE</span>
                    @elseif($user->status=='pending')
                      <span class="label label-info">PENDING</span>
                    @else
                      <span class="label label-danger">BLOCKED</span>
                    @endif
                  </td>
                  <td>{{count($user->vehicles)}}</td>
                  <td>{{$user->firstname}}</td>
                  <td>{{$user->lastname}}</td>
                  <td>{{$user->created_at}}</td>
                  <td>
                    {{-- @if(Session::get('admin_permission')=='read_write')
                      <a href="" class="btn btn-danger">Block</a>
                    @endif --}}
                    <button class="btn btn-primary" id="user_history" onclick="showUserHistory('{{route('admin.user.history',['id'=>$user->id])}}')">History</button>
                  </td>
              </tr>
            @endforeach
          <tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<!-- Default Size -->
<div class="modal fade" id="showUserHistoryModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">User History</h4>
            </div>
            <div class="modal-body" id="showUserHistoryContent">
                   
            </div>
        </div>
    </div>
</div>