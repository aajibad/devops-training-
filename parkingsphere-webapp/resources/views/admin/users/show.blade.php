@extends('layouts.core')

@section('content_body')
<div class="block-header">
    <h2>User Profile</h2>
</div>
<div class="row clearfix">
    <!-- Basic Examples -->
    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
        <!-- <div class="block-header">
            <h2>User Informations</h2>
        </div>     -->
        <div class="card">
            <div class="body">
                <div class="row">
                    <div class="col-md-4">
                        <img src="{{secure_asset(is_null($user->avatar)?'images/admin/user.jpg':$user->avatar)}}" class="img-responsive">
                        <button class="btn btn-primary" style="margin-top:15px;" type="button" id="update-profile">Update Profile</button>
                    </div>
                    <div class="col-md-8">
                        <div class="col-md-6">
                            <p class="font-20">
                                {{strtoupper($user->name)}}
                                <br>
                                <small>{{$user->email}}</small>                           
                            </p>
                        </div>
                        <div class="col-md-6">
                            <p class="font-16 col-indigo">
                                No of Apartments : {{count($user->apartments)}}
                            </p>
                        </div>
                            <table class="table">
                                <tr>
                                    <th>Full Name<th>
                                    <td>{{$user->firstname}} {{$user->lastname}}<td>
                                </tr>
                                <tr>
                                    <th>Phone<th>
                                    <td>{{$user->phone}}<td>
                                </tr>
                                <tr>
                                    <th colspan=1>Address<th>
                                    <th><th>
                                </tr>
                                <tr>
                                    <td colspan=3>{{$user->address}}</td>
                                    <td></td>
                                </tr>
                            </table>
                    </div>
                    <div class="col-md-12" id="update-profile-form" style="display:none">
                        <form action="{{route('admin.user.update')}}" method="POST" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div class="col-md-6">
                                <label>Firstname</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" class="form-control" placeholder="Firstname" value="{{$user->firstname}}" name="firstname" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label>Lastname</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" class="form-control" placeholder="Lastname" value="{{$user->lastname}}" name="lastname" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <label>Address</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" class="form-control" placeholder="Address" value="{{$user->address}}" name="address" required>
                                    </div>
                                </div>    
                            </div>
                            <div class="col-md-6">
                                <label>Phone</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="tel" class="form-control" placeholder="Phone" value="{{$user->phone}}" name="phone" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label>Avatar</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="file" class="form-control" name="avatar">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </form>              
                    </div>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="header">
                <h2>
                    ADD NEW APARTMENT
                </h2>
            </div>
            <div class="body">
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <form action="{{route('admin.users.apartment.create')}}" method="post" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <input type="hidden" name="admin" value="{{Auth::user()->id}}">
                            <label for="apartment_logo">Logo / Banner</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="file" id="apartment_logo" name="apartment_logo" class="form-control" required>
                                </div>
                            </div>
                            <label for="apartment_name">Name</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" id="apartment_name" name="apartment_name" class="form-control" placeholder="Apartment name" required>
                                </div>
                            </div>
                            <label for="apartment_phone">Contact</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="number" id="apartment_phone" name="apartment_phone" class="form-control" placeholder="Phone / Mobile" required>
                                </div>
                            </div>
                            <label for="apartment_address">Address</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" id="apartment_address" name="apartment_address" class="form-control" placeholder="Address" required>
                                </div>
                            </div>
                            <label for="subscription">Subscription</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <select class="form-control show-tick" name="subscription" id="subscription" required>
                                        <option value="monthly">Monthly</option>
                                        <option value="3_months">3 Months</option>
                                        <option value="1_year">1 Year</option>
                                    </select>
                                </div>
                            </div>

                            <button type="submit" class="btn btn-primary m-t-15 waves-effect">Save</button>
                        </form>
                    </div>
                </div>            
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        <div class="block-header">
            <h2>Registered Apartments</h2>
        </div>
        <div class="row clearfix">
            @foreach($user->apartments as $apartment)
            <div class="col-xs-12 ol-sm-12 col-md-12 col-lg-12">
                <div class="panel-group" id="accordion_1" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-col-indigo">
                        <div class="panel-heading" role="tab" id="heading_{{$apartment->id}}">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion_1" href="#{{$apartment->id}}" aria-expanded="false" aria-controls="{{$apartment->id}}">
                                    {{$apartment->name}}
                                    @if(Session::get('admin_apartment')->name==$apartment->name)
                                        <span class="label label-info" style="padding: .4em 1em .4em; position:relative; top:5px; border-radius:3px; float:right;">Current</span>
                                    @endif                                     
                                </a>                                
                            </h4>
                        </div>
                        <div id="{{$apartment->id}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_{{$apartment->id}}">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <img src="{{ secure_asset($apartment->logo)}}" style="width:100%;" class="img-resposive">
                                    </div>
                                    <div class="col-md-12">
                                        <h6>{{$apartment->name}}</h6>
                                        </hr>
                                        <div class="col-md-6">
                                            <label>Phone</label>
                                            <p>{{$apartment->phone}}</p>
                                        </div>
                                        <div class="col-md-6">
                                            <label>No of users</label>
                                            <p>{{count($apartment->users)}}</p>
                                        </div>
                                        <div class="col-md-12">
                                            <label>Address</label>
                                            <p>{{$apartment->address}}</p>
                                        </div>   
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>    
</div>
@endsection

@section('script')
<script>
    $("#update-profile").on('click',function(){
        $("#update-profile-form").slideToggle();
    })
</script>
@endsection