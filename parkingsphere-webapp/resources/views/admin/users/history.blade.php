<div class="row">
    <div class="col-md-12">
        <h5>Parking Permits</h5>
        <table class="table table-bordered datatable" style="width:100%">
            <thead>
                <tr>
                    <th>Purchased</th>
                    <th>Parking Spot</th>
                    <th>Reference</th>
                    <th>Payment Term</th>
                    <th>Payment Amount</th>
                    <th>Expired On</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                @foreach($permits as $permit)
                <tr>
                    <td>{{$permit->created_at}}</td>
                    <td>{{$permit->parking_spot}}</td>
                    <td>{{$permit->parking_spot_meta}}</td>
                    <td>{{$permit->payment_term}}</td>
                    <td>{{$permit->payment_amount}}</td>
                    <td>{{$permit->expired_on}}</td>
                    <td>{{$permit->status}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <h5>Generated Permits</h5>
        <table class="table table-bordered datatable">
            <thead>
                <tr>
                    <th>Generated</th>
                    <th>Phone</th>
                    <th>Parking Spot</th>
                    <th>Reference</th>
                    <th>Start From</th>
                    <th>Expired On</th>
                    <th>Ticket Token</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                @foreach($v_permits as $permit)
                <tr>
                    <td>{{$permit->created_at}}</td>
                    <td>{{$permit->phone}}</td>
                    <td>{{$permit->parking_spot}}</td>
                    <td>{{$permit->parking_spot_meta}}</td>
                    <td>{{$permit->start_from}}</td>
                    <td>{{$permit->expired_on}}</td>
                    <td>{{$permit->ticket_token}}</td>
                    <td>{{$permit->status}}</td>
                </tr>
                @endforeach 
            </tbody>
        </table>
    </div>
</div>
