<div class="card">
    <div class="header">
        <h2>LIST OF TOWED VEHICLES</h2>
        </div>
    <div class="body">
        <table id="example" class="mdl-data-table datatable" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th>Company Name</th>
                <th>Model</th>
                <th>License No</th>
                <th>Color</th>
                <th>Company Address</th>
                <th>Company Phone</th>
                <th>Owner</th>
                <th>Owner Info</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($towed_vehicles as $vehicle)
                <tr>
                    <td>{{$vehicle->towingCompany->name}}</td>
                    <td>{{$vehicle->vehicle_model}}</td>
                    <td>{{$vehicle->vehicle_license_no}}</td>
                    <td>{{$vehicle->vehicle_color}}</td>
                    <td>{{$vehicle->towingCompany->address}}</td>
                    <td>{{$vehicle->towingCompany->phone}}</td>
                    <td>
                        @if($vehicle->user_type=='user')
                            USER
                        @elseif($vehicle->user_type=='visitor')
                            VISITOR
                        @endif
                    </td>
                    <td>
                        @if($vehicle->user_type=='user')
                            {{$vehicle->user->name}}
                        @elseif($vehicle->user_type=='visitor')
                            No info
                        @endif
                    </td>
                    <td>
                        <button class="btn btn-primmary btn-sm" onclick="getImages('{{route('user.vehicle',['id'=>$vehicle->vehicle_id])}}')">Images</button>
                    </td>
                </tr>
            @endforeach
            <tbody>
        </table>
    </div>
</div>

<!-- More Info Parking Permit Model -->
<div>
    <div class="modal fade" id="moreInfo" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                <h4 class="modal-title" id="largeModalLabel">More Info</h4>
            </div>
            <div class="modal-body" id="more-info-content">
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>