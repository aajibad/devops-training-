<div class="card">
    <div class="header">
        <h2>LIST OF TOWING COMPANIES</h2>
    </div>
    <div class="body">
        <table id="example" class="mdl-data-table datatable" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th>Name</th>
                <th>Address</th>
                <th>Phone</th>
                <th>Contract</th>
                <th>Created At</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($towing_companies as $company)
                <tr>
                    <td>{{$company->name}}</td>
                    <td>{{$company->address}}</td>
                    <td>{{$company->phone}}</td>
                    <td><a href="{{secure_asset($company->contract)}}" class="btn btn-primary" download>DOWNLOAD</td>
                    <td>{{$company->created_at}}</td>
                    <td>
                        <a href="{{route('admin.towing.company.delete',['id'=>$company->id])}}" class="btn btn-danger">Delete</a>
                    </td>
                </tr>
            @endforeach
            <tbody>
        </table>
    </div>
</div>