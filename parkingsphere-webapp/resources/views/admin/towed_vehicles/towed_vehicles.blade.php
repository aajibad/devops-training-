@extends('layouts.core')

@section('content_body')
@if(Session::get('return')=="success")
    <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
        {!!Session::get("message")!!}
    </div>
@elseif(Session::get('return')=="failed")
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
        {!!Session::get("message")!!}
    </div>
@endif
<div class="row">
    <div class="col-md-8">
        @include('admin.towed_vehicles.create')
    </div>
    <div class="col-md-12">
        @include('admin.towed_vehicles.list_companies')
    </div>
</div>
<div class="row" id="app">
    <div class="col-md-12">
        @include('admin.towed_vehicles.list_vehicles')
    </div>
</div>
@endsection

@section('script')
<script>
    function getImages(url){
        $('#moreInfo').modal();
        $.get(url,function(html){  
            $('#more-info-content').html($(html).find('#carousel-example-generic'));
        });
    }
</script>    
@endsection