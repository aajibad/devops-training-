<div class="card">
    <div class="header">
        <h2>
            ADD TOWING COMPANY
        </h2>
    </div>
    <div class="body">
        <form class="form-horizontal" method="POST" action="{{route('admin.towing.company.create')}}" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="row clearfix">
                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-5 form-control-label">
                    <label for="name">Company Name</label>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
                    <div class="form-group">
                        <div class="form-line">
                            <input type="text" id="name" name="name" class="form-control" placeholder="Company name" required>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-5 form-control-label">
                    <label for="address">Company Address</label>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
                    <div class="form-group">
                        <div class="form-line">
                            <input type="text" id="address" name="address" class="form-control" placeholder="Company address" required>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-5 form-control-label">
                    <label for="phone">Phone</label>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
                    <div class="form-group">
                        <div class="form-line">
                            <input type="tel" name="phone" id="phone" class="form-control" placeholder="Company contact" required>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-5 form-control-label">
                    <label for="contract">Upload Contract</label>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
                    <div class="form-group">
                        <div class="form-line">
                            <input type="file" name="contract" id="contract" class="form-control" placeholder="Company contract" required>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-lg-offset-3 col-md-offset-3 col-sm-offset-4 col-xs-offset-5">
                    <button type="submit" class="btn btn-primary btn-lg m-t-15 waves-effect">SAVE</button>
                </div>
            </div>
        </form>
    </div>
</div>