@if(Session::get('admin_permission')=='read_write')
  <!-- Vertical Layout -->
  <!-- #END# Vertical Layout -->
  <div class="row clearfix">
      <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
          <div class="card">
              <div class="header">
                  <h2>
                      ADD NEW CARD
                  </h2>
              </div>
              <div class="body">
                  <div class="creditCardForm">
                    <div class="payment">
                        <form method="post" action="{{route('admin.card.create')}}">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group owner">
                                        <label for="owner">Owner</label>
                                        <div class="form-line">
                                            <input type="text" class="form-control" id="owner" placeholder="Card Holder" name="card_holder">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group CVV">
                                        <label for="cvv">CVV</label>
                                        <div class="form-line">
                                            <input type="text" class="form-control" id="cvv" placeholder="CVV/CVC" name="card_cvv">
                                        </div>
                                    </div>  
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group" id="card-number-field">
                                        <label for="cardNumber">Card Number</label>
                                        <div class="form-line">
                                            <input type="text" class="form-control" id="cardNumber" placeholder="16 Digit credit/debit card number" name="card_number">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <label>Expiration Date</label>
                            <div class="form-group" id="expiration-date" style="margin-bottom:10px;">
                                <div class="row">
                                    <div class="col-md-7">
                                        <select class="form-control show-tick" name="expiry_month">
                                            <option value="01">January</option>
                                            <option value="02">February </option>
                                            <option value="03">March</option>
                                            <option value="04">April</option>
                                            <option value="05">May</option>
                                            <option value="06">June</option>
                                            <option value="07">July</option>
                                            <option value="08">August</option>
                                            <option value="09">September</option>
                                            <option value="10">October</option>
                                            <option value="11">November</option>
                                            <option value="12">December</option>
                                        </select>
                                    </div>
                                    <div class="col-md-5">
                                        <select class="form-control show-tick" name="expiry_year">
                                            <option value="16"> 2016</option>
                                            <option value="17"> 2017</option>
                                            <option value="18"> 2018</option>
                                            <option value="19"> 2019</option>
                                            <option value="20"> 2020</option>
                                            <option value="21"> 2021</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group" id="credit_cards">
                                <input type="hidden" name="card_type" id="card_type">
                                <img src="{{secure_asset('plugins/payform/images/visa.jpg')}}" id="visa">
                                <img src="{{secure_asset('plugins/payform/images/mastercard.jpg')}}" id="mastercard">
                                <img src="{{secure_asset('plugins/payform/images/amex.jpg')}}" id="amex">
                            </div>
                            <div class="form-group" id="pay-now">
                                <button type="submit" class="btn btn-primary btn-block wave-effect btn-lg" id="confirm-purchase">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
              </div>
          </div>
      </div>
  </div>
@endif