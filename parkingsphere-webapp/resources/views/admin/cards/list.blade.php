<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="header">
        <h2>Cards</h2>
      </div>
      <div class="body">
        <table id="example" class="mdl-data-table datatable" cellspacing="0" width="100%">
          <thead>
              <tr>
                <th>Card Holder</th>
                <th>Card Type</th>
                <th>Number</th>
                <th>Expiry</th>
                <th>Auto payment</th>
                <th>Action</th>
              </tr>
          </thead>
          <tbody>
            @foreach($cards as $card)
              <tr>
                  <td>{{$card->card_holder}}</td>
                  <td>
                    @if($card->card_type=='master_card')
                      <img src="{{secure_asset('plugins/payform/images/mastercard.jpg')}}">
                    @elseif($card->card_type=='amex')
                      <img src="{{secure_asset('plugins/payform/images/amex.jpg')}}">
                    @elseif($card->card_type=='visa')
                      <img src="{{secure_asset('plugins/payform/images/visa.jpg')}}">
                    @endif
                  </td>
                  <td>{{$card->numberMask($card->card_number)}}</td>
                  <td>{{$card->expiry}}</td>
                  <td>
                    <form action="{{route('admin.card.update')}}" method="POST" id="switchAutoPayment">
                      {{csrf_field()}}
                      <div class="switch">
                        <label>
                          <input type="hidden" value="{{$card->id}}" name="id">
                          <input type="checkbox" {{$card->auto_payment==1?'checked':''}} onchange="$(this).closest('form').submit();" name="auto_payment">
                          <span class="lever"></span>
                        </label>
                      </div>
                    </form>
                  </td>
                  <td>
                    <form action="{{route('admin.card.delete')}}" method="post">
                      {{csrf_field()}}
                      <input type="hidden" value="{{$card->id}}" name="id">
                      <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                  </td>
              </tr>
            @endforeach
          <tbody>
        </table>
      </div>
    </div>
  </div>
</div>
