@extends('layouts.core')

@section('style')
<style>
  .transparent {
    opacity: 0.2;
  }
  .creditCardForm #credit_cards {
    width: 235px;
    margin: 10px auto 20px auto;
  }
</style>
@endsection

@section('content_body')
  @include('admin.cards.create')
  @include('admin.cards.list')
@endsection

@section('script')
<script>
  $('.date').bootstrapMaterialDatePicker();

  var owner = $('#owner'),
    cardNumber = $('#cardNumber'),
    cardNumberField = $('#card-number-field .form-line'),
    CVV = $("#cvv"),
    cardType=$("#card_type"),
    mastercard = $("#mastercard"),
    confirmButton = $('#confirm-purchase'),
    visa = $("#visa"),
    amex = $("#amex");

  cardNumber.payform('formatCardNumber');
  CVV.payform('formatCardCVC');

  cardNumber.keyup(function() {
    amex.removeClass('transparent');
    visa.removeClass('transparent');
    mastercard.removeClass('transparent');

    if ($.payform.validateCardNumber(cardNumber.val()) == false) {
        cardNumberField.removeClass('focused success');
        cardNumberField.addClass('focused error');
    } else {
        cardNumberField.removeClass('focused error');
        cardNumberField.addClass('focused success');
    }

    if ($.payform.parseCardType(cardNumber.val()) == 'visa') {
        mastercard.addClass('transparent');
        amex.addClass('transparent');
        cardType.val($.payform.parseCardType(cardNumber.val()));
    } else if ($.payform.parseCardType(cardNumber.val()) == 'amex') {
        mastercard.addClass('transparent');
        visa.addClass('transparent');
        cardType.val($.payform.parseCardType(cardNumber.val()));
    } else if ($.payform.parseCardType(cardNumber.val()) == 'mastercard') {
        amex.addClass('transparent');
        visa.addClass('transparent');
        cardType.val($.payform.parseCardType(cardNumber.val()));
    }
  });
</script>
@endsection