<div class="block-header" id="head">
    <h3 class="col-blue-grey">
        Towed Vehicles
        <small>{{$date_from}} - {{$date_to}}</small>
    </h3>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="body">
                <div id="vehicle_area_chart" style="width: 100%; height: 400px;"></div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="card">
            <div class="body">
                <div id="user_bar_chart" style="width: 100%; min-height: 350px;"></div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="card">
            <div class="body">
                <div id="visitor_bar_chart" style="width: 100%; min-height: 350px;"></div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    google.charts.load('current', {'packages':['corechart']});
    google.charts.load('current', {'packages':['bar']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
        var data = google.visualization.arrayToDataTable(
            [
                ['Date', 'No of towed vehiles'],
            @foreach($data["vehicle"] as $vehicle)
                ['{{date("Y-m-d",strtotime($vehicle->date))}}',  {{$vehicle->vehicle}} ],
            @endforeach
            ]
        );

        var options = {
        title: 'No of towed vehicles',
        legend: {position: 'top', maxLines: 3},
        hAxis: {title: 'Date',  titleTextStyle: {color: '#333'}},
        vAxis: {minValue: 0}
        };

        var chart = new google.visualization.AreaChart(document.getElementById('vehicle_area_chart'));
        chart.draw(data, options);
    }

    google.charts.setOnLoadCallback(drawUserBarChart);

    function drawUserBarChart() {
        var data = google.visualization.arrayToDataTable([
            ['Date', 'No of user\'s vehicles'],
            @foreach($data['user_vehicle'] as $vehicle)
                ['{{date("Y-m-d",strtotime($vehicle->date))}}',  {{$vehicle->vehicle}} ],
            @endforeach
        ]);

        var options = {
            chart: {
                title: 'No of user\'s towed vehicles',
                subtitle: 'No of user\'s towed vehicles from {{$date_from}} to {{$date_to}}',
            },
            bars: 'horizontal'
        };

        var chart = new google.charts.Bar(document.getElementById('user_bar_chart'));

        chart.draw(data, google.charts.Bar.convertOptions(options));
    }

    google.charts.setOnLoadCallback(drawVisitorBarChart);

    function drawVisitorBarChart() {
        var data = google.visualization.arrayToDataTable([
            ['Date', 'No of visitor\'s vehicles'],
            @foreach($data['visitor_vehicle'] as $vehicle)
                ['{{date("Y-m-d",strtotime($vehicle->date))}}',  {{$vehicle->vehicle}} ],
            @endforeach
        ]);

        var options = {
            chart: {
                title: 'No of visitor\'s towed vehicles',
                subtitle: 'No of visitor\'s towed vehicles from {{$date_from}} to {{$date_to}}',
            },
            bars: 'horizontal'
        };

        var chart = new google.charts.Bar(document.getElementById('visitor_bar_chart'));

        chart.draw(data, google.charts.Bar.convertOptions(options));
    }

</script>

<div class="row">
    
</div>

<div class="card" id="content">
    <div class="header">
        {{strtoupper(str_replace('_',' ',$table))}}
    </div>
    <div class="body">
        <div class="">
        <table id="example" class="mdl-data-table datatable table-hover display" style="width:100%;">
            <thead>
                <tr>
                    <th>User ID</th>
                    <th>Name</th>
                    <th>Company Name</th>
                    <th>Company Contact</th>
                    {{-- <th>Status</th> --}}
                    <th>Vehicle Model</th>
                    <th>License No</th>
                    <th>Vehicle Color</th>
                </tr>
            </thead>
            <tbody>
                @foreach($query as $record)
                    <tr>
                        <td>{{$record->user->id}}</td>
                        <td>{{$record->user->name}}</td>
                        <td>{{$record->towingCompany->name}}</td>
                        <td>{{$record->towingCompany->phone}}</td>
                        {{-- <td> 
                            @if($record->status=="active")
                                <span class="label label-success">ACTIVE</span>
                            @elseif($record->status=="expired")
                                <span class="label label-danger">EXPIRED</span>
                            @elseif($record->status=="pending")
                                <span class="label label-info">PENDING</span>
                            @endif
                        </td> --}}
                        <td>{{$record->vehicle->model}}</td>
                        <td>{{$record->vehicle->license_no}}</td>
                        <td>{{$record->vehicle->color}}</td>
                    </tr>
                    {{-- <tr>
                        <td colspan="2">
                            <p>Vehicle Model</p>
                            <p>License No</p>
                            <p>Vehicle Color</p>
                        </td>
                        <td colspan="3">
                            <p style="text-align:left;">{{$record->vehicle->model}}</p>
                            <p style="text-align:left;">{{$record->vehicle->license_no}}</p>
                            <p style="text-align:left;">{{$record->vehicle->color}}</p>
                        </td>
                    </tr> --}}
                @endforeach
            </tbody>
        </table>
        </div>
    </div>
</div>