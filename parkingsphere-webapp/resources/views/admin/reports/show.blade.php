{{-- <button class="btn btn-default" onclick="exportPDF()">Export to PDF</button> --}}
<div style="height:60px; width:100%;" id="scrollTo"></div>
@if($table=="parking_permits")
    @include('admin.reports.parking_permit_report')
@elseif($table=='visitor_parking_permits')
    @include('admin.reports.visitor_parking_permit_report')
@elseif($table=='towed_vehicles')
    @include('admin.reports.towed_vehicle_report')
@endif
