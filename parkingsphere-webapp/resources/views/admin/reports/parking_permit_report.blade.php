<div class="block-header" id="head">
    <h3 class="col-blue-grey">
        Parking Permit Payments Report
        <small>{{$date_from}} - {{$date_to}}</small>
    </h3>
</div>
<div class="row">
    @if($date_range!="today")
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="info-box-2 bg-blue-grey">
            <div class="icon">
                <i class="material-icons">storage</i>
            </div>
            <div class="content">
                <div class="text">PURCHASED PERMITS</div>
                <div class="number count-to" data-from="0" data-to="125" data-speed="1000" data-fresh-interval="20">
                    <?php 
                        $totalPermit=0;
                        foreach($data["permit"] as $permit){
                            $totalPermit+=$permit->permit;
                        }
                    ?>
                    {{$totalPermit}}
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="info-box-2 bg-indigo">
            <div class="icon">
                <i class="material-icons">attach_money</i>
            </div>
            <div class="content">
                <div class="text">TOTAL REVENUE</div>
                <div class="number count-to" data-from="0" data-to="257" data-speed="1000" data-fresh-interval="20">
                    <?php 
                        $totalRevenue=0;
                        foreach($data["revenue"] as $revenue){
                            $totalRevenue+=$revenue->revenue;
                        }
                    ?>
                    ${{$totalRevenue}}
                </div>
            </div>
        </div>
    </div>
    @else
    <div class="col-lg-7 col-md-7 col-sm-6 col-xs-12">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="body">
                        <div class="row">
                            <div class="col-md-12">
                                <div id="donut_chart_material" style="width: 100%; min-height:300px"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php 
            $floor_permit_total=0;
            $building_permit_total=0;
            $available_floor=$data["total_floor_permit"];
            $available_building=$data["total_building_permit"];

            foreach($data["floor_permit"] as $floor){
                $floor_permit_total+=$floor->permit;
            }

            foreach($data["building_permit"] as $building){
                $building_permit_total+=$building->permit;
            }
        ?>
        <script>
            google.charts.load('current', {'packages':['corechart']});
            google.charts.setOnLoadCallback(drawDonutChart);
            // google.charts.setOnLoadCallback(drawPieChart);

            // function drawPieChart() {
            //     // Create the data table.
            //     var data = new google.visualization.DataTable();
            //     data.addColumn('string', 'Type');
            //     data.addColumn('number', 'Permits');
            //     data.addRows([
            //         ['Floor',10],
            //         ['Building', 15],
            //     ]);

            //     // Set chart options
            //     var options = {
            //         title:'Parking Permits',
            //         is3D: true,
            //     };

            //     // Instantiate and draw our chart, passing in some options.
            //     var chart = new google.visualization.PieChart(document.getElementById('pie_chart_material'));
            //     chart.draw(data, options);
            // }

            function drawDonutChart() {
                // Create the data table.
                var data = new google.visualization.DataTable();
                data.addColumn('string', 'Type');
                data.addColumn('number', 'Permits');
                data.addRows([
                    ['Available',{{$available_floor+$available_building}}],
                    ['Floor',{{$building_permit_total}}],
                    ['Building', {{$floor_permit_total}}],
                ]);

                // Set chart options
                var options = {
                    title:'Sold & Available Parking Permits',
                    pieHole: 0.4,
                };

                // Instantiate and draw our chart, passing in some options.
                var chart = new google.visualization.PieChart(document.getElementById('donut_chart_material'));
                chart.draw(data, options);
            }
        </script>
    </div>
    <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12">
        <div class="info-box-2 bg-blue-grey">
            <div class="icon">
                <i class="material-icons">storage</i>
            </div>
            <div class="content">
                <div class="text">PURCHASED PERMITS</div>
                <div class="number count-to" data-from="0" data-to="125" data-speed="1000" data-fresh-interval="20">
                    <?php 
                        $totalPermit=0;
                        foreach($data["permit"] as $permit){
                            $totalPermit+=$permit->permit;
                        }
                    ?>
                    {{$totalPermit}}
                </div>
            </div>
        </div>
        <div class="info-box-2 bg-indigo">
            <div class="icon">
                <i class="material-icons">attach_money</i>
            </div>
            <div class="content">
                <div class="text">TOTAL REVENUE</div>
                <div class="number count-to" data-from="0" data-to="257" data-speed="1000" data-fresh-interval="20">
                    <?php 
                        $totalRevenue=0;
                        foreach($data["revenue"] as $revenue){
                            $totalRevenue+=$revenue->revenue;
                        }
                    ?>
                    ${{$totalRevenue}}
                </div>
            </div>
        </div>
    </div>
    @endif
    {{-- <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="info-box-2 bg-default">
            <div class="icon">
                <i class="material-icons">bookmark</i>
            </div>
            <div class="content">
                <div class="text">VEHICLE REGISTERD</div>
                <div class="number count-to" data-from="0" data-to="117" data-speed="1000" data-fresh-interval="20">117</div>
            </div>
        </div>
    </div> --}}
</div>

@if($date_range!="today")
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="body">
                <div id="chart_div" style="width:100%; height:350px;"></div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="body">
                <div id="columnchart_material" style="width: 900px; height: 500px;"></div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    google.charts.load('current', {'packages':['corechart']});
    google.charts.load('current', {'packages':['bar']});
    
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
        var data = google.visualization.arrayToDataTable(
            [
                ['Date', 'Total Revenue'],
                @foreach($data["revenue"] as $revenue)
                    [
                        '{{$revenue->date}}',
                        {{$revenue->revenue}}
                    ],
                @endforeach
            ]
        );

        var options = {
        title: 'Parking Permits Revenue {{$date_from}} - {{$date_to}}',
        legend: {position: 'top', maxLines: 3},
        hAxis: {title: 'Date',  titleTextStyle: {color: '#333'}},
        vAxis: {minValue: 0}
        };

        var chart = new google.visualization.AreaChart(document.getElementById('chart_div'));
        chart.draw(data, options);
    }

    google.charts.setOnLoadCallback(drawBarChart);

    function drawBarChart() {
        var data = google.visualization.arrayToDataTable([
            ['Date', 'Total Permit'],
            @foreach($data["permit"] as $permit)
                [
                    '{{$permit->date}}',
                    {{$permit->permit}}
                ],
            @endforeach
        ]);

        var options = {
            chart: {
                title: 'Purchased Parking Permits',
                subtitle: 'Parking Permits purchased from {{$date_from}} to {{$date_to}}',
            },
            bars: 'horizontal'
        };

        var chart = new google.charts.Bar(document.getElementById('columnchart_material'));

        chart.draw(data, google.charts.Bar.convertOptions(options));
    }
</script>
@endif
</div>

<div class="card" id="content">
    <div class="header">
        {{strtoupper(str_replace('_',' ',$table))}}
    </div>
    <div class="body">
        <div class="">
        <table id="example" class="mdl-data-table datatable table-hover display" style="width:100%;">
            <thead>
                <tr>
                    <th>User</th>
                    <th>Parking Spot</th>
                    <th>Payment</th>
                    <th>Expired On</th>
                    <th>Status</th>
                    <th>Purchase Date</th>
                    <th>Renewed</th>
                    <th>Vehicle Model</th>
                    <th>License No</th>
                    <th>Vehicle Color</th>
                </tr>
            </thead>
            <tbody>
                @foreach($query as $record)
                    <tr>
                        <td>{{strtoupper($record->user->name)}}</td>
                        <td>{{strtoupper($record->parking_spot)}}</td>
                        <td>
                            @if($record->payment_term=="monthly")
                                MONTHLY
                            @elseif($record->payment_term="yearly")
                                YEARLY
                            @endif
                            <br>${{$record->payment_amount}}
                        </td>
                        <td>{{$record->expired_on}}</td>
                        <td>
                            @if($record->status=="active")
                                <span class="label label-success">ACTIVE</span>
                            @elseif($record->status=="expired")
                                <span class="label label-danger">EXPIRED</span>
                            @elseif($record->status=="pending")
                                <span class="label label-info">PENDING</span>
                            @endif
                        </td>
                        <td>{{date('Y-m-d',strtotime($record->created_at))}}</td>
                        <td>{{date('Y-m-d',strtotime($record->updated_at))}}</td>
                        <td>{{$record->vehicle->model}}</td>
                        <td>{{$record->vehicle->license_no}}</td>
                        <td>{{$record->vehicle->color}}</td>
                    </tr>
                    {{-- <tr style="background:#EDE7F6;">
                        <th colspan="2">
                            <p style="font-weight:600;">Vehicle Model</p>
                            <p style="font-weight:600;">License No</p>
                            <p style="font-weight:600;">Color</p>
                        </th>
                        <td colspan="2">
                            <p style="text-align:left">{{$record->vehicle->model}}</p>
                            <p style="text-align:left">{{$record->vehicle->license_no}}</p>
                            <p style="text-align:left">{{$record->vehicle->color}}</p>
                        </td>
                        <td colspan="4"></td>
                    </tr> --}}
                @endforeach
            </tbody>
        </table>
        </div>
    </div>
</div>
