<div class="block-header" id="head">
    <h3 class="col-blue-grey">
        Visitor Permit History Report
        <small>{{$date_from}} - {{$date_to}}</small>
    </h3>
</div>

<div class="row">
    @if($date_range!="today")
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="info-box-2 bg-blue-grey">
            <div class="icon">
                <i class="material-icons">storage</i>
            </div>
            <div class="content">
                <div class="text">GENERATED PERMITS</div>
                <div class="number count-to" data-from="0" data-to="125" data-speed="1000" data-fresh-interval="20">
                    <?php 
                        $totalPermit=0;
                        foreach($data["permit"] as $permit){
                            $totalPermit+=$permit->permit;
                        }
                    ?>
                    {{$totalPermit}}
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="info-box-2 bg-indigo">
            <div class="icon">
                <i class="material-icons">theaters</i>
            </div>
            <div class="content">
                <div class="text">FLOOR PERMITS</div>
                <div class="number count-to" data-from="0" data-to="257" data-speed="1000" data-fresh-interval="20">
                    <?php 
                        $totalFloorPermit=0;
                        foreach($data["floor_permit"] as $revenue){
                            $totalFloorPermit+=$revenue->permit;
                        }
                    ?>
                    {{$totalFloorPermit}}
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
            <div class="info-box-2 bg-teal">
                <div class="icon">
                    <i class="material-icons">equalizer</i>
                </div>
                <div class="content">
                    <div class="text">BUILDING PERMITS</div>
                    <div class="number count-to" data-from="0" data-to="257" data-speed="1000" data-fresh-interval="20">
                        <?php 
                            $totalBuildingPermit=0;
                            foreach($data["building_permit"] as $revenue){
                                $totalFloorPermit+=$revenue->permit;
                            }
                        ?>
                        {{$totalBuildingPermit}}
                    </div>
                </div>
            </div>
        </div>
    @endif
    {{-- <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="info-box-2 bg-default">
            <div class="icon">
                <i class="material-icons">bookmark</i>
            </div>
            <div class="content">
                <div class="text">VEHICLE REGISTERD</div>
                <div class="number count-to" data-from="0" data-to="117" data-speed="1000" data-fresh-interval="20">117</div>
            </div>
        </div>
    </div> --}}
</div>


<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="body">
                <div id="chart_div" style="width:100%; height:350px;"></div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="body">
                <div id="floor_bar_chart" style="width: 900px; height: 500px;"></div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="card">
            <div class="body">
                <div id="building_bar_chart" style="width: 900px; height: 500px;"></div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    google.charts.load('current', {'packages':['corechart']});
    google.charts.load('current', {'packages':['bar']});
    
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
        var data = google.visualization.arrayToDataTable(
            [
                ['Date', 'Permits'],
            @foreach($data["permit"] as $visitor)
                [ '{{date("Y-m-d",strtotime($visitor->date))}}',  {{$visitor->permit}} ],
            @endforeach
            ]
        );

        var options = {
        title: 'No of Visitor Parking Permit',
        legend: {position: 'top', maxLines: 3},
        hAxis: {title: 'Date',  titleTextStyle: {color: '#333'}},
        vAxis: {minValue: 0}
        };

        var chart = new google.visualization.AreaChart(document.getElementById('chart_div'));
        chart.draw(data, options);
    }

    google.charts.setOnLoadCallback(drawFloorBarChart);

    function drawFloorBarChart() {
        var data = google.visualization.arrayToDataTable(
            [
                ['Date', 'Permits'],
            @foreach($data["floor_permit"] as $visitor)
                [ '{{date("Y-m-d",strtotime($visitor->date))}}',  {{$visitor->permit}} ],
            @endforeach
            ]
        );

        var options = {
            chart: {
                title: 'No of Floor Visitor Parking Permit',
                subtitle: 'No of floor visitor parking permits from {{$date_from}} to {{$date_to}}',
            },
            bars: 'horizontal'
        };

        var chart = new google.charts.Bar(document.getElementById('floor_bar_chart'));

        chart.draw(data, google.charts.Bar.convertOptions(options));
    }

    google.charts.setOnLoadCallback(drawBuildingBarChart);

    function drawBuildingBarChart() {
        var data = google.visualization.arrayToDataTable(
            [
                ['Date', 'Permits'],
            @foreach($data["building_permit"] as $visitor)
                [ '{{date("Y-m-d",strtotime($visitor->date))}}',  {{$visitor->permit}} ],
            @endforeach
            ]
        );

        var options = {
            chart: {
                title: 'No of Building Visitor Parking Permit',
                subtitle: 'No of building visitor parking permits from {{$date_from}} to {{$date_to}}',
            },
            bars: 'horizontal'
        };

        var chart = new google.charts.Bar(document.getElementById('building_bar_chart'));

        chart.draw(data, google.charts.Bar.convertOptions(options));
    }

</script>

<div class="card" id="content">
    <div class="header">
        {{strtoupper(str_replace('_',' ',$table))}}
    </div>
    <div class="body">
        <div class="">
        <table id="example" class="mdl-data-table datatable table-hover display" style="width:100%;">
            <thead>
                <tr>
                    <th>Generated By</th>
                    <th>Parking Spot</th>
                    <th>Start from</th>
                    <th>End </th>
                    <th>Status</th>
                    <th>Generated At</th>
                    <th>Vehicle Model</th>
                    <th>License No</th>
                    <th>Vehicle Color</th>
                </tr>
            </thead>
            <tbody>
                @foreach($query as $record)
                    <tr>
                        <td>{{strtoupper($record->user->name)}}</td>
                        <td>{{strtoupper($record->parking_spot)}}</td>
                        <td>{{$record->start_from}}</td>
                        <td>{{$record->expired_on}}</td>
                        <td>
                            @if($record->status=="active")
                                <span class="label label-success">ACTIVE</span>
                            @elseif($record->status=="expired")
                                <span class="label label-danger">EXPIRED</span>
                            @elseif($record->status=="pending")
                                <span class="label label-info">PENDING</span>
                            @endif
                        </td>
                        <td>{{$record->created_at}}</td>
                        <td>{{$record->vehicle->model}}</td>
                        <td>{{$record->vehicle->license_no}}</td>
                        <td>{{$record->vehicle->color}}</td>
                    </tr>
                    {{-- <tr style="background:#EDE7F6;">
                        <th colspan="2">
                            <p style="font-weight:600;">Vehicle Model</p>
                            <p style="font-weight:600;">License No</p>
                            <p style="font-weight:600;">Color</p>
                        </th>
                        <td colspan="2">
                            <p style="text-align:left">{{$record->vehicle->model}}</p>
                            <p style="text-align:left">{{$record->vehicle->license_no}}</p>
                            <p style="text-align:left">{{$record->vehicle->color}}</p>
                        </td>
                        <td colspan="2"></td>
                    </tr> --}}
                @endforeach
            </tbody>
        </table>
        </div>
    </div>
</div>