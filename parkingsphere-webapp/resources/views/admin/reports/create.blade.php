@extends('layouts.core')

@section('content_body')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    GENERATE REPORT
                </h2>
            </div>
            <div class="body">
                <form action="{{route('admin.report.generate')}}" method="POST" id="form_generate_report">
                    {{csrf_field()}}
                    <div class="row clearfix">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <input name="report_type" type="radio" id="radio_11" class="radio-col-indigo report_type" value="parking_permit" checked required/>
                                <label for="radio_11">Parking Permit Payments</label>
                            </div>
                            <div class="form-group">
                                <input name="report_type" type="radio" id="radio_12" class="radio-col-indigo report_type" value="visitor_parking_permit" required/>
                                <label for="radio_12">Visitor's Parking Permit History</label>
                            </div>
                            <div class="form-group">
                                <input name="report_type" type="radio" id="radio_13" class="radio-col-indigo report_type" value="towed_vehicle" required/>
                                <label for="radio_13">Towed Vehicle</label>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <input type="hidden" name="range_selector" value="0">
                                <input type="checkbox" id="md_checkbox_10" name="range_selector" value="1" class="filled-in chk-col-indigo" onchange="showDateRange(this)"/>
                                <label for="md_checkbox_10">Specific range</label>
                            </div>
                            <div id="date_range" style="display:none">
                                <label>From</label>
                                <div class="input-group">
                                    <div class="form-line">
                                        <input type="text" name="date_from" class="datepicker form-control date date_from" placeholder="Ex: 30/07/2016">
                                    </div>
                                </div>
                                <label>To</label>
                                <div class="input-group">
                                    <div class="form-line">
                                        <input type="text" name="date_to" class="datepicker form-control date date_to" placeholder="Ex: 30/07/2016">
                                    </div>
                                </div>
                            </div>
                            <div id="date_tags">
                                <div class="form-group">
                                    <input name="date_range" type="radio" id="radio_15" class="radio-col-indigo date_tag_radio_input" checked value="today"/ required>
                                    <label for="radio_15">Today</label>
                                </div>
                                <div class="form-group">
                                    <input name="date_range" type="radio" id="radio_16" class="radio-col-indigo date_tag_radio_input" value="week"/ required>
                                    <label for="radio_16">Last Week</label>
                                </div>
                                <div class="form-group">
                                    <input name="date_range" type="radio" id="radio_17" class="radio-col-indigo date_tag_radio_input" value="month"/ required>
                                    <label for="radio_17">Last Month</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4" id="filter_user">
                            <div class="form-group" id="selected_user">
                                <p>
                                    <b>Search tenant by username</b>
                                </p>
                                <select class="form-control show-tick tenant_select_input" data-live-search="true" name="selected_user[]" multiple required>
                                    @foreach($users as $user)
                                        <option value="{{$user->id}}">{{ucfirst($user->name)}}, {{$user->apartment->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                    <input type="hidden" name="all_users" value="0">
                                <input type="checkbox" id="md_checkbox_14" name="all_users" value="1" class="filled-in chk-col-indigo" onchange="showSelectUsers(this)"/>
                                <label for="md_checkbox_14">All Tenants</label>
                            </div>
                        </div>
                        <div class="col-sm-4 pull-right">
                            <div class="form-group">
                            <button type="submit" class="btn btn-block btn-lg btn-primary waves-effect pull-right" onclick="generateReport(this,event)">Generate</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="show_report">
    <div id="preloader" style="display:none">
        <div style="width:100px; margin:25px auto;">
            <div class="loader">
                <div class="preloader">
                    <div class="spinner-layer pl-red">
                        <div class="circle-clipper left">
                            <div class="circle"></div>
                        </div>
                        <div class="circle-clipper right">
                            <div class="circle"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
            <!--#END# DateTime Picker -->
@endsection

@section('script')
<script>
  $('.date').bootstrapMaterialDatePicker({
      time:false
  });

  function generateReport(dom,e){
    e.preventDefault();
    form=$(dom).closest('form');
    url=form.prop('action');
    data=form.serialize();

    $( document ).ajaxStart(function() {
        $("#show_report").find('#preloader').removeAttr('style');
    });

    $.ajax({
        url:url,
        method:'POST',
        data:data,
        success:function(html){
            $('#show_report').html(html);
            var header_message=$(html).find('.block-header').html();
            $('#show_report').find('table').dataTable({
                responsive:true,
                select: true,
                dom: 'Bfrtip',
                lengthMenu: [
                    [ 10, 25, 50, -1 ],
                    [ '10 rows', '25 rows', '50 rows', 'Show all' ]
                ],
                buttons: [
                    'pageLength','pdf', 'excel', 'print'
                ]
            });

            $('html, body').animate({
                scrollTop: $("#scrollTo").offset().top
            }, 500);
            // document.getElementById("form_generate_report").reset();
        } 
    })
  }    

  $('.report_type').on('click',function(){
      if($(this).val()=='towed_vehicle'){
        $('#filter_user').slideUp();
      }
      else{
        $('#filter_user').slideDown();
      }
  })

  function showDateRange(dom){
      if($(dom).prop('checked')){
        $('#date_range').slideDown();
        $('#date_tags').slideUp();
        // console.log('true');
        $('#date_range').find('.date').prop('required','required');
        $('#date_tags').find('.date_tag_radio_input').removeAttr('required');
      }
      else{
        $('#date_range').slideUp();
        $('#date_tags').slideDown();
        $('#date_range').find('.date_from').val('');
        $('#date_range').find('.date_to').val('');
        $('#date_tags').find('.date_tag_radio_input').prop('required','required');
        $('#date_range').find('.date').removeAttr('required');
      }
  }

  function showSelectUsers(dom){
      if($(dom).prop('checked')){
        $('#selected_user').slideUp();
        $('#selected_user').find('.tenant_select_input').removeAttr('required');
        // console.log('true');
      }
      else{
        $('#selected_user').slideDown();
        $('#selected_user').find('.tenant_select_input').prop('required','required');
      }
  }

</script>
@endsection