@extends('layouts.main')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-7 col-lg-offset-1">
            <div class="block-header" style="margin-top:4%;">
                <p class="font-24" style="color:rgb(23,75,140)"><b>ParkingSphere.com</b><br>
                    <small>Submit your vehicle informations here.</small>
                </p>
            </div>
            <div style="margin:0px; width:100%; height:auto;">
                @if(session('return')=='success')
                    <div class="alert alert-success">
                        <strong>Success!</strong> {{session('message')}}<br>
                        <button class="btn btn-primary btn-sm wave-effects" onclick="showQrCode('{{route('qrcode.generate',['ticket_token'=>session('ticket_token')])}}')" style="margin-top: 8px;">View QRCode</button>
                    </div>
                    <div id="qr-cover" style="min-height:290px; width:100%; padding:20px; display:none; margin-bottom:15px;">
                        <div id="qr-image" style="width:250px; min-height:250px; margin:0px auto;"></div>
                        <p class="font-24" style="text-transform:uppercase; text-align:center; margin-top: 10px;">{{session('ticket_token')}}</p>
                    </div>
                @elseif(session('return')=='error')
                    <div class="alert alert-danger">
                        <strong>Error!</strong> {{session('message')}}
                    </div>
                @endif
            </div>
            <div class="card">
                {{-- <div class="header">
                    <h2>
                        
                    </h2>
                </div> --}}
                <div class="body">
                    <form action="{{route('visitor.details.create')}}" method="POST" enctype="multipart/form-data">
                        <input type="hidden" name="ticket_token" value="{{$token}}">
                        {{csrf_field()}}
                        <label for="model">Vehicle Model</label>
                        <div class="form-group">
                            <div class="form-line {{$errors->first('vehicle_model')?'focused error':''}}">
                                <input type="text" id="model" name="vehicle_model" class="form-control" placeholder="Enter your vehicle model" value="{{ old('vehicle_model') }}">
                            </div>
                        </div>
                        <label for="license_no">Vehicle License No</label>
                        <div class="form-group">
                            <div class="form-line {{$errors->first('vehicle_license_no')?'focused error':''}}">
                                <input type="text" id="license_no" name="vehicle_license_no" class="form-control" placeholder="Enter your vehicle license no" value="{{ old('vehicle_license_no') }}" >
                            </div>
                        </div>
                        <label for="color">Vehicle Color</label>
                        <div class="form-group">
                            <div class="form-line {{$errors->first('vehicle_color')?'focused error':''}}">
                                <input type="text" id="color" name="vehicle_color" class="form-control" placeholder="Enter your vehicle color" value="{{ old('vehicle_color') }}">
                            </div>
                        </div>
                        <label for="image">Upload your images</label>
                        <div class="form-group">
                            <div class="form-line">
                                <input type="file" class="form-control" name="vehicle_image[]">
                                <input type="file" class="form-control" name="vehicle_image[]">
                                <input type="file" class="form-control" name="vehicle_image[]">
                                <input type="file" class="form-control" name="vehicle_image[]">
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary btn-lg waves-effect">SUBMIT</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script type="text/javascript">
        function showQrCode(url){
            $('#qr-cover').toggle();
            $.get(url,'',function(res){
                $('#qr-image').html(res);
            })
        }
    </script>
@endsection