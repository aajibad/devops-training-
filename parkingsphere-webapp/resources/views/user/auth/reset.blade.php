@extends('layouts.main')

@section('body_class')
    class="fp-page"
@endsection

@section('content')

<div class="fp-box">
        <div class="logo">
            <a href="javascript:void(0);">Parking<b>Sphere</b></a>
            <small>User Reset Password</small>
        </div>
        <div class="card">
            <div class="body">
                <div class="alert" id="response" style="display:none"></div>
                <form id="forgot_password" method="POST" action="{{ route('user.password.request') }}">
                    {{ csrf_field() }}

                    <input type="hidden" name="token" value="{{ $token }}">

                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">email</i>
                        </span>
                        <div class="form-line">
                            <input type="email" class="form-control" name="email" placeholder="Email" required autofocus value="{{$email}}">
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            <input type="password" class="form-control" name="password" id="password" placeholder="Password" required autofocus>
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            <input type="password" class="form-control" name="password_confirmation" id="password_confirmation" placeholder="Re-type password" required autofocus>
                            @if ($errors->has('password_confirmation'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <button class="btn btn-block btn-lg bg-pink waves-effect" type="submit" onclick="resetPassword();">RESET PASSWORD</button>

                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
<script>

    function resetPassword(){
        var form=$('#forgot_password');
        
        form.submit(function(e){
            return false;
        })

        $.ajax({
            url:form.prop('action'),
            method:'post',
            data:form.serialize(),
            success:function(response){
                if(response.message=='success'){
                    $('#response').slideDown().addClass('alert-success').html('<h6 style="text-align: center; margin: 0px; font-weight: 400;">Your password has been reset!</h6>');
                }
                else if(response.message=='failed'){
                    $('#response').slideDown() .addClass('alert-danger').html('<h6 style="text-align: center; margin: 0px; font-weight: 400;">Oops! something wrong.</h6>');
                }
            }
        })    
    }
</script>
@endsection