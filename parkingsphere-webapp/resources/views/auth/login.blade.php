@extends('layouts.auth')

@section('content')
<!-- <div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Login</div>
                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Login
                                </button>

                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    Forgot Your Password?
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> -->

<div class="app-form">
  <div class="form-header">
            <div class="app-brand"><span class="highlight">ParkingSphere</div>
          </div>
  <form method="POST" action="{{ route('login') }}">
    {{ csrf_field() }}
    <div class="input-group">
      <span class="input-group-addon" id="basic-addon1">
        <i class="fas fa-user" aria-hidden="true"></i>
      </span>
      <input id="email" type="email" class="form-control" name="email" placeholder="Email" aria-describedby="basic-addon1">
      @if ($errors->has('email'))
          <span class="help-block">
              <strong>{{ $errors->first('email') }}</strong>
          </span>
      @endif
    </div>
    <div class="input-group">
      <span class="input-group-addon" id="basic-addon2">
        <i class="fas fa-key" aria-hidden="true"></i>
      </span>
      <input id="password" type="password" name="password" class="form-control" placeholder="Password" aria-describedby="basic-addon2">
      @if ($errors->has('password'))
          <span class="help-block">
              <strong>{{ $errors->first('password') }}</strong>
          </span>
      @endif
    </div>
    <div class="checkbox">
      <input type="checkbox" id="checkbox1" name="remember" {{ old('remember') ? 'checked' : '' }}>
      <label for="checkbox1">
        Remember Me
      </label>
    </div>
    <div class="text-center">
      <input type="submit" class="btn btn-success btn-submit" value="Login">
    </div>
    <div class="text-center">
      <a class="btn btn-link" href="{{ route('password.request') }}">
          Forgot Your Password?
      </a>
    </div>
  </form>
  <div class="form-line">
            <div class="title">OR</div>
          </div>
  <div class="form-footer">
            <button type="button" class="btn btn-default btn-sm btn-social __facebook">
              <div class="info">
                <i class="icon fab fa-facebook-f" aria-hidden="true"></i>
                <span class="title">Login with Facebook</span>
              </div>
            </button>
          </div>
</div>
@endsection
