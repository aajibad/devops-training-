<?php

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Redis;
// use Auth;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('redis/check',function(){
    $data=[
        "Hello world"
    ];

    Redis::publish('test-channel', json_encode($data));

    return response()->json("Done.");
});

Route::middleware('jwt.auth')->get('user', function (Request $request) {
    $user=JWTAuth::user();
    return response()->json([
        'user'=>$user,
        'apartment'=>$user->apartment,
        'parking_permits'=>$user->parkingPermits,
        'visitor_permits'=>$user->visitorParkingPermits,
        'vehicles'=>$user->vehicles
    ]);
});

Route::middleware('api')->get('apartments', function () {
    return App\Apartment::where(["status"=>"active"])->get()->toJson();
});

Route::group(['prefix'=>'user','middleware'=>['api','cors']],function () {
    
    Route::post('/login','Auth\User\LoginController@login');

    Route::post('/login/with','Auth\User\LoginController@loginWith');

    Route::post('/register', 'Auth\User\RegisterController@register');

    Route::post('/password/email', 'Auth\User\ForgotPasswordController@sendResetLinkEmail');

    // Route::post('/logout',function(){
    //     return response()->json(['message'=>'success']);  
    // });
});

Route::group(['prefix'=>'user','middleware'=>['jwt.auth']],function () {
    Route::get('/show',function(Request $request){
        return Auth::user()->name;
    });  
    
    // Update user status
    // Params id , status=boolean
    Route::get('/status/update','User\UserController@updateStatus');

    Route::get('/permits','User\UserController@permitsByUserId');

    Route::post('/profile/update','User\UserController@update');
});

Route::group(['prefix'=>'complaint','middleware'=>['jwt.auth']],function () {
    Route::get('/conversations','User\MessageController@conversations'); 
    Route::post('/create','User\MessageController@create');  
    Route::get('/show','User\MessageController@showConversationByUserId');  
    Route::get('/message/read','User\MessageController@markAsRead');  
    Route::get('/message/{id}','User\MessageController@showMessageByConversationId');
    Route::get('/conversation/delete','User\MessageController@deleteConversation');
    Route::post('/upload/image','User\MessageController@uploadComplaintImages'); 
    Route::get('/show/images','User\MessageController@showComplaintImages'); 
});

Route::middleware(['jwt.auth'])->get('/announcements','User\MessageController@announcements'); 

Route::group(['prefix'=>'ticket','middleware'=>['jwt.auth']],function () {
    Route::post('/purchase','User\TicketController@create');
    Route::post('/generate','User\VisitorTicketController@generate');  
    Route::get('/list','User\TicketController@listPermits');  
    Route::get('/list/visitor','User\VisitorTicketController@listVisitorPermits');  
});

Route::group(['prefix'=>'vehicle','middleware'=>['auth:user-api']],function () {
    Route::get('/search','User\VehicleController@search');
    Route::get('/show/images','User\VehicleController@showVehicleImages');
});

// API Admin
Route::group(['prefix'=>'admin','middleware'=>['api','cors']],function () {
    Route::post('/login','Auth\Admin\LoginController@TokenizeLogin');
});

Route::group(['prefix'=>'admin','middleware'=>['auth:admin-api','api','cors']],function () {
    Route::get('/check','Admin\UserController@authenticateTokenUser');
    Route::post('/invalidate','Admin\UserController@invalidateTokenUser');

    // Search vehicle
    Route::post('/vehicle/search','Admin\UserController@searchVehicle');
});