<?php

use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Illuminate\Http\Response;
use Illuminate\Http\Request;

// use Nexmo;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Auth::routes();

// Route::get('/', function () {
//   return view('welcome'); 
// });

// Route::get('qr-code', function () 
// {
//   return QRCode::text('QR Code Generator for Laravel!')->png();    
// });
// Route::get('api/user', function () {
//   $users=Apartment::all();
//   return response()->json($users);
// });

Route::get('/', 'HomeController@landing')->name('home');

Route::get('/message/send', 'Admin\MessageController@create');

Route::get('admin/setup','Admin\SetupController@index')->middleware(['auth:admin'])->name('setup');
Route::post('admin/setup/submit','Admin\SetupController@create')->middleware(['auth:admin'])->name('setup.create');

// Admin route
Route::group(['prefix'=>'admin'],function () {

  Route::get('/register', 'Auth\Admin\RegisterController@showRegistrationForm')->name('admin.register.form');
  Route::post('/register', 'Auth\Admin\RegisterController@register')->name('admin.register');

  Route::get('/login', 'Auth\Admin\LoginController@showLoginForm')->name('admin.login.form');
  Route::post('/login', 'Auth\Admin\LoginController@login')->name('admin.login');

  Route::post('/password/email', 'Auth\Admin\ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
  Route::get('/password/reset', 'Auth\Admin\ForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
  Route::post('/password/reset', 'Auth\Admin\ResetPasswordController@reset');
  Route::get('/password/reset/{token}', 'Auth\Admin\ResetPasswordController@showResetForm')->name('admin.password.reset');

  Route::post('/logout', 'Admin\UserController@logout')->name('admin.logout');
});

// Login with social
Route::get('admin/login/{service}', 'Auth\Admin\SocialLoginController@redirectToProvider')->name('admin.login.social.redirect');
Route::get('admin/login/{service}/callback', 'Auth\Admin\SocialLoginController@handleProviderCallback')->name('admin.login.social.callback');

// Home | Settings
Route::group(['prefix'=>'admin','middleware'=>['auth:admin','setup']],function () {
  Route::get('/home', 'Admin\HomeController@index')->name('admin.home');
  Route::get('/vehicle/{id}', 'Admin\HomeController@showVehicle')->name('user.vehicle');
  Route::get('/home/switch/{apartment}', 'Admin\HomeController@switchApartment')->name('admin.apartment.switch');
  Route::get('/home/{date}', 'Admin\HomeController@index')->name('admin.analytic');
  
});

// Admin Option
Route::group(['prefix'=>'admin/settings','middleware'=>['auth:admin','setup']],function(){
  Route::get('/', 'Admin\AdminOptionController@index')->name('admin.settings');
  Route::post('/create', 'Admin\AdminOptionController@create')->name('admin.settings.create');
  Route::get('/subscription/{type}', 'Admin\AdminOptionController@subscription')->name('admin.settings.subscription');
  Route::post('/add/permit', 'Admin\AdminOptionController@addParkingPermit')->name('admin.settings.permit.add');
  Route::get('/permit/history/{permit_type}', 'Admin\AdminOptionController@showSetParkingPermitHistory')->name('admin.settings.permit.history');
});

// Payments

Route::group(['prefix'=>'admin/payment','middleware'=>['auth:admin','setup']],function () {
  Route::get('/', 'Admin\PaymentController@index')->name('admin.payments');
  
  Route::post('/create', 'Admin\PaymentController@create')->name('admin.payment.create');

  Route::get('/delete', 'Admin\PaymentController@delete')->name('admin.payment.delete');

  Route::get('/upgrade', 'Admin\PaymentController@upgradeSubscription')->name('admin.payment.upgrade');

  // Review subscription payment for express checkout
  Route::get('/review', 'Admin\PaymentController@reviewSubscription')->name('admin.subscription.review');

  // Express checkout create and authorize payment routes
  Route::post('/prepare', 'Admin\PaymentController@readyToCheckoutSubscription')->name('admin.subscription.prepare');

  Route::post('/proceed','Admin\PaymentController@proceedToCheckoutSubscription')->name('admin.subscription.proceed');

  // Express checkout return routes
  Route::get('/confirm','Admin\PaymentController@reviewSubscription')->name('admin.subscription.confirm');

  Route::get('/cancel','Admin\PaymentController@proceedToCancelSubscription')->name('admin.subscription.cancel');

  // Payment billing plan
  Route::post('/plan/create','Admin\PaymentController@createBillingPlan')->name('admin.subscription.plan.create');

  Route::get('/plan/activate','Admin\PaymentController@activateBillingPlan')->name('admin.subscription.plan.activate');

  Route::post('/plan/cancel','Admin\PaymentController@cancelBillingPlan')->name('admin.subscription.plan.cancel');

  Route::get('/plan/get/{subscription_id}','Admin\PaymentController@getBillingPlan')->name('admin.subscription.plan');

  // Execute billing agreement
  Route::post('/agreement/execute','Admin\PaymentController@executeBillingAgreement')->name('admin.subscription.agreement.execute');

});

// Get instant notification payments from PAY PAL
Route::post('/payment/ipn','Admin\PaymentController@getPaymentNotification');

// Cards
Route::group(['prefix'=>'admin/card','middleware'=>['auth:admin','setup']],function () {
  Route::get('/', 'Admin\CardController@index')->name('admin.cards');
  Route::post('/create', 'Admin\CardController@create')->name('admin.card.create');
  Route::post('/update', 'Admin\CardController@update')->name('admin.card.update');
  Route::post('/delete', 'Admin\CardController@delete')->name('admin.card.delete');
});

// Users
Route::group(['prefix'=>'admin/user','middleware'=>['auth:admin','setup']],function () {
  Route::get('/', 'Admin\UserController@index')->name('admin.users');
  Route::get('/show', 'Admin\UserController@show')->name('admin.users.show');
  Route::get('/history/{id}', 'Admin\UserController@showUserHistory')->name('admin.user.history');
  Route::post('/create/apartment', 'Admin\UserController@createApartment')->name('admin.users.apartment.create');
  Route::post('/create/admin', 'Admin\UserController@createAdmin')->name('admin.admin.create');
  Route::post('/create/user', 'Admin\UserController@createUser')->name('admin.user.create');
  Route::post('/update', 'Admin\UserController@update')->name('admin.user.update');
  Route::get('/delete', 'Admin\UserController@delete')->name('admin.users.delete');
  // Route::get('/change/permission', 'Admin\UserController@changePermission')->name('admin.user.permission');
  
  Route::get('/approvals', 'Admin\UserController@listApprovals')->name('admin.users.approvals');
  Route::post('/approve', 'Admin\UserController@approveUser')->name('admin.user.approve');
});

// Report
Route::group(['prefix'=>'admin/report','middleware'=>['auth:admin','setup']],function () {
  Route::get('/', 'Admin\ReportController@index')->name('admin.report.form');
  Route::post('/report/generate', 'Admin\ReportController@generateReport')->name('admin.report.generate');
});


// Towed Vehicles
Route::group(['prefix'=>'admin/towing','middleware'=>['auth:admin','setup']],function () {
  Route::get('/', 'Admin\TowedVehicleController@index')->name('admin.towed_vehicles');
  Route::post('/create', 'Admin\TowedVehicleController@create')->name('admin.towing.company.create');
  Route::get('/delete/{id}', 'Admin\TowedVehicleController@delete')->name('admin.towing.company.delete');
});

// Notification
Route::group(['prefix'=>'admin/notification','middleware'=>['auth:admin','setup']],function () {
  Route::get('/', 'Admin\NotificationController@index')->name('admin.notifications');
  Route::post('/send', 'Admin\NotificationController@create')->name('admin.notification.send');
});

// Complaints
Route::group(['prefix'=>'admin/complaints','middleware'=>['auth:admin','setup']],function () {
  Route::get('/', 'Admin\ComplaintController@index')->name('admin.complaints');
  Route::get('/show', 'Admin\ComplaintController@show')->name('admin.complaint.show');
  Route::post('/reply', 'Admin\ComplaintController@create')->name('admin.complaint.reply');
  Route::get('/send', 'Admin\MessageController@send')->name('admin.complaint.send');
});

// Wallets
Route::group(['prefix'=>'admin/wallet','middleware'=>['auth:admin','setup']],function () {
  Route::get('/', 'Admin\WalletController@index')->name('admin.wallets');
  Route::get('/send/request', 'Admin\WalletController@send')->name('admin.wallet.request');
});


// Super Admin route
Route::group(['prefix'=>'superadmin'],function () {

  Route::get('/register', 'Auth\Superadmin\RegisterController@showRegistrationForm')->name('superadmin.register.form');
  Route::post('/register', 'Auth\Superadmin\RegisterController@register')->name('superadmin.register');
  Route::get('/login', 'Auth\Superadmin\LoginController@showLoginForm')->name('superadmin.login.form');
  Route::post('/login', 'Auth\Superadmin\LoginController@login')->name('superadmin.login');
  Route::get('/home','Superadmin\HomeController@index')->name('superadmin.home');
  Route::post('/option','Superadmin\SuperAdminOptionController@create')->name('superadmin.option.create');

  Route::post('/password/email', 'Auth\Superadmin\ForgotPasswordController@sendResetLinkEmail')->name('superadmin.password.email');
  Route::get('/password/reset', 'Auth\Superadmin\ForgotPasswordController@showLinkRequestForm')->name('superadmin.password.request');
  Route::post('/password/reset', 'Auth\Superadmin\ResetPasswordController@reset');
  Route::get('/password/reset/{token}', 'Auth\Superadmin\ResetPasswordController@showResetForm')->name('superadmin.password.reset');

  Route::post('/logout', 'Superadmin\UserController@logout')->name('superadmin.logout');
});

// // Superadmin Cards
// Route::group(['prefix'=>'superadmin/card','middleware'=>'auth:superadmin'],function () {
//   Route::get('/', 'Superadmin\CardController@index')->name('superadmin.cards');
//   Route::post('/create', 'Superadmin\CardController@create')->name('superadmin.card.create');
//   Route::get('/update', 'Superadmin\CardController@update')->name('superadmin.card.update');
//   Route::post('/delete', 'Superadmin\CardController@delete')->name('superadmin.card.delete');
// });

// Superadmin Home
Route::group(['prefix'=>'superadmin','middleware'=>'auth:superadmin'],function () {
  Route::get('/home', 'Superadmin\HomeController@index')->name('superadmin.home');
  Route::get('/profile', 'Superadmin\UserController@index')->name('superadmin.profile');
  // Route::get('/withdrawals', 'Superadmin\WithdrawalController@index')->name('superadmin.withdrawals');
  // Route::get('/withdrawal_requests', 'Superadmin\Withdrawal@index')->name('superadmin.withdrawal.requests');
});

Route::group(['prefix'=>'superadmin/apartments','middleware'=>'auth:superadmin'],function () {
  Route::get('/', 'Superadmin\ApartmentController@index')->name('superadmin.apartments');
  Route::post('/change/status', 'Superadmin\ApartmentController@changeStatus')->name('superadmin.apartment.change.status');
});

Route::group(['prefix'=>'superadmin/user','middleware'=>'auth:superadmin'],function () {
  Route::get('/', 'Superadmin\UserController@index')->name('superadmin.users');
  Route::get('/apartments/{id}', 'Superadmin\UserController@showApartments')->name('superadmin.user.apartments');
  Route::get('/subscriptions/{id}', 'Superadmin\UserController@showSubscriptionHistory')->name('superadmin.user.subscriptions');
  Route::get('/user/{id}', 'Superadmin\UserController@showVehicleInfo')->name('superadmin.user.vehicle');
  Route::post('/block_apartments', 'Superadmin\UserController@blockAllApartments')->name('superadmin.apartments.block');
});

// Superadmin Payments
Route::group(['prefix'=>'superadmin/payment','middleware'=>'auth:superadmin'],function () {
  Route::get('/', 'Superadmin\PaymentController@index')->name('superadmin.payments');
  Route::post('/action', 'Superadmin\PaymentController@create')->name('superadmin.payment.create');
  Route::get('/delete', 'Superadmin\PaymentController@delete')->name('superadmin.payment.delete');
});

Route::post('/password/reset', 'Auth\User\ResetPasswordController@reset')->name('user.password.request');
Route::get('/password/reset/{token}', 'Auth\User\ResetPasswordController@showResetForm')->name('password.reset');

Route::get('/vehicle/details/{token}','User\VisitorTicketController@showForm')->name('visitor.details.show.form');
Route::post('/vehicle/details','User\VisitorTicketController@registerVehicle')->name('visitor.details.create');
Route::get('/generate/qrcode/{ticket_token}','User\VisitorTicketController@showQrCode')->name('qrcode.generate');

// QR code test route
Route::get('/test/qrcode/{message}',function($message){
  // return var_dump(public_path('images/admin/logo.png'));  
  return QrCode::margin(2)
      ->size(250)
      ->generate('Apartment : eihw')
      ->generate('Message : deeiwewuig');
});

Route::get('/test/sms/{phone}/{message}',function($phone,$message){
    try {
        $send=Nexmo::message()->send([
          'to'   => $phone,
          'from' => '16105552344',
          'text' => $message
        ]);
    } catch (Exception $e) {
        return $e->getMessage();
    }
});

// Make https requests
if (env('APP_ENV') === 'production') {
  \URL::forceScheme('https');
}
