#!/bin/bash
# Licensed to Cloudera, Inc. under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  Cloudera, Inc. licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# This should be run on the host on which the CM server is to run.

# Set up some vars
config_file=clouderaconfig.ini
cm_server_host=$(grep cm.host ${config_file} | awk -F'=' '{print $2}')
scm_db_host=$(grep db.host ${config_file} | awk -F'=' '{print $2}')
scm_db=$(grep scm.db ${config_file} | awk -F'=' '{print $2}')
scm_user=$(grep scm.user ${config_file} | awk -F'=' '{print $2}')
scm_pass=$(grep scm.password ${config_file} | awk -F'=' '{print $2}')

# Prep Cloudera repo
sudo yum -y install wget
wget http://archive.cloudera.com/cm5/redhat/7/x86_64/cm/cloudera-manager.repo
sudo mv cloudera-manager.repo /etc/yum.repos.d/
if [ ! -e "/etc/yum.repos.d/cloudera-manager.repo" ]
then
    echo "Unable to verify existance of the CDH manager repo, unable to continue"
    exit 1
fi

# Turn off firewall
sudo service iptables stop

# Turn off SELINUX
echo 0 | sudo tee /selinux/enforce > /dev/null

# Set up python
sudo pip install cm_api
sudo yum install -y oracle-j2sdk1.7
sudo yum install -y mysql-connector-java
# Make sure DNS is set up properly so all nodes can find all other nodes

# For master
sudo yum -y install cloudera-manager-agent cloudera-manager-daemons cloudera-manager-server
sudo /usr/share/cmf/schema/scm_prepare_database.sh mysql -h ${scm_db_host} ${scm_db} ${scm_user} ${scm_pass}
#sudo yum -y install cloudera-manager-server-db-2
#sudo service cloudera-scm-server-db start
sudo service cloudera-scm-server start
RETVAL=`sudo service cloudera-scm-server status`
ACTIVE_VAL=`echo $RETVAL | sed -rn "s/.*Active: ([a-z]+) .*/\1/p"`
if [ ! "active" eq $ACTIVE_VAL ]
then
    echo "Service cloudera-scm-server was unable to be started"
    exit 1
fi
sudo sed -i.bak -e"s%server_host=localhost%server_host=${cm_server_host}%" /etc/cloudera-scm-agent/config.ini
sudo service cloudera-scm-agent start
RETVAL=`sudo service cloudera-scm-agent status`
ACTIVE_VAL=`echo $RETVAL | sed -rn "s/.*Active: ([a-z]+) .*/\1/p"`
if [ ! "active" eq $ACTIVE_VAL ]
then
    echo "Service cloudera-scm-agent was unable to be started"
    exit 1
fi

# Sleep for a while to give the agents enough time to check in with the master.
# Or better yet, make a dependency so that the slave setup scripts don't start until now and the rest of this script doesn't finish until the slaves finish.
sleep_time=180
echo "Sleeping for ${sleep_time} seconds so managed cluster nodes can get set up."
sleep ${sleep_time}
echo "Done sleeping. Deploying cluster now."

# Execute script to deploy Cloudera cluster
#sudo python deploycloudera.py

# Now stop the cluster gracefully if necessary; ie if all servers are automatically rebooted at the end of the provisioning process
#sudo python stopcloudera.py
