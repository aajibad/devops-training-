variable "region" {}

variable "account" {}

variable "accountID" {}

variable "cluster" {}

variable "environment" {}

variable "os" {}

variable "seclevel" {}

variable "ami" {}

variable "instanceSubnet" {}

variable "instanceVPCID" {}

variable "instanceIPCidrBlock" {}

variable "ephemeralVPCID" {}

variable "hdfsCount" {}

variable "wnCount" {}

variable "mnCount" {}

variable "cmCount" {}

#variable "cdhinstanceprofile_id"{


#}

