resource "aws_security_group" "cdh_access_sg" {
  name = "CDHCluster-${var.cluster}-CDHAccessSG"
  description = "intranet-clouderaAccess"
  vpc_id = "${var.instanceVPCID}"

  ingress {
    from_port = 0
    to_port = 0
    protocol = -1
    cidr_blocks = ["10.146.16.0/22","134.244.0.0/16","146.122.0.0/16","161.134.0.0/16","172.17.1.0/24"]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

output "cdh_access_sg_id" {
  value = "${aws_security_group.cdh_access_sg.id}"
}

resource "aws_security_group" "cluster_sg" {
  name = "CDHCluster-${var.cluster}-IntraClusterSG"
  description = "intra-ClusterAccess"
  vpc_id = "${var.instanceVPCID}"

  ingress {
    from_port = 0
    to_port = 0
    protocol = -1
    cidr_blocks = ["${var.instanceIPCidrBlock}"]
    security_groups = ["${aws_security_group.wn_elb_sg.id}"]
    self = "true"
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
output "cluster_sg_id" {
  value = "${aws_security_group.cluster_sg.id}"
}
resource "aws_security_group" "remote_access_sg" {
  name = "CDHCluster-${var.cluster}-RemoteAccessSG"
  description = "intranet-remoteAccess"
  vpc_id = "${var.instanceVPCID}"

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["10.146.16.0/22","134.244.0.0/16","146.122.0.0/16","161.134.0.0/16","172.17.1.0/24"]
  }

  ingress {
    from_port = 3389
    to_port = 3389
    protocol = "tcp"
    cidr_blocks = ["10.146.16.0/22","134.244.0.0/16","146.122.0.0/16","161.134.0.0/16","172.17.1.0/24"]
  }

  ingress {
    from_port = -1
    to_port = -1
    protocol = "icmp"
    cidr_blocks = ["10.146.16.0/22","134.244.0.0/16","146.122.0.0/16","161.134.0.0/16","172.17.1.0/24"]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
output "remote_access_sg_id" {
  value = "${aws_security_group.remote_access_sg.id}"
}

resource "aws_security_group" "wn_elb_sg" {
  name = "CDHCluster-${var.cluster}-WNELB"
  description = "CDHCluster-${var.cluster}-WNELB"
  vpc_id = "${var.instanceVPCID}"

  ingress {
    from_port = 21050
    to_port = 21050
    protocol = "tcp"
    cidr_blocks = ["${var.instanceIPCidrBlock}"]
  }

  ingress {
    from_port = 21000
    to_port = 21000
    protocol = "tcp"
    cidr_blocks = ["${var.instanceIPCidrBlock}"]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
output "wn_elb_sg_id" {
  value = "${aws_security_group.wn_elb_sg.id}"

}
