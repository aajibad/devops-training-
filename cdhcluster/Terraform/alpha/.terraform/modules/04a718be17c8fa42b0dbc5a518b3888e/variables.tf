variable "region" {

}

variable "account" {

}

variable "accountID" {

}

variable "cluster" {

}



variable "os" {
  type = "string"
  default = "centos7"
}

variable "seclevel" {

}

variable "ami" {

}

variable "instanceSubnet" {

}

variable "instanceVPCID" {

}

variable "instanceIPCidrBlock" {

}

variable "ephemeralVPCID" {

}

variable "hdfsCount" {

}

variable "wnCount" {

}
variable "environment" {

}
variable "mnCount" {

}
variable "cmCount" {

}
#variable "masternodes_id"{

#}
#variable "clouderanode_id"{

#}
#FROM  SECURITY GROUP MODULE
variable "wn_elb_sg_id"{}
#from workernodes INSTANCE MODULE
variable "worknodes_id"{
  type = "list"
}
#variable "masternodes_id"{

#}
#variable "clouderanode_id"{

#}
##other modules
variable "cdhrole_id"{}
variable "remote_access_sg_id"{}
variable "cluster_sg_id"{}
variable "cdhinstanceprofile_id"{}
variable "cdh_access_sg_id"{}
