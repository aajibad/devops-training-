#!/bin/bash

yum install -y epel-release python python-pip ansible dos2unix
yum install -y python-pip
pip install awscli
tempdir=$(mktemp -t -d ansible.XXXXXX)
account_name=$(aws --output text iam list-account-aliases | awk '{print $2}' | head -1)
###aws s3 sync s3://${account_name}-continuous-delivery/Ansible/integration/ ${tempdir}
aws s3 sync s3://${account_name}-continuous-delivery/Ansible/playbooks/ ${tempdir}
for i in `find $tempdir -type f`; do dos2unix $i; done
#ansible-playbook -i ${tempdir}/playbooks/localhost.cfg ${tempdir}/base.yml --extra-vars="hosts=all"
#ansible-playbook -i ${tempdir}/playbooks/localhost.cfg ${tempdir}/cdh-mn.yml --extra-vars="hosts=all"

ansible-playbook -i ${tempdir}/localhost.cfg ${tempdir}/base.yml --extra-vars="hosts=all"
ansible-playbook -i ${tempdir}/localhost.cfg ${tempdir}/cdh-mn.yml --extra-vars="hosts=all"
