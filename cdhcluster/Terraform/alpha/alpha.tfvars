region = "us-east-1"

account = "plmitproject5"

accountID = "068527915195"

cluster = "alphatesting"

environment = ""

seclevel = "dev"

ami = "ami-2e83dc54"

instanceSubnet = "subnet-838313da"

instanceVPCID = "vpc-03f3d366"

instanceIPCidrBlock = "10.146.4.0/22"

ephemeralVPCID = "vpc-4eb06e37"

wnCount = 2

hdfsCount = 2

mnCount = 3

cmCount = 2

os = "centos7"
