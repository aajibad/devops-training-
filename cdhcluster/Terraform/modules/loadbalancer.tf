resource "aws_elb" "wnELB" {
  name = "CDHCluster-${var.cluster}-wn-elb"
  subnets = ["${var.instanceSubnet}"]
  security_groups = ["${var.wn_elb_sg_id}"]

  listener {
    instance_port = 21000
    instance_protocol = "tcp"
    lb_port = 21000
    lb_protocol = "tcp"
  }

  listener {
    instance_port = 21050
    instance_protocol = "tcp"
    lb_port = 21050
    lb_protocol = "tcp"
  }

  health_check {
    healthy_threshold = 4
    unhealthy_threshold = 2
    timeout = 15
    target = "tcp:21000"
    interval = 30
  }

  instances = ["${var.worknodes_id}"]
  cross_zone_load_balancing = false
  idle_timeout = 60
  connection_draining = true
  connection_draining_timeout = 300
}
output "wnELB" {
  value = "${aws_elb.wnELB.name}"
}
