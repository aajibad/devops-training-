resource "aws_instance" "clouderanode" {
  count = "${var.cmCount}"
  ami = "${var.ami}"
  instance_type = "m4.xlarge"
  subnet_id = "${var.instanceSubnet}"
  key_name = "${var.account}-${var.region}"
  vpc_security_group_ids = [
    "${var.cdh_access_sg_id}",
    "${var.cluster_sg_id}",
    "${var.remote_access_sg_id}"]
  iam_instance_profile = "${var.cdhinstanceprofile_id}"
  user_data = "${file("../modules/files/cm_userdata.sh")}"

  root_block_device {
    volume_type = "gp2"
    volume_size = 200
    delete_on_termination= "true"
  }

  tags {
    #Name = "${var.cluster}-cdh-cm"
    Name = "${var.cluster}-cdh-cm${count.index}"
    Application = "product-intelligence"
    Cluster = "${var.cluster}"
    Environment = "${var.environment}"
    OS = "${var.os}"
    Role = "cdh-cm"
    SecLevel = "${var.seclevel}"
  }
}


resource "aws_instance" "hdfsnodes" {
  count = "${var.hdfsCount}"
  ami = "${var.ami}"
  instance_type = "d2.4xlarge"
  subnet_id = "${var.instanceSubnet}"
  key_name = "${var.account}-${var.region}"
  vpc_security_group_ids =  [
    "${var.cdh_access_sg_id}",
    "${var.cluster_sg_id}",
    "${var.remote_access_sg_id}"]
  iam_instance_profile = "${var.cdhinstanceprofile_id}"
  user_data = "${file("../modules/files/wn_userdata.sh")}"

  root_block_device {
    volume_type = "gp2"
    volume_size = 200
    delete_on_termination= "true"
  }

  tags {
    Name = "${var.cluster}-cdh-hdfs${count.index}"
    Application = "product-intelligence"
    Cluster = "${var.cluster}"
    Environment = "${var.environment}"
    OS = "${var.os}"
    Role = "cdh-hdfs"
    SecLevel = "${var.seclevel}"
  }
}

resource "aws_instance"  "masternodes" {
  count = "${var.mnCount}"
  instance_type = "m4.xlarge"
  ami = "${var.ami}"
  count = 3

  subnet_id = "${var.instanceSubnet}"
  key_name = "${var.account}-${var.region}"
  vpc_security_group_ids = [
    "${var.cdh_access_sg_id}",
    "${var.cluster_sg_id}",
    "${var.remote_access_sg_id}"]
  iam_instance_profile = "${var.cdhinstanceprofile_id}"
  user_data = "${file("../modules/files/mn_userdata.sh")}"

  root_block_device {
    volume_type = "gp2"
    volume_size = 200
    delete_on_termination= "true"
  }

  ebs_block_device {
    device_name = "/dev/sdb"
    volume_type = "gp2"
    volume_size = 1000
    delete_on_termination= "true"
    encrypted = "true"
  }

  tags {
    Name = "${var.cluster}-cdh-mn${count.index}"
    Application = "product-intelligence"
    Cluster = "${var.cluster}"
    Environment = "${var.environment}"
    OS = "${var.os}"
    Role = "cdh-mn"
    SecLevel = "${var.seclevel}"
  }

#  provisioner "file" {
#    source = "files/cloudera"
#    destination = "/tmp/cloudera"
#  }
}

resource "aws_instance" "workernodes" {
  count = "${var.wnCount}"
  ami = "${var.ami}"
  instance_type = "r4.8xlarge"
  subnet_id = "${var.instanceSubnet}"
  key_name = "${var.account}-${var.region}"
  vpc_security_group_ids =  [
    "${var.cdh_access_sg_id}",
    "${var.cluster_sg_id}",
    "${var.remote_access_sg_id}"]
  iam_instance_profile = "${var.cdhinstanceprofile_id}"
  user_data = "${file("../modules/files/wn_userdata.sh")}"

  root_block_device {
    volume_type = "gp2"
    volume_size = 200
    delete_on_termination= "true"
  }

  ebs_block_device {
    device_name = "/dev/sdb"
    volume_type = "gp2"
    volume_size = 1000
    delete_on_termination= "true"
    encrypted = "true"
  }

  ebs_block_device {
    device_name = "/dev/sdc"
    volume_type = "gp2"
    volume_size = 1000
    delete_on_termination= "true"
    encrypted = "true"
  }

  ebs_block_device {
    device_name = "/dev/sdd"
    volume_type = "gp2"
    volume_size = 1000
    delete_on_termination= "true"
    encrypted = "true"
  }

  ebs_block_device {
    device_name = "/dev/sde"
    volume_type = "gp2"
    volume_size = 1000
    delete_on_termination= "true"
    encrypted = "true"
  }

  tags {
    Name = "${var.cluster}-cdh-wn${count.index}"
    Application = "product-intelligence"
    Cluster = "${var.cluster}"
    Environment = "${var.environment}"
    OS = "${var.os}"
    Role = "cdh-wn"
    SecLevel = "${var.seclevel}"
  }
}
output "hdfsnodes_id" {
  value = "${aws_instance.hdfsnodes.*.id}"
}

output "workernodes_id" {
  value = "${aws_instance.workernodes.*.id}"
}

output "masternodes_id" {
  value = "${aws_instance.masternodes.*.id}"
}

output "clouderanode_id" {
  value = "${aws_instance.clouderanode.*.id}"
}
