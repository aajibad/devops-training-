#!/bin/bash
##input the right information of the dns server
DOMAIN=
## This get the hostname
USER_DATA = `/usr/bin/curl -s http://169.254.169.254/latest/user-data`
###echo the hostname
HOSTNAME = `echo $USER_DATA`
### This get the public IP address of the instance
PUBIP = `/usr/bin/curl -s http://169.254.169.254/latest/user-data/public_ip`
###command nsupdate
cat<<EOF | /usr/bin/nsupdate -v
##nameserver if any  (input the right information)
server ns1.$DOMAIN
###zone  if any (input the right information)
zone ec2.$DOMAIN
##delete Instance public address of the  instance from PTR  record
update delete $PUBIP.in-addr.arpa IN PTR
##bind same public address to the hostname ( This part might not be needed if there something else doing that)
update add $PUBIP.in-addr.arpa 86400 IN PTR $HOSTNAME.$DOMAIN
send
EOF
