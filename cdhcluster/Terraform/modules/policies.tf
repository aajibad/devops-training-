resource "aws_iam_role_policy" "CDHS3Policy"{
  name = "${var.cluster}-CDHS3policy"
  role = "${var.cdhrole_id}"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": [
                "s3:ListAllMyBuckets"
            ],
            "Resource": "arn:aws:s3:::*",
            "Effect": "Allow",
            "Sid": "AllowListAllMyBuckets"
        },
        {
            "Action": [
                "s3:ListBucketVersions",
                "s3:ListBucket",
                "s3:ListBucketMultipartUploads"
            ],
            "Resource": "arn:aws:s3:::${var.account}-${var.cluster}-cdh",
            "Effect": "Allow",
            "Sid": "AllowedBucketOperations"
        },
        {
            "Action": [
                "s3:GetBucketNotification",
                "s3:GetBucketAcl",
                "s3:GetBucketCORS",
                "s3:GetBucketRequestPayment",
                "s3:GetBucketTagging",
                "s3:GetBucketLogging",
                "s3:GetReplicationConfiguration",
                "s3:GetBucketPolicy",
                "s3:GetLifecycleConfiguration",
                "s3:GetBucketLocation",
                "s3:GetBucketVersioning",
                "s3:GetBucketWebsite"
            ],
            "Resource": "arn:aws:s3:::${var.account}-${var.cluster}-cdh",
            "Effect": "Allow",
            "Sid": "AllowedBucketSubresourceOperations"
        },
        {
            "Action": [
                "s3:GetObjectVersionTorrent",
                "s3:GetObjectAcl",
                "s3:AbortMultipartUpload",
                "s3:GetObjectTorrent",
                "s3:RestoreObject",
                "s3:GetObjectVersion",
                "s3:DeleteObject",
                "s3:GetObject",
                "s3:ListMultipartUploadParts",
                "s3:PutObject",
                "s3:GetObjectVersionAcl"
            ],
            "Resource": "arn:aws:s3:::${var.account}-${var.cluster}-cdh",
            "Effect": "Allow",
            "Sid": "AllowedObjectOperations"
        },
        {
            "Action": [
                "s3:GetObject"
            ],
            "Resource": "arn:aws:s3:::${var.region}.elasticmapreduce/*",
            "Effect": "Allow",
            "Sid": "AllowedGetElasticMapReduceStuff"
        }
    ]
}
EOF
}

resource "aws_iam_role_policy" "CDHVPCPolicy" {
  name = "${var.cluster}-CDHVPCpolicy"
  role = "${var.cdhrole_id}"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "ec2:RunInstances",
            "Resource": [
                "arn:aws:ec2:${var.region}::image/*",
                "arn:aws:ec2:${var.region}::snapshot/*",
                "arn:aws:ec2:${var.region}:${var.accountID}:instance/*",
                "arn:aws:ec2:${var.region}:${var.accountID}:placement-group/*",
                "arn:aws:ec2:${var.region}:${var.accountID}:volume/*"
            ],
            "Effect": "Allow",
            "Sid": "EC2RunInstances"
        },
        {
            "Action": "ec2:RunInstances",
            "Resource": [
                "arn:aws:ec2:${var.region}:${var.accountID}:key-pair/${var.account}-ephemeral-${var.cluster}*"
            ],
            "Effect": "Allow",
            "Sid": "EC2RunInstancesRestrictKeyPair"
        },
        {
            "Condition": {
                "StringEquals": {
                    "ec2:vpc": "arn:aws:ec2:${var.region}:${var.accountID}:vpc/${var.ephemeralVPCID}"
                }
            },
            "Action": "ec2:RunInstances",
            "Resource": [
                "arn:aws:ec2:${var.region}:${var.accountID}:network-interface/*",
                "arn:aws:ec2:${var.region}:${var.accountID}:security-group/*",
                "arn:aws:ec2:${var.region}:${var.accountID}:subnet/*"
            ],
            "Effect": "Allow",
            "Sid": "EC2RunInstancesConditionalVPC"
        },
        {
            "Condition": {
                "StringEquals": {
                    "ec2:vpc": "arn:aws:ec2:${var.region}:${var.accountID}:vpc/${var.ephemeralVPCID}"
                }
            },
            "Action": [
                "ec2:AuthorizeSecurityGroupEgress",
                "ec2:AuthorizeSecurityGroupIngress",
                "ec2:CreateSecurityGroup",
                "ec2:DeleteSecurityGroup",
                "ec2:RevokeSecurityGroupEgress",
                "ec2:RevokeSecurityGroupIngress"
            ],
            "Resource": "*",
            "Effect": "Allow",
            "Sid": "EC2SecuityGroupConditionalVPC"
        }
    ]
}
EOF
}
resource "aws_iam_role_policy" "CloudWatchPolicy" {
  name = "${var.cluster}-CloudWatchpolicy"
  role = "${var.cdhrole_id}"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": [
                "cloudwatch:PutMetricAlarm",
                "cloudwatch:PutMetricData",
                "cloudwatch:DeleteAlarms"
            ],
            "Resource": "*",
            "Effect": "Allow",
            "Sid": "AllowedCloudwatchMetricAlarms"
        }
    ]
}
EOF
}

resource "aws_iam_role_policy" "SharedEC2Policy"{
  name = "${var.cluster}-CDHEC2policy"
  #role = "${aws_iam_role.cdhrole.id}"
  role = "${var.cdhrole_id}"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": [
                "iam:ListAccountAliases"
            ],
            "Resource": [
                "*"
            ],
            "Effect": "Allow",
            "Sid": "AllowLimitedIAM"
        },
        {
            "Action": [
                "ec2:DescribeTags",
                "ec2:DescribeInstances"
            ],
            "Resource": [
                "*"
            ],
            "Effect": "Allow",
            "Sid": "AllowEC2Describe"
        },
        {
            "Action": [
                "ssm:DescribeAssociation",
                "ssm:ListAssociations",
                "ssm:GetDocument",
                "ssm:UpdateAssociationStatus",
                "ds:CreateComputer"
            ],
            "Resource": [
                "*"
            ],
            "Effect": "Allow",
            "Sid": "AllowAccessToSSM"
        }
    ]
}
EOF
}

resource "aws_iam_role_policy" "SharedS3Policy"{
  name = "${var.cluster}-SharedS3policy"
  role = "${var.cdhrole_id}"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": [
                "s3:GetBucketLocation",
                "s3:ListAllMyBuckets"
            ],
            "Resource": [
                "arn:aws:s3:::*"
            ],
            "Effect": "Allow"
        },
        {
            "Action": [
                "s3:ListBucket"
            ],
            "Resource": [
                "arn:aws:s3:::${var.account}-omneo-config-data",
                "arn:aws:s3:::${var.account}-continuous-delivery"
            ],
            "Effect": "Allow"
        },
        {
            "Action": [
                "s3:GetObject",
                "s3:GetObjectVersion",
                "s3:ListObjects"
            ],
            "Resource": [
                "arn:aws:s3:::${var.account}-omneo-config-data/*",
                "arn:aws:s3:::${var.account}-continuous-delivery/CodeDeploy/*",
                "arn:aws:s3:::${var.account}-continuous-delivery/Ansible/*",
                "arn:aws:s3:::${var.account}-continuous-delivery/common/*",
                "arn:aws:s3:::${var.account}-continuous-delivery/CloudFormation/*"
            ],
            "Effect": "Allow"
        },
        {
            "Action": [
                "s3:PutObject"
            ],
            "Resource": [
                "arn:aws:s3:::${var.account}-continuous-delivery/CodeDeploy/logs/*"
            ],
            "Effect": "Allow"
        }
    ]
}
EOF
}
