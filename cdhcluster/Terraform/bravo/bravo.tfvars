region = "us-east-1"

account = "plmitproject5"

accountID = "068527915195"

cluster = "bravo"

environment = ""

seclevel = "dev"

ami = "ami-2e83dc54"

instanceSubset = "subnet-4bb40f60"

instanceVPCID = "vpc-03f3d366"

instanceIPCidrBlock = "10.146.4.0/22"

ephemeralVPCID = "vpc-4eb06e37"

wnCount = 1

hdfsCount = 1

mnCount = 3

cmCount = 1

os = "centos7"
