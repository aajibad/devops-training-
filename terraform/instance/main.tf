resource "aws_instance" "instance" {
  count = "${var.count}"

  instance_type          = "${var.instance_type}"
  ami                    = "${lookup(var.aws_amis, var.aws_region)}"
  key_name               = "${var.key_pair_id}"
  vpc_security_group_ids = ["${var.security_group_id}"]
  subnet_id              = "${var.subnet_id}"

  root_block_device {
    volume_size = "${var.disk_size}"
  }

  tags {
    Name  = "${format("%s%02d", var.group_name, count.index + 1)}"
    Group = "${var.group_name}"
  }

  lifecycle {
    create_before_destroy = true
  }

  # Provisioning

  connection {
    user        = "ubuntu"
    private_key = "${file(var.private_key_path)}"
  }
  provisioner "remote-exec" {
    inline = [
      "sudo apt-get -y update",
    ]
  }
}

## Creating Launch Configuration
resource "aws_launch_configuration" "scaling" {
  image_id        = "${lookup(var.amis,var.region)}"
  instance_type   = "${var.instance_type}"
  security_groups = ["${var.security_group_id}"]
  key_name        = "${var.key_pair_id}"

  lifecycle {
    create_before_destroy = true
  }
}

## Creating AutoScaling Group
resource "aws_autoscaling_group" "example" {
  launch_configuration = "${aws_launch_configuration.scaling.id}"
  availability_zones   = ["${data.aws_availability_zones.all.names}"]
  min_size             = 2
  max_size             = 10
  load_balancers       = ["${aws_elb.webUI.name}"]
  health_check_type    = "ELB"

  tag {
    key                 = "Name"
    value               = "webUIELB"
    propagate_at_launch = true
  }
}

module "backend_scheduler" {
  source            = "./instance"
  subnet_id         = "${aws_subnet.private.id}"
  key_pair_id       = "${aws_key_pair.auth.id}"
  security_group_id = "${aws_security_group.default.id}"

  count      = 2
  group_name = "api"
}

module "backend_worker" {
  source            = "./instance"
  subnet_id         = "${aws_subnet.private.id}"
  key_pair_id       = "${aws_key_pair.auth.id}"
  security_group_id = "${aws_security_group.default.id}"

  count         = 2
  group_name    = "worker"
  instance_type = "t2.medium"
}

module "webUI" {
  source            = "./instance"
  subnet_id         = "${aws_subnet.private.id}"
  key_pair_id       = "${aws_key_pair.auth.id}"
  security_group_id = "${aws_security_group.default.id}"

  count      = 2
  group_name = "webUI"
}

# Public Backend ELB
resource "aws_elb" "backend" {
  name = "elb-public-backend"

  subnets         = ["${aws_subnet.public.id}", "${aws_subnet.private.id}"]
  security_groups = ["${aws_security_group.elb.id}"]
  instances       = ["${module.backend_scheduler.instance_ids}"]

  listener {
    instance_port     = 80
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "HTTP:80/healthcheck.php"
    interval            = 30
  }
}

# Public webUI ELB
resource "aws_elb" "webUI" {
  name = "elb-public-webUI"

  subnets         = ["${aws_subnet.public.id}", "${aws_subnet.private.id}"]
  security_groups = ["${aws_security_group.elb.id}"]
  instances       = ["${module.webUI.instance_ids}"]

  listener {
    instance_port     = 80
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "HTTP:80/healthcheck.php"
    interval            = 30
  }
}
